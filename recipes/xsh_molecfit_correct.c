/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/* xshooter headers */
#include <xsh_error.h>
#include <xsh_utils.h>

/* Molecfit Model */
#include "xsh_molecfit_correct.h"
#include <mf_wrap_config.h>
#include <telluriccorr.h>
//#include <mf_spectrum.h>
//#include <mf_wrap.h>
/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Enumeration types
 */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                             Recipe Defines
 ---------------------------------------------------------------------------*/

#define RECIPE_ID "xsh_molecfit_correct"
#define RECIPE_AUTHOR "N. Fernando, B. Miszalski"
#define RECIPE_CONTACT "nuwanthika.fernando@partner.eso.org"

/*---------------------------------------------------------------------------
                            Functions prototypes
 ---------------------------------------------------------------------------*/

/*
 *   Plugin initalization, execute and cleanup handlers
 */

int xsh_molecfit_correct_create(cpl_plugin *);
int xsh_molecfit_correct_exec(cpl_plugin *);
int xsh_molecfit_correct_destroy(cpl_plugin *);

/* The actual executor function */
//int xsh_molecfit_correct(cpl_frameset *frameset, const cpl_parameterlist  *parlist);

/*----------------------------------------------------------------------------*/
/**
 *                 static variables
 */
/*----------------------------------------------------------------------------*/

char xsh_molecfit_correct_description_short[] =
"Applies molecfit_correct";

char xsh_molecfit_correct_description[] =
"Applies molecfit_correct";

/*----------------------------------------------------------------------------*/
/**
 *                 Macros
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Structured types
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 *                 Functions prototypes
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @defgroup molecfit_correct  It runs Molecfit on a generic input spectrum file to compute an atmospheric model.
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
 *                              Functions code
 */
/*----------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module.
  @param    list    the plugin list
  @return   0 if everything is ok, -1 otherwise

  Create the recipe instance and make it available to the application using
  the interface. This function is exported.
 */
/*--------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist *list) {
  cpl_recipe *recipe = NULL;
  cpl_plugin *plugin = NULL;

  recipe = cpl_calloc(1, sizeof(*recipe));
  if ( recipe == NULL ){
    return -1;
  }

  plugin = &recipe->interface ;

  cpl_plugin_init(plugin,
                  CPL_PLUGIN_API,                   /* Plugin API */
                  XSH_BINARY_VERSION,            /* Plugin version */
                  CPL_PLUGIN_TYPE_RECIPE,           /* Plugin type */
                  RECIPE_ID,                        /* Plugin name */
                  xsh_molecfit_correct_description_short, /* Short help */
                  xsh_molecfit_correct_description,   /* Detailed help */
                  RECIPE_AUTHOR,                    /* Author name */
                  RECIPE_CONTACT,                   /* Contact address */
                  xsh_get_license(),                /* Copyright */
                  xsh_molecfit_correct_create,
                  xsh_molecfit_correct_exec,
                  xsh_molecfit_correct_destroy);

  cpl_pluginlist_append(list, plugin);

  return (cpl_error_get_code() != CPL_ERROR_NONE);
 }

/*--------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using
  the interface.

 */
/*--------------------------------------------------------------------------*/

int xsh_molecfit_correct_create(cpl_plugin *plugin){
  cpl_recipe *recipe = NULL;
  /*xsh_clipping_param detarc_clip_param =  {2.0, 0, 0.7, 0, 0.3};
  xsh_detect_arclines_param detarc_param =
  {6, 3, 0, 5, 4, 1, 5, 5.0,
    XSH_GAUSSIAN_METHOD, FALSE};
  xsh_dispersol_param dispsol_param = { 4, 5 } ; 
  char paramname[256];
  cpl_parameter* p=NULL;
  int ival=DECODE_BP_FLAG_DEF;
  */

  /* Reset library state */
  xsh_init();

  /* Check input */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin");

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH,
          "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  assure( recipe->parameters != NULL,
          CPL_ERROR_ILLEGAL_OUTPUT,
          "Memory allocation failed!");

  //xsh_molecfit_correct parameters

  //column_wave
  check(xsh_parameters_new_string(recipe->parameters,RECIPE_ID,
  "COLUMN_WAVE","WAVE",
  "In the case of fits binary science input: name of the column in the input that identifies the wavelength."));

  //column_flux
  check(xsh_parameters_new_string(recipe->parameters,RECIPE_ID,
  "COLUMN_FLUX","FLUX",
  "In the case of fits binary science input: name of the column in the input that identifies the flux."));

  //column_dflux
  check(xsh_parameters_new_string(recipe->parameters,RECIPE_ID,
  "COLUMN_DFLUX","ERR",
  "In the case of fits binary science input: name of the column in the input that identifies the flux errors."));

  //threshold 
  check(xsh_parameters_new_double(recipe->parameters,RECIPE_ID,
  "THRESHOLD",0.01,
  "Use this value when the transmission function is lower than the specified threshold."));

  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ){
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else {
      return 0;
    }
}


/*--------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/

int xsh_molecfit_correct_exec(cpl_plugin *plugin) {
  cpl_recipe *recipe = NULL;

  /* Check parameter */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin" );

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;
  /* Check recipe */
  xsh_molecfit_correct( recipe->frames, recipe->parameters);

  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ) {
      xsh_error_dump(CPL_MSG_ERROR);
      cpl_error_reset();
      return 1;
    }
    else {
      return 0;
    }
}

/*--------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/
int xsh_molecfit_correct_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe = NULL;

    /* Check parameter */
    assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin" );

    /* Get the recipe out of the plugin */
    assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
            CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

    recipe = (cpl_recipe *)plugin;

    xsh_free_parameterlist(&recipe->parameters);

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {
            return 1;
        }
    else
        {
            return 0;
        }
}

int xsh_molecfit_correct(cpl_frameset *frameset, const cpl_parameterlist  *parlist){

  cpl_frameset_dump(frameset,NULL);

  /* Get initial errorstate */
  cpl_error_ensure(frameset && parlist, CPL_ERROR_NULL_INPUT, return CPL_ERROR_NULL_INPUT, "NULL input : frameset and/or parlist");
  cpl_errorstate initial_errorstate = cpl_errorstate_get();
  cpl_error_code err = CPL_ERROR_NONE;//molecfit_check_and_set_groups(frameset);

  cpl_parameterlist* ilist = cpl_parameterlist_new();
  cpl_parameterlist* iframelist = cpl_parameterlist_new();
  // get instrument specific parameter defaults
  err = xsh_molecfit_correct_config(frameset,parlist,ilist,iframelist);

  const char* input_name = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"INPUTNAME"));
  const char* arm = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"ARM"));
  //const char* obsmode = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"OBSMODE"));
  //const char* is_idp = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"IDP"));
  //const char* fname = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"INPUTFILENAME"));


  /* Check frameset TAGS */
    //setup some necessary frameset tags 
    cpl_frame* f = cpl_frameset_find(frameset,input_name);
    if(f){
      cpl_frame_set_group(f,CPL_FRAME_GROUP_RAW);
    }
    const char* tcor_tag = mf_wrap_tag_suffix("TELLURIC_CORR",arm,CPL_FALSE);
    f = cpl_frameset_find(frameset,tcor_tag);
    if(f){
      cpl_frame_set_group(f,CPL_FRAME_GROUP_CALIB);
    }

    if(strstr(input_name,"SCI_SLIT_FLUX_MERGE2D")){
        err = CPL_ERROR_INCOMPATIBLE_INPUT;
        cpl_msg_info(cpl_func,"Error: Invalid 2D input (not currently supported)");
        return err;
    }


  /* Check mandatory TAGS/Parameters */
  /*
  if (!err) {

      // SCIENCE_CALCTRANS/SCIENCE : Check only the first (at least need to contain one) 
      cpl_frame *input_frame_data = cpl_frameset_find(frameset, MOLECFIT_SCIENCE);
      if (!input_frame_data) {
          err = cpl_error_set_message(cpl_func, CPL_ERROR_DATA_NOT_FOUND, "%s data not found in frameset!", MOLECFIT_SCIENCE);
      }

      // TELLURIC_CORR 
      if (!err) {
          const cpl_frame *input_telluric_corr = cpl_frameset_find_const(frameset, MOLECFIT_TELLURIC_CORR);
          if (!input_telluric_corr) err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT, "Illegal INPUT TELLURIC_CORR!");
      }

      // MAPPING_CORRECT 
      if (!err) {
          const cpl_frame *input_mapping_correct = cpl_frameset_find_const(frameset, MOLECFIT_MAPPING_CORRECT);
          if (!input_mapping_correct && (!strcmp(cpl_parameter_get_string(cpl_parameterlist_find_const(parlist, MOLECFIT_PARAMETER_MAPPING_CORRECT)), MF_PARAMETERS_NULL) ) ){
              err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT, "Illegal INPUT MAPPING_CORRECT!");
          }
      }
  }
  }*/
  
  
  // TEST MAPPING CORRECT 
  /*if (!err) {
      cpl_error_code map_err=CPL_ERROR_NONE;
      map_err=molecfit_config_mappingchk(frameset,parlist,MOLECFIT_TELLURIC_CORR,
                                                          MOLECFIT_MAPPING_CORRECT,
							  "TELLURIC_CORR_EXT",
							  "MAPPING_CORRECT",
							  "SCIENCE");
      if (map_err) cpl_msg_error(cpl_func,"Mapping Errors detected cannot proceed further");
  }*/
     

  // Recipe Parameters : Need scientific_header_primary 
  /*molecfit_correct_parameter *parameters = NULL;
  if (!err) {

      // Get recipe parameters and update the molecfit default configuration 
      cpl_msg_info(cpl_func, "Load '%s' recipe parameters ...", MOLECFIT_CORRECT);
      parameters = molecfit_correct_parameters(frameset, parlist);

      if (!parameters) err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT, "Illegal INPUT PARAMETERS!");
      else             err = cpl_error_get_code();
  }*/
    cpl_parameterlist* mergedlist = cpl_parameterlist_new();

    cpl_msg_info(cpl_func,"calling mf_wrap_merge_parameterlists");
    err = mf_wrap_merge_parameterlists(ilist, parlist,mergedlist);

    /* Recipe Parameters : Need scientific_header_primary */
    molecfit_correct_parameter *parameters = NULL;
    if (!err) {

        /* Get recipe parameters and update the molecfit default configuration */
        cpl_msg_info(cpl_func, "Load 'MOLECFIT_CORRECT' recipe parameters ...");
        /* Note: TELLURIC_CORR is loaded in mf_wrap_config_corr_init() */
        parameters = mf_wrap_config_corr_init(frameset, mergedlist,arm);

        if (!parameters) err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT, "Illegal INPUT PARAMETERS!");
        else             err = cpl_error_get_code();
    }


  /* Load TAG = MAPPING_CORRECT */
  cpl_table *mapping_correct = NULL;
  if (!err) {

      if (parameters->mapping_correct_table) {
          mapping_correct = cpl_table_duplicate(parameters->mapping_correct_table);
      } else {
          const char* tag_name = mf_wrap_tag_suffix(MOLECFIT_MAPPING_CORRECT,NULL,CPL_FALSE);
          cpl_msg_info (cpl_func, "Loading %s cpl_table", tag_name);
          //cpl_msg_info (cpl_func, "Loading %s cpl_table", MOLECFIT_MAPPING_CORRECT);
            mapping_correct = mf_wrap_load_unique_table(frameset, tag_name);
          //mapping_correct = molecfit_load_unique_table(frameset, MOLECFIT_MAPPING_CORRECT);
      }

      if (     !mapping_correct                                                           ) err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT,      "Illegal INPUT MAPPING_CORRECT -> Not input!");
      else if (cpl_table_get_column_min(mapping_correct, MOLECFIT_MAPPING_CORRECT_EXT) < 0) err = cpl_error_set_message(cpl_func, CPL_ERROR_INCOMPATIBLE_INPUT, "Illegal INPUT MAPPING_CORRECT -> Minimum column < 0!");
      else                                                                                  err = cpl_error_get_code();
  }

  /*** Correct SCIENCE_CALCTRANS/SCIENCE spectra ***/
  if (!err) {

      //cpl_msg_info(cpl_func, "CORRECT input %s/%s files ", MOLECFIT_SCIENCE_CALCTRANS, MOLECFIT_SCIENCE);

      cpl_size n_frames = cpl_frameset_get_size(frameset);
      for (cpl_size frame = 0; frame < n_frames && !err; frame++) {

         /* Check all inputs in frameset to get only the SCIENCE_CALCTRANS/SCIENCE frames */
         cpl_frame  *frame_data = cpl_frameset_get_position(frameset, frame);
         const char *tag        = cpl_frame_get_tag(frame_data);
         //use strstr here, as the arm suffix may be present
         if ( strstr(tag,"SCI_SLIT_FLUX_IDP") ||  
              strstr(tag,"SCI_SLIT_FLUX_MERGE1D") ||  
              strstr(tag,"SCI_SLIT_FLUX_MERGE2D") ||  
              strstr(tag,"TELL_SLIT_MERGE1D") ||  
              strstr(tag,"TELL_SLIT_FLUX_MERGE1D") ||  
              strstr(tag,"TELL_SLIT_FLUX_IDP")
             ){

             /* Create a new independent cpl_frameset for this concrete input DATA frame */
             cpl_frameset *frameset_output = cpl_frameset_new();
             cpl_frameset_insert(frameset_output, cpl_frame_duplicate(frame_data));

             /* Load every frame in the input */
             const char    *filename = cpl_frame_get_filename(frame_data);
             mf_wrap_fits *data     = mf_wrap_fits_load(filename, CPL_FALSE);
             //molecfit_fits *data     = molecfit_fits_load(filename, CPL_FALSE);

             /* Check DATA */
             if (!data) err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_INPUT, "Illegal INPUT %s cpl_frame!", tag);
             else       err = cpl_error_get_code();

             /* Convert to table for execute molecfit */
             //if (!err) err = molecfit_data_convert_to_table( data,
             if (!err) err = mf_wrap_data_convert_to_table( data,
                                                             parameters->chip_extensions,
                                                             parameters->use_only_input_pri_ext,
                                                             parameters->dflux_extension_data,
                                                             parameters->mask_extension_data,
                                                             parameters->column_lambda,
                                                             parameters->column_flux,
                                                             parameters->column_dflux,
                                                             parameters->column_mask);

             /* Save input DATA --> Combined (molecfit_spectrum in BINTABLE format) */
//not relevant for xshooter
/*             if (parameters->chip_extensions) {

                 const char *filename_base = basename(data->filename);

                 char *out_combined = NULL;
                 if (parameters->suppress_extension) out_combined  = cpl_sprintf("%s_%lld.fits", MOLECFIT_CORRECT_CHIPS_COMBINED, frame        );
                 else                                out_combined  = cpl_sprintf("%s_%s",        MOLECFIT_CORRECT_CHIPS_COMBINED, filename_base);

                 cpl_size index_combined = data->v_ext[0].spectrum_data ? 0 : 1;
                 cpl_msg_info(cpl_func, "Save combined multi-extension molecfit_spectrum input FITS file ('%s') in ext=%lld ...", out_combined, index_combined);

                 err += molecfit_save(frameset, frameset, parlist, RECIPE_NAME, parameters->pl, MOLECFIT_CORRECT_CHIPS_COMBINED, out_combined);
                 if (!err) err = molecfit_save_mf_results(data->v_ext[index_combined].spectrum_head, out_combined, CPL_FALSE, NULL, data->v_ext[index_combined].spectrum_data, NULL);
             }
             */

             /* Execution extensions */
             cpl_size n_ext;
             if (     parameters->use_only_input_pri_ext) n_ext = 1;
             else if (parameters->chip_extensions       ) n_ext = data->v_ext[0].spectrum_data ? 1 : 2;
             else n_ext = data->n_ext;

             /* Create output SPECTRUM SCIENCE FITS file */
             mf_wrap_fits *spectrum_corr_data = NULL;
             //molecfit_fits *spectrum_corr_data = NULL;
             if (!err) {

                 char *spectrum_filename = cpl_sprintf("SPECTRUM_%s.fits", input_name);//data->filename);
                 //char *spectrum_filename = cpl_sprintf("SPECTRUM_%s", data->filename);
                 //char *spectrum_filename = cpl_sprintf("SPECTRUM_%s", basename(data->filename));
                 //spectrum_corr_data = molecfit_fits_create(spectrum_filename, molecfit_fits_file_image_1D, molecfit_fits_vector, data->n_ext);
                 spectrum_corr_data = mf_wrap_fits_create(spectrum_filename, mf_wrap_fits_file_image_1D, mf_wrap_fits_vector, data->n_ext);
                 cpl_free(spectrum_filename);

                 if (spectrum_corr_data) err = cpl_error_get_code();
                 else                    err = cpl_error_set_message(cpl_func, CPL_ERROR_ILLEGAL_OUTPUT, "I cannot create the FITS output file %s!", MOLECFIT_SPECTRUM_TELLURIC_CORR);

                 /* Create dummy extensions */
                 for (cpl_size ext = 0; ext < data->n_ext; ext++) {
                     if (data->v_ext[ext].spectrum_data) {
                         cpl_size size = -1;
                         //if (     data->format == molecfit_fits_file_table   ) {
                         if (     data->format == mf_wrap_fits_file_table) {
                             if (   cpl_propertylist_get_int(data->v_ext[ext].header, MOLECFIT_FITS_KEYWORD_NAXIS1) >  1
                                 && cpl_propertylist_get_int(data->v_ext[ext].header, MOLECFIT_FITS_KEYWORD_NAXIS2) == 1) {
                                 size = cpl_propertylist_get_int(data->v_ext[ext].header, MOLECFIT_FITS_KEYWORD_NAXIS1);
                             } else {
                                 size = cpl_table_get_nrow(data->v_ext[ext].table);
                             }
                         }
                         //else if (data->format == molecfit_fits_file_image_1D) size = cpl_vector_get_size(   data->v_ext[ext].vector);
                         //else if (data->format == molecfit_fits_file_image_2D) size = cpl_image_get_size_x(  data->v_ext[ext].image );
                         //else if (data->format == molecfit_fits_file_image_3D) size = cpl_imagelist_get_size(data->v_ext[ext].cube  );
                         else if (data->format == mf_wrap_fits_file_image_1D) size = cpl_vector_get_size(   data->v_ext[ext].vector);
                         else if (data->format == mf_wrap_fits_file_image_2D) size = cpl_image_get_size_x(  data->v_ext[ext].image );
                         else if (data->format == mf_wrap_fits_file_image_3D) size = cpl_imagelist_get_size(data->v_ext[ext].cube  );

                         spectrum_corr_data->v_ext[ext].vector = cpl_vector_new(size);
                     }
                 }
             }

             /* Loop over the data extensions */
             int null;
             for (cpl_size ext = 0; ext < n_ext && !err; ext++) {

                 /* Create spectrum header output */
                 if (!err) spectrum_corr_data->v_ext[ext].header = cpl_propertylist_duplicate(data->v_ext[ext].spectrum_head);

                 /* Get data extension : Only manage extensions with data */
                 if (data->v_ext[ext].spectrum_data) {

                     /* Get TELLURIC_CORR */
                     cpl_size         index_telluric_corr_ext = cpl_table_get(mapping_correct, MOLECFIT_MAPPING_CORRECT_EXT, ext, &null);
                     cpl_vector *telluric_corr          = parameters->telluric_corr->v_ext[index_telluric_corr_ext].vector;
                     double threshold = cpl_parameter_get_double(cpl_parameterlist_find(mergedlist,"THRESHOLD"));
                     //only apply the threshold if it is a sensible value
                     if(threshold >= 0 && threshold <= 1.0) {
                        cpl_size vs = cpl_vector_get_size(telluric_corr);
                        //iterate over vector, applying the threshold
                        for (cpl_size vdx=0;vdx < vs; vdx++){
                            if(cpl_vector_get(telluric_corr,vdx) < threshold){
                                cpl_vector_set(telluric_corr,vdx,threshold);
                            }
                        }
                     }
                     //debug - shows that the threshold is applied
                     //const char* vfname = cpl_sprintf("vec%lld.fits",ext);
                     //cpl_vector_save(telluric_corr,vfname,CPL_TYPE_DOUBLE,NULL,CPL_IO_CREATE);

                     /* Check DATA with TELLURIC_CORR */
                     if (!telluric_corr) 
                         err = cpl_error_set_message(cpl_func, CPL_ERROR_INCOMPATIBLE_INPUT, "I cannot MATCH the TELLURIC_CORR extension with the %s extension for correct!", tag);
                     else if (cpl_table_get_nrow(data->v_ext[ext].spectrum_data) <= 0
                              || cpl_table_get_nrow(data->v_ext[ext].spectrum_data) != cpl_vector_get_size(telluric_corr)) 
                         err = cpl_error_set_message(cpl_func, CPL_ERROR_INCOMPATIBLE_INPUT, "I cannot MATCH the rows between the TELLURIC_CORR extension with the %s extension for correct!", tag);

                     if (!err) {

                         /* Create input molecfit data */
                         cpl_msg_info(cpl_func, "Create input molecfit cpl_table data : %s [%s], ext=%lld", tag, data->filename, ext);
                         cpl_table *molecfit_data = mf_spectrum_create_input_to_correct( data->v_ext[ext].spectrum_data,
                                                                                         parameters->column_lambda,
                                                                                         parameters->column_flux,
                                                                                         parameters->column_dflux,
                                                                                         parameters->column_mask,
                                                                                         telluric_corr);

                         if (molecfit_data) err = cpl_error_get_code();
                         else               err = cpl_error_set_message(cpl_func, CPL_ERROR_INCOMPATIBLE_INPUT, "I cannot Create the input molecfit SPECTRUM_TABLE to correct!");


                         /* Apply correction to molecfit data */
                         if (!err) {
                             cpl_msg_info(cpl_func, "File : %s, ext=%lld : Apply correction", data->filename, ext);
                             err = mf_calctrans_correct_spectrum(molecfit_data, MF_PARAMETERS_TRANSMISSION_TRUE);
                         }

                         /* Create spectrum data output */
                         if (!err) {

                             /* Copy data */
                             err = mf_wrap_data_update_vector(molecfit_data, parameters->chip_extensions, spectrum_corr_data, ext);
                             //err = molecfit_data_update_vector(molecfit_data, parameters->chip_extensions, spectrum_corr_data, ext);

                             /* Copy additional if chip combined */
                             if (parameters->chip_extensions) {
                                 for (cpl_size i = ext + 1; i < data->n_ext; i++) {
                                     spectrum_corr_data->v_ext[i].header = cpl_propertylist_duplicate(data->v_ext[i].spectrum_head);
                                 }
                             }
                         }

                         /* Update Input data with telluric correction */
                         if (!err) {
                             if (     data->format == mf_wrap_fits_file_table   ) err = mf_wrap_data_update_table( molecfit_data, parameters->chip_extensions, data, ext, parameters->column_flux);
                             else if (data->format == mf_wrap_fits_file_image_1D) err = mf_wrap_data_update_vector(molecfit_data, parameters->chip_extensions, data, ext);
                             else if (data->format == mf_wrap_fits_file_image_2D) err = mf_wrap_data_update_image( molecfit_data, parameters->chip_extensions, data, ext);
                             else if (data->format == mf_wrap_fits_file_image_3D) err = mf_wrap_data_update_cube(  molecfit_data, parameters->chip_extensions, data, ext);
                             /*if (     data->format == molecfit_fits_file_table   ) err = molecfit_data_update_table( molecfit_data, parameters->chip_extensions, data, ext, parameters->column_flux);
                             else if (data->format == molecfit_fits_file_image_1D) err = molecfit_data_update_vector(molecfit_data, parameters->chip_extensions, data, ext);
                             else if (data->format == molecfit_fits_file_image_2D) err = molecfit_data_update_image( molecfit_data, parameters->chip_extensions, data, ext);
                             else if (data->format == molecfit_fits_file_image_3D) err = molecfit_data_update_cube(  molecfit_data, parameters->chip_extensions, data, ext);
                             */
                         }

                         /* Cleanup */
                         if (molecfit_data) cpl_table_delete(molecfit_data);
                     }
                 }
             }

             /* Save update OUT : SPECTRUM and DATA */
             if (!err) {

                 //const char *filename_base = basename(data->filename);
                 const char *filename_base = input_name;//data->filename;//basename(data->filename);
                 //const char* output_tag = mf_wrap_tag_suffix(MOLECFIT_SPECTRUM_TELLURIC_CORR,arm,CPL_FALSE);
                 //TODO: still to sort output filenames/tags etc.

                 /* Save input SPECTRUM extracted --> Corrected */
                 char *out_telluric_corr = NULL;
                 //const char* output_tag = mf_wrap_tag_suffix(MOLECFIT_SPECTRUM_TELLURIC_CORR,arm,CPL_FALSE);
                 if (parameters->suppress_extension) out_telluric_corr  = cpl_sprintf("%s_%lld.fits", MOLECFIT_SPECTRUM_TELLURIC_CORR, frame        );
                 else                                out_telluric_corr  = cpl_sprintf("%s_%s.fits",        MOLECFIT_SPECTRUM_TELLURIC_CORR, filename_base);

                 //cpl_msg_info(cpl_func, "Writing on disk [%s] %s input FITS corrected on SPECTRUM format --> %s (PRO.CATG=%s)", data->filename, tag, out_telluric_corr, MOLECFIT_SPECTRUM_TELLURIC_CORR);
                 err = mf_wrap_fits_write(frameset, frameset_output, parlist, RECIPE_NAME, MOLECFIT_SPECTRUM_TELLURIC_CORR, spectrum_corr_data, out_telluric_corr);

                 cpl_free(out_telluric_corr);


                 /* Save input SCIENCE --> Corrected */
                 if (!err) {

                     char *out_data = NULL;
                     if (parameters->suppress_extension) out_data = cpl_sprintf("%s_%lld.fits", MOLECFIT_SCIENCE_TELLURIC_CORR, frame        );
                     else                                out_data = cpl_sprintf("%s_%s.fits",        MOLECFIT_SCIENCE_TELLURIC_CORR, filename_base);

                     cpl_msg_info(cpl_func, "Writing on disk [%s] %s input FITS corrected on the same format --> %s (PRO.CATG=%s)", data->filename, tag, out_data, MOLECFIT_SCIENCE_TELLURIC_CORR);
                     err = mf_wrap_fits_write(frameset, frameset_output, parlist, RECIPE_NAME, MOLECFIT_SCIENCE_TELLURIC_CORR, data, out_data);
                     //err = molecfit_fits_write(frameset, frameset_output, parlist, RECIPE_NAME, MOLECFIT_SCIENCE_TELLURIC_CORR, data, out_data);

                     cpl_free(out_data);
                 }
             }

             /* Cleanup */
             if (frameset_output   ) cpl_frameset_delete(frameset_output);
             if (data              ) mf_wrap_fits_delete(data);
             if (spectrum_corr_data) mf_wrap_fits_delete(spectrum_corr_data);
             //if (data              ) molecfit_fits_delete(data);
             //if (spectrum_corr_data) molecfit_fits_delete(spectrum_corr_data);
         }
      }
  }

  /* Cleanup */
  if (parameters     ) molecfit_correct_parameter_delete( parameters     );
  if (mapping_correct) cpl_table_delete(                        mapping_correct);


  /* Check Recipe status and end */
  if (!err && cpl_errorstate_is_equal(initial_errorstate)) {
      cpl_msg_info(cpl_func,"Recipe successfully!");
  } else {
      /* Dump the error history */
      cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);
      cpl_msg_error(cpl_func,"Recipe failed!, error(%d)=%s", err, cpl_error_get_message());
  }

  return err;
}


cpl_error_code xsh_molecfit_correct_config(cpl_frameset *frameset, const cpl_parameterlist  *parlist,
    		cpl_parameterlist* ilist, cpl_parameterlist* iframelist){

        //cpl_msg_info(cpl_func,"xsh_molecfit_correct_config");
        //cpl_msg_info(cpl_func,"FRAMESET");
        //cpl_frameset_dump(frameset,stdout);
        //cpl_msg_info(cpl_func,"PARLIST");
        //cpl_parameterlist_dump(parlist,stdout);
        //cpl_msg_info(cpl_func,"");

        cpl_parameterlist* iframe = cpl_parameterlist_new();
        cpl_error_code err= CPL_ERROR_NONE;
        err=xsh_molecfit_utils_find_input_frame(frameset, iframe);

        //const char* input_name = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"INPUTNAME"));
        //const char* arm = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"ARM"));
        //const char* obsmode = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"OBSMODE"));
        const char* is_idp = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"IDP"));
        //const char* fname = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"INPUTFILENAME"));

        //cpl_msg_info(cpl_func,"iframe details; INPUTNAME: %s; ARM: %s; IDP: %s; OBSMODE: %s; FILENAME: %s",input_name,arm,is_idp,obsmode,fname);

        //add iframe parameters (INPUTNAME,ARM,OBSMODE,IDP) to iframelist so that we can access them from xsh_molecfit_correct
        //these are not added to ilist, as they are only meant to be instrument dependent molecfit parameters
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"INPUTNAME"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"ARM"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"OBSMODE"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"IDP"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"INPUTFILENAME"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"ISTWOD"));

        //merge1d 
        const char* MAPPING_CORRECT;
        cpl_boolean USE_PRIMARY_DATA;
        int USE_DFLUX;
        //If IDP format
        if(!strcmp(is_idp,"TRUE")){
            MAPPING_CORRECT = "0,1";
            USE_DFLUX= 0;
            USE_PRIMARY_DATA = CPL_FALSE;
        } else {
            //if not IDP format
            MAPPING_CORRECT = "1";
            USE_PRIMARY_DATA= CPL_TRUE;
            USE_DFLUX = 1;
        }
        int USE_MASK = 0;
        cpl_boolean SUPPRESS_EXTENSION = CPL_FALSE;
        cpl_boolean CHIP_EXTENSIONS= CPL_FALSE;

        cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_MAPPING_CORRECT,CPL_TYPE_STRING,NULL,NULL,MAPPING_CORRECT));
        cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_ONLY_INPUT_PRIMARY_DATA",CPL_TYPE_BOOL,NULL,NULL,USE_PRIMARY_DATA));
        cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_DATA_EXTENSION_AS_DFLUX",CPL_TYPE_INT,NULL,NULL,USE_DFLUX));

        cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_CHIP_EXTENSIONS,CPL_TYPE_BOOL,NULL,NULL,CHIP_EXTENSIONS));
        cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_SUPPRESS_EXTENSION,CPL_TYPE_BOOL,NULL,NULL,SUPPRESS_EXTENSION));
        cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_DATA_EXTENSION_AS_MASK",CPL_TYPE_INT,NULL,NULL,USE_MASK));
        return err;
}
