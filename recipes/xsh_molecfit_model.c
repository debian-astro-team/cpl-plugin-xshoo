/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/* xshooter headers */
#include <xsh_error.h>
#include <xsh_utils.h>

/* Molecfit Model */
#include "xsh_molecfit_model.h"
#include <mf_wrap_config.h>
#include <telluriccorr.h>
//#include <mf_spectrum.h>
//#include <mf_wrap.h>
/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Enumeration types
 */
/*----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                             Recipe Defines
 ---------------------------------------------------------------------------*/

#define MODEL_RECIPE_ID "xsh_molecfit_model"
#define MODEL_RECIPE_AUTHOR "N. Fernando, B. Miszalski"
#define MODEL_RECIPE_CONTACT "nuwanthika.fernando@partner.eso.org"

/*---------------------------------------------------------------------------
                            Functions prototypes
 ---------------------------------------------------------------------------*/

/*
 *   Plugin initalization, execute and cleanup handlers
 */

int xsh_molecfit_model_create(cpl_plugin *);
int xsh_molecfit_model_exec(cpl_plugin *);
int xsh_molecfit_model_destroy(cpl_plugin *);

/* The actual executor function */
int xsh_molecfit_model(cpl_frameset *frameset, const cpl_parameterlist  *parlist);

/*----------------------------------------------------------------------------*/
/**
 *                 static variables
 */
/*----------------------------------------------------------------------------*/

char xsh_molecfit_model_description_short[] =
"Applies molecfit_model to input spectra";

char xsh_molecfit_model_description[] =
"Applies molecfit_model to input spectra";

/*----------------------------------------------------------------------------*/
/**
 *                 Macros
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Structured types
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 *                 Functions prototypes
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @defgroup molecfit_model  It runs Molecfit on a generic input spectrum file to compute an atmospheric model.
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
 *                              Functions code
 */
/*----------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module.
  @param    list    the plugin list
  @return   0 if everything is ok, -1 otherwise

  Create the recipe instance and make it available to the application using
  the interface. This function is exported.
 */
/*--------------------------------------------------------------------------*/

int cpl_plugin_get_info(cpl_pluginlist *list) {
  cpl_recipe *recipe = NULL;
  cpl_plugin *plugin = NULL;

  recipe = cpl_calloc(1, sizeof(*recipe));
  if ( recipe == NULL ){
    return -1;
  }

  plugin = &recipe->interface ;

  cpl_plugin_init(plugin,
                  CPL_PLUGIN_API,                   /* Plugin API */
                  XSH_BINARY_VERSION,            /* Plugin version */
                  CPL_PLUGIN_TYPE_RECIPE,           /* Plugin type */
                  MODEL_RECIPE_ID,                        /* Plugin name */
                  xsh_molecfit_model_description_short, /* Short help */
                  xsh_molecfit_model_description,   /* Detailed help */
                  MODEL_RECIPE_AUTHOR,                    /* Author name */
                  MODEL_RECIPE_CONTACT,                   /* Contact address */
                  xsh_get_license(),                /* Copyright */
                  xsh_molecfit_model_create,
                  xsh_molecfit_model_exec,
                  xsh_molecfit_model_destroy);

  cpl_pluginlist_append(list, plugin);

  return (cpl_error_get_code() != CPL_ERROR_NONE);
 }

/*--------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using
  the interface.

 */
/*--------------------------------------------------------------------------*/

int xsh_molecfit_model_create(cpl_plugin *plugin){
  cpl_recipe *recipe = NULL;
  /*xsh_clipping_param detarc_clip_param =  {2.0, 0, 0.7, 0, 0.3};
  xsh_detect_arclines_param detarc_param =
  {6, 3, 0, 5, 4, 1, 5, 5.0,
    XSH_GAUSSIAN_METHOD, FALSE};
  xsh_dispersol_param dispsol_param = { 4, 5 } ; 
  char paramname[256];
  cpl_parameter* p=NULL;
  int ival=DECODE_BP_FLAG_DEF;
  */

  /* Reset library state */
  xsh_init();

  /* Check input */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin");

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH,
          "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  assure( recipe->parameters != NULL,
          CPL_ERROR_ILLEGAL_OUTPUT,
          "Memory allocation failed!");

  //xsh_molecfit_model parameters

  //MOLECFIT_PARAMETER_LIST
  //list_molec
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_LIST,"NULL",
  XSH_MOLECFIT_PARAMETER_LIST_DESC));
   
  //MOLECFIT_PARAMETER_FIT
  //fit_molec
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_FIT,"NULL",
  XSH_MOLECFIT_PARAMETER_FIT_DESC));

  //MOLECFIT_PARAMETER_RELATIVE_VALUE
  //rel_col
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_RELATIVE_VALUE,"NULL",
  XSH_MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC)); 

  //MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE
  //wave_include
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE,"NULL",
  XSH_MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC));

  //MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE
  //wave_exclude
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE,"NULL",
  XSH_MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC));

  //MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE
  //pixel_exclude
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE,"NULL",
  XSH_MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC));

  //MF_PARAMETERS_COLUMN_LAMBDA
  //column_lambda
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_COLUMN_LAMBDA,"WAVE",
  XSH_MF_PARAMETERS_COLUMN_LAMBDA_DESC));

  //MF_PARAMETERS_COLUMN_FLUX
  //column_flux
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_COLUMN_FLUX,"FLUX",
  XSH_MF_PARAMETERS_COLUMN_FLUX_DESC));
  
  //MF_PARAMETERS_COLUMN_DFLUX
  //column_dflux
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_COLUMN_DFLUX,"ERR",
  XSH_MF_PARAMETERS_COLUMN_DFLUX_DESC));

  //MF_PARAMETERS_DEFAULT_ERROR
  //default_error
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_DEFAULT_ERROR,0.01,
  XSH_MF_PARAMETERS_DEFAULT_ERROR_DESC));

  //MF_PARAMETERS_FTOL
  //ftol
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FTOL,0.001,
  XSH_MF_PARAMETERS_FTOL_DESC));

  //MF_PARAMETERS_XTOL
  //xtol
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_XTOL,0.001,
  XSH_MF_PARAMETERS_XTOL_DESC));

  //MF_PARAMETERS_FIT_CONTINUUM
  //fit_continuum
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_CONTINUUM,"1",
  XSH_MF_PARAMETERS_FIT_CONTINUUM_DESC));

  //MF_PARAMETERS_CONTINUUM_N
  //continuum_n
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_CONTINUUM_N,"1",
  XSH_MF_PARAMETERS_CONTINUUM_N_DESC));

  //MF_PARAMETERS_FIT_WLC
  //fit_wlc
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_WLC,"1",
  XSH_MF_PARAMETERS_FIT_WLC_DESC));
  //MF_PARAMETERS_WLC_N
  //wlc_n
  check(xsh_parameters_new_int(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_WLC_N,2,
  XSH_MF_PARAMETERS_WLC_N_DESC));

  //MF_PARAMETERS_WLC_CONST
  //wlc_const
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_WLC_CONST,0.0,
  XSH_MF_PARAMETERS_WLC_CONST_DESC));

  //MF_PARAMETERS_FIT_RES_BOX
  //fit_res_box
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_RES_BOX,false,
  XSH_MF_PARAMETERS_FIT_RES_BOX_DESC));

  //MF_PARAMETERS_RES_BOX
  //res_box
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_RES_BOX,1.0,
  XSH_MF_PARAMETERS_RES_BOX_DESC));

  //MF_PARAMETERS_FIT_GAUSS
  //fit_res_gauss
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_GAUSS,true,
  XSH_MF_PARAMETERS_FIT_GAUSS_DESC));

  //MF_PARAMETERS_RES_GAUSS
  //res_gauss
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_RES_GAUSS,1.0,
  XSH_MF_PARAMETERS_RES_GAUSS_DESC));

  //MF_PARAMETERS_FIT_LORENTZ
  //fit_res_lorentz
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_LORENTZ,false,
  XSH_MF_PARAMETERS_FIT_LORENTZ_DESC));
  
  //MF_PARAMETERS_RES_LORENTZ
  //res_lorentz
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_RES_LORENTZ,2.0,
  XSH_MF_PARAMETERS_RES_LORENTZ_DESC));

  //MF_PARAMETERS_KERN_MODE
  //kernmode
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_KERN_MODE,false,
  XSH_MF_PARAMETERS_KERN_MODE_DESC));

  //MF_PARAMETERS_KERN_FAC
  //kernfac
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_KERN_FAC,3.0,
  XSH_MF_PARAMETERS_KERN_FAC_DESC));

  //MF_PARAMETERS_VAR_KERN
  //varkern
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_VAR_KERN,true,
  XSH_MF_PARAMETERS_VAR_KERN_DESC));

  //MOLECFIT_PARAMETER_USE_INPUT_KERNEL
  //use_input_kernel
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MOLECFIT_PARAMETER_USE_INPUT_KERNEL,true,
  XSH_MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC));

  //MF_PARAMETERS_FIT_TELESCOPE_BACK
  //fit_telescope_background
  check(xsh_parameters_new_boolean(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_FIT_TELESCOPE_BACK,false,
  XSH_MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC));

  //MF_PARAMETERS_TELESCOPE_BACK_CONST
  //telescope_background_const
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_TELESCOPE_BACK_CONST,0.1,
  XSH_MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC));

  //MF_PARAMETERS_PWV
  //pwv
  check(xsh_parameters_new_double(recipe->parameters,MODEL_RECIPE_ID,
  MF_PARAMETERS_PWV,-1.0,
  XSH_MF_PARAMETERS_PWV_DESC));

  //MF_LNFL_LINE_DB 
  //lnfl_line_db
  check(xsh_parameters_new_string(recipe->parameters,MODEL_RECIPE_ID,
  MF_LNFL_LINE_DB,MF_LNFL_LINE_DB_AER_VERSION,
  XSH_MF_LNFL_LINE_DB_DESC));


  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ){
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else {
      return 0;
    }
}


/*--------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/

int xsh_molecfit_model_exec(cpl_plugin *plugin) {
  cpl_recipe *recipe = NULL;

  /* Check parameter */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin" );

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;
  /* Check recipe */
  xsh_molecfit_model( recipe->frames, recipe->parameters);

  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ) {
      xsh_error_dump(CPL_MSG_ERROR);
      cpl_error_reset();
      return 1;
    }
    else {
      return 0;
    }
}

/*--------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/
int xsh_molecfit_model_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe = NULL;

    /* Check parameter */
    assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin" );

    /* Get the recipe out of the plugin */
    assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
            CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

    recipe = (cpl_recipe *)plugin;

    xsh_free_parameterlist(&recipe->parameters);

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
        {
            return 1;
        }
    else
        {
            return 0;
        }
}

cpl_error_code xsh_molecfit_setup_frameset(cpl_frameset* frameset,cpl_parameterlist* list,const char* arm,const char* input_name){

      const char* tag;
      //check the input frame (SCIENCE); we know the tag already thanks to the input_name parameter
      //so no need to search frameset for it
      //input_name may be one of the following:
      //SCI_SLIT_FLUX_IDP_XXX 
      //SCI_SLIT_FLUX_MERGE1D_XXX
      //TELL_SLIT_MERGE1D_XXX
      //TELL_SLIT_FLUX_MERGE1D_XXX
      //STD_SLIT_FLUX_IDP_YYY_XXX
      //where XXX is the arm and YYY is NOD, STARE or OFFSET

      tag = input_name;
      cpl_frame* f = cpl_frameset_find(frameset,input_name);
      cpl_frame_set_group(f,CPL_FRAME_GROUP_RAW);

      //cpl_frame_set_type(f,CPL_FRAME_TYPE_NONE);
      //cpl_frame_set_tag(f,"SCIENCE");
      //cpl_frame_set_level(f,CPL_FRAME_LEVEL_NONE);

      //other inputs 

      //KERNEL_LIBRARY_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_KERNEL_LIBRARY,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      //GDAS_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_GDAS,arm);
      //tag = MOLECFIT_GDAS;
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      } 
      //WAVE_INCLUDE_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_WAVE_INCLUDE,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      } 
      //WAVE_EXCLUDE_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_WAVE_EXCLUDE,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      //PIX_EXCLUDE_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_PIXEL_EXCLUDE,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      //MOLECULES_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_MOLECULES,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      //ATM_PROFILE_STANDARD_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_ATM_PROFILE_STANDARD,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      //ATM_PROFILE_COMBINED_XXX
      tag = cpl_sprintf("%s_%s",MOLECFIT_ATM_PROFILE_COMBINED,arm);
      f = cpl_frameset_find(frameset,tag);
      if(f){
            cpl_frame_set_group(f, CPL_FRAME_GROUP_CALIB);
      }
      return CPL_ERROR_NONE;
}



/*----------------------------------------------------------------------------*/
/**
 * @brief    Interpret the command line options and execute the data processing
 *
 * @param    frameset   the frames list
 * @param    parlist    the parameters list
 *
 * @return   CPL_ERROR_NONE if everything is OK or CPL_ERROR_CODE in other case
 *
 */
/*----------------------------------------------------------------------------*/

/*Instrument sid- example instrument - XSHOOTER?*/
int xsh_molecfit_model(cpl_frameset *frameset, const cpl_parameterlist  *parlist)
{
    //TODO: cpl_errorstate_ensure NOT DEFINED???

    //cpl_errorstate_ensure(frameset && parlist, CPL_ERROR_NULL_INPUT, return, CPL_ERROR_NULL_INPUT, 'NULL input: framset and/or parlist');
    cpl_error_code err= CPL_ERROR_NONE;
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    //INPUT FRAMES WILL BE NAMED DIFFERENT TO MOLECFIT_SCIENCE, MOLECFIT_STD* etc.
    //TODO: implement xsh_check_and_set_input_tags (if needed?)
    //err = xsh_check_and_set_input_tags(frameset); //INSTRUMENT_SPECIFIC
    //cpl_boolean* science_data_is_table;

    //err = mf_wrap_preconfig_check(frameset, parlist,science_data_is_table);/*Check parameters and frameset for correct inputs - returns CPL_ERROR*/

    cpl_parameterlist* ilist = cpl_parameterlist_new();
    cpl_parameterlist* iframelist = cpl_parameterlist_new();
    // get instrument specific parameter defaults
    err = xsh_molecfit_model_config(frameset,parlist,ilist,iframelist);
    //return err;
    //

    cpl_msg_info(cpl_func,"completed xsh_molecfit_model_config; %d : %s",err,cpl_error_get_message());
      //these are in iframelist, so they don't end up as input parameters for molecfit via 
      const char* input_name = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"INPUTNAME"));
      const char* arm = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"ARM"));
      //const char* obsmode = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"OBSMODE"));
      const char* is_idp = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"IDP"));
      const char* fname = cpl_parameter_get_string(cpl_parameterlist_find(iframelist,"INPUTFILENAME"));
      
      cpl_msg_info(cpl_func,"iframelist details: INPUTNAME: %s; ARM: %s; IDP: %s; INPUTFILENAME: %s;",input_name,arm,is_idp,fname);

      //setup some output filename suffixes

      //suffix == 'XXX' in xsh_molecfit_model specs
      const char* suffix = arm;
      //type_suffix == 'YYY' in xsh_molecfit_model specs (note that definition of YYY here is different to YYY of input frames)
      const char* type_suffix = "STD";
      if(strstr(input_name,"SCI")){
          type_suffix = "SCI";
      }
          

      //cpl_frame* input_frame = cpl_frameset_find(frameset,input_name);

      //BEFORE WE LOAD THE DATA (SPECTRUM), first load some params that we need to convert spectrum from mf_wrap_fits format into proper format (cpl_table)

      //parlist2 is NOW called mergedlist
      cpl_parameterlist* mergedlist = cpl_parameterlist_new();
      //TODO: Check merge function - does not currently work?
      cpl_msg_info(cpl_func,"Calling mf_wrap_merge_parameterlists");
      
      //ilist has instrument defaults...these should be ignored if param is provided...

      err = mf_wrap_merge_parameterlists(ilist, parlist,mergedlist);
/*      cpl_msg_info(cpl_func,"AFTER MERGE PARAM LISTS");
      cpl_msg_info(cpl_func,"ILIST");
      cpl_parameterlist_dump(ilist,stdout);
      cpl_msg_info(cpl_func,"PARLIST");
      cpl_parameterlist_dump(parlist,stdout);
      cpl_msg_info(cpl_func,"MERGEDLIST");
      cpl_parameterlist_dump(mergedlist,stdout);
      */
      cpl_msg_info(cpl_func,"Finished calling mf_wrap_merge_parameterlists %d: %s",err,cpl_error_get_message());


      err = cpl_error_get_code();
      cpl_msg_info(cpl_func,"ERR after mf_wrap_merge_parameterlists %d: %s",err,cpl_error_get_message());
      
      //setup framset tags
      //essential function to allow mf_wrap_save() to work!
      //could also sort out all input/output frameset issues in this function
      err = xsh_molecfit_setup_frameset(frameset,mergedlist,arm,input_name);
        //3. SETUP INPUT FILE NAME TAGS
        //cpl_msg_info(cpl_func,"calling mf_wrap_config_model_input_filenames");
        //err = mf_wrap_config_model_input_filenames(frameset,mergedlist,arm);
        //4. SETUP OUTPUT FILE NAME TAGS
        //cpl_msg_info(cpl_func,"calling mf_wrap_config_model_output_filenames");
        //N.B. This is not needed!
       //err = mf_wrap_config_model_output_filenames(frameset,mergedlist,arm,input_name);



/*
      cpl_msg_info(cpl_func,"PARLIST");
      cpl_parameterlist_dump(parlist,stdout);
      cpl_msg_info(cpl_func,"ILIST");
      cpl_parameterlist_dump(ilist,stdout);
      cpl_msg_info(cpl_func,"MERGEMF_DLIST");
      cpl_parameterlist_dump(mergedlist,stdout);
*/

      mf_wrap_fits* data;//= mf_wrap_fits_load(fname, CPL_FALSE);
      cpl_msg_info(cpl_func,"calling mf_wrap_fits_load");
	  data = mf_wrap_fits_load(fname, CPL_FALSE);
      /*if(data != NULL){
        cpl_msg_info(cpl_func,"n_ext %d",data->n_ext); 
      }*/

  
      //err = set_output_filename_suffix(arm, parlist); // This could be moved to instlist create function mf_inst_config();
  
      
      cpl_msg_info(cpl_func,"calling mf_wrap_config_model_init");
      cpl_msg_info(cpl_func,"property list of header size");
      cpl_msg_info(cpl_func,"size: %lld",cpl_propertylist_get_size(data->v_ext[0].header));
      mf_wrap_model_parameter* parameters = mf_wrap_config_model_init(mergedlist,data->v_ext[0].header);


      /*if(data == NULL){
          cpl_msg_info(cpl_func,"pre xsh_molecfit_model_spec_data_calcs: mf_wrap_fits* data == NULL");
      }*/
  
  
      //cpl_msg_info(cpl_func,"xsh_molecfit_model_spec_data_calcs");
  
      // - once data is loaded we need to prep_data for mf_model -- //
      // -- include the data checks in here  --- //
  
  
      // DO we extract the mf_wrap_data_convert_to_table() from mf_wrap_data() ?
      //NOTE - Are the parameters required to be already updated with the header values? //
  
      /*merging with header data */
      //cpl_error_code e = mf_parameters_config_update_with_header_keywords(parameters->mf_config->parameters, data->v_ext[0].header);
      //BRENT NOTES: mf_config->parameters does not compile
      //cpl_error_code e = mf_parameters_config_update_with_header_keywords(mf_config->parameters, data->v_ext[0].header);
     //NOTE- how do we hanlde CHIPS and RANGES as a part of this??
  
      // ---- move this into its own function later- OR - potentially at the end of mf_wrap_data ----//
      if (!err) {
          err = mf_wrap_data_convert_to_table( data,
                                                parameters->chip_extensions,
                                                parameters->use_only_input_pri_ext,
                                                parameters->dflux_extension_data,
                                                parameters->mask_extension_data,
                                                parameters->mf_config->parameters->inputs.column_lambda,
                                                parameters->mf_config->parameters->inputs.column_flux,
                                                parameters->mf_config->parameters->inputs.column_dflux,
                                                parameters->mf_config->parameters->inputs.column_mask);
          cpl_msg_info(cpl_func,"after 'mf_wrap_data_convert_to_table': %s",cpl_error_get_message());
          
          //Calculate CONTINUUM_CONST
          err = xsh_molecfit_model_spec_data_calcs(data,is_idp,mergedlist,parameters);
          cpl_msg_info(cpl_func,"after 'xsh_molecfit_model_spec_data_calcs': %s",cpl_error_get_message());
          //cpl_parameterlist_dump(mergedlist,stdout);

  
          //return err;
  
          /* Save input DATA --> Combined (molecfit_spectrum in BINTABLE format) */
          //Not relevant for xshooter 
          /*if (parameters->chip_extensions) {
  
              cpl_size index_combined = data->v_ext[0].spectrum_data ? 0 : 1;
              cpl_msg_info(cpl_func, "Save combined multi-extension molecfit_spectrum input FITS file ('%s') in ext=%lld ...", MOLECFIT_MODEL_CHIPS_COMBINED, index_combined);
  
              err += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, MOLECFIT_MODEL_CHIPS_COMBINED, NULL);
              if (!err) err = mf_wrap_save_mf_results(data->v_ext[index_combined].spectrum_head, MOLECFIT_MODEL_CHIPS_COMBINED, CPL_TRUE, NULL, data->v_ext[index_combined].spectrum_data, NULL);
          }*/
      }
      cpl_msg_info(cpl_func,"before 'mf_wrap_molecules': %s",cpl_error_get_message());

      // Load MOLECULES
      cpl_table* molecules = NULL;

      cpl_msg_info(cpl_func,"calling mf_wrap_molecules");
      //Data not found originating from mf_wrap_molecules!
      molecules = mf_wrap_molecules(frameset, data, mergedlist, parameters,suffix);
      //cpl_msg_info(cpl_func,"DUMP MOLECULES TABLE AFTER mf_wrap_molecules");
      //cpl_table_dump(molecules,0,cpl_table_get_nrow(molecules),stdout);

      cpl_msg_info(cpl_func,"after 'mf_wrap_molecules': %s",cpl_error_get_message());
      /* Load TAG = WAVE_INCLUDE, WAVE_EXCLUDE, PIXEL_EXCLUDE */
      cpl_table* inc_wranges = NULL;
      cpl_table* exc_wranges = NULL;
      cpl_table* exc_pranges = NULL;
    
      cpl_msg_info(cpl_func,"calling mf_wrap_wranges");
      err = mf_wrap_wranges(frameset, data, mergedlist, parameters, &inc_wranges, &exc_wranges, &exc_pranges,suffix);
      //cpl_msg_info(cpl_func,"nrows: inc_wranges %lld exc_wranges %lld exc_pranges %lld",cpl_table_get_nrow(inc_wranges),cpl_table_get_nrow(exc_wranges),cpl_table_get_nrow(exc_pranges));
      //when we are done with the tables, should probably cleanup like this (but later on)...
      //cpl_table_delete(inc_wranges);
      //cpl_table_delete(exc_wranges);
      //cpl_table_delete(exc_pranges);
      
  
      /* Load TAG : MODEL_KERNEL_LIBRARY/KERNEL_LIBRARY */
      mf_wrap_fits *kernel_data    = NULL;
      cpl_table     *mapping_kernel = NULL;
//
      err = mf_wrap_kernel(frameset, parameters, kernel_data, mapping_kernel,suffix);
//
//    /* Load TAG = GDAS */
      cpl_boolean gdas_write = CPL_FALSE;
      cpl_table   *gdas_user = NULL;
      cpl_msg_error(cpl_func,"before mf_wrap_gdas: %d %s", err, cpl_error_get_message());
//
      err = mf_wrap_gdas(frameset, parameters, gdas_user,suffix);
      cpl_msg_error(cpl_func,"after mf_wrap_gdas: %d %s", err, cpl_error_get_message());
  
      /* Load TAG = ATM_PROFILE_STANDARD */
      cpl_boolean atm_profile_standard_write = CPL_FALSE;
      cpl_table   *atm_profile_standard      = NULL;
      cpl_table   *atm_profile_combined      = NULL;
      cpl_msg_error(cpl_func,"before mf_wrap_atm : %d %s", err, cpl_error_get_message());
      const char* atm_suffix = cpl_sprintf("%s_%s",type_suffix,suffix);
  
      err = mf_wrap_atm(frameset, parameters, &atm_profile_standard,atm_suffix);//, &atm_profile_combined);
  
  
      cpl_msg_error(cpl_func,"after mf_wrap_atm : %d %s", err, cpl_error_get_message());
      /*** Save generic output files */
      if (!err) {
  
          cpl_msg_info(cpl_func, "Save generic multi-extension output FITS file ('%s','%s','%s') ...", MOLECFIT_ATM_PARAMETERS, MOLECFIT_BEST_FIT_MODEL, MOLECFIT_BEST_FIT_PARAMETERS);

          const char* combined_suffix = cpl_sprintf("%s_%s",type_suffix,suffix);
  
          const char* output_fname = mf_wrap_tag_suffix(MOLECFIT_ATM_PARAMETERS,combined_suffix,CPL_TRUE);
          const char* tag_name = mf_wrap_tag_suffix(MOLECFIT_ATM_PARAMETERS,combined_suffix,CPL_FALSE);
            
          err     += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, tag_name, output_fname);

          output_fname = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_PARAMETERS,combined_suffix,CPL_TRUE);
          tag_name = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_PARAMETERS,combined_suffix,CPL_FALSE);
          err     += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, tag_name,  output_fname);

          output_fname = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_MODEL,combined_suffix,CPL_TRUE);
          tag_name = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_MODEL,combined_suffix,CPL_FALSE);
          err     += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, tag_name,output_fname);
          
          output_fname = mf_wrap_tag_suffix("MOLECFIT_DATA",combined_suffix,CPL_TRUE);
          tag_name = mf_wrap_tag_suffix("MOLECFIT_DATA",combined_suffix,CPL_FALSE);
          err     += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, tag_name,output_fname);
         // cpl_msg_error(cpl_func,"after mf_wrap_save MOLECFIT_BEST_FIT_PARAMETERS: %d %s", err, cpl_error_get_message());
          //cpl_msg_error(cpl_func,"after mf_wrap_save MOLECFIT_BEST_FIT_MODEL: %d %s", err, cpl_error_get_message());
          //cpl_msg_error(cpl_func,"after mf_wrap_save MOLECFIT_DATA: %d %s", err, cpl_error_get_message());
  
  
          if (kernel_data) {
              output_fname = mf_wrap_tag_suffix(MOLECFIT_MODEL_KERNEL_LIBRARY,suffix,CPL_TRUE);
              tag_name = mf_wrap_tag_suffix(MOLECFIT_MODEL_KERNEL_LIBRARY,suffix,CPL_FALSE);
              err += mf_wrap_save(frameset, frameset, mergedlist, MODEL_RECIPE_ID, parameters->pl, tag_name,output_fname);
          }
      }
  
  
      int null;
      cpl_msg_error(cpl_func,"PRE-RECIPE EXECUTION STEP: %d %s", err, cpl_error_get_message());
  
      /*** Recipe execution ***/
      if (!err) {
          cpl_msg_error(cpl_func,"RECIPE EXECUTION STEP: %d %s", err, cpl_error_get_message());
  
          /* Execution extensions with spectral data in table data->v_ext[?].spectrum data  */
          /*  NOTE: If chip_extensions = TRUE then all spectra data from extensions with    */
          /*  index > 0 have been merged into the table data->v_ext[1].spectrum data        */
  
          cpl_size n_ext;
          if (     parameters->use_only_input_pri_ext) n_ext = 1;
          else if (parameters->chip_extensions    ) n_ext = data->v_ext[0].spectrum_data ? 1 : 2;
          else n_ext = data->n_ext;
  
          cpl_msg_error(cpl_func,"RECIPE n_ext: %lld %s", n_ext, cpl_error_get_message());
  
          if (!parameters->use_only_input_pri_ext && n_ext == 1){
                  cpl_msg_warning(cpl_func,"No data extensions found. USE_ONLY_INPUT_PRIMARY_DATA=FALSE and number of extensions ==  1");
          }
  
  
          /* --------------- */
          /* Execution Loops */
          /* --------------- */
          /* Iterate over all extensions in the data object that contain spectrum_data tables.*/
          for (cpl_size ext = 0; ext < n_ext && !err; ext++) {
  
              mf_model_results *results = NULL;
  
              /* Create input molecfit spec format */
              if (data->v_ext[ext].spectrum_data) {
  
                  /* Get kernel */
                  cpl_propertylist *header_kernel = NULL;
                  cpl_matrix       *kernel        = NULL;
                  if (kernel_data) {
  
                      if (mapping_kernel) {
                          //TODO: check whether MOLECFIT_MAPPING_KERNEL_EXT needs suffix or not - suspect not
                          cpl_size index_kernel_ext = cpl_table_get(mapping_kernel, MOLECFIT_MAPPING_KERNEL_EXT, ext, &null);
                          header_kernel = kernel_data->v_ext[index_kernel_ext].header;
                          kernel        = kernel_data->v_ext[index_kernel_ext].matrix;
                      } else {
                          header_kernel = kernel_data->v_ext[ext].header;
                          kernel        = kernel_data->v_ext[ext].matrix;
                      }
                  }
  
                  /* Get STD_MODEL/SCIENCE_CALCTRANS/SCIENCE extension header/table data */
                  cpl_msg_info(cpl_func, "Load spectrum, ext = %lld ...", ext);
                  cpl_table *molecfit_data = mf_spectrum_create(parameters->mf_config->parameters, data->v_ext[ext].spectrum_data);
  
  
  
                  /* If the chip_extension flag is false then molecfit_data is taken from a single extension as we need to test */
                  /* that all ranges assigned to this extension no have wavlength overlap with the data in this extension          */
                  if (!parameters->chip_extensions) {
                      double max_wave = cpl_table_get_column_max(molecfit_data,MF_COL_IN_LAMBDA);
                      double min_wave = cpl_table_get_column_min(molecfit_data,MF_COL_IN_LAMBDA);
                      // err= molecfit_model_check_extensions_and_ranges(ext,min_wave,max_wave,inc_wranges);
                      err = xsh_molecfit_model_check_extensions_and_ranges(ext,min_wave,max_wave,inc_wranges);
                      if(!err){
                        inc_wranges = cpl_table_extract_selected(inc_wranges);
                      }

                      //-------  ^ NOT IMPLEMENTED --------------------//
                  }
  
  
                  cpl_msg_error(cpl_func,"pre mf_model %s", cpl_error_get_message());
                  //cpl_table_dump(molecfit_data,0,cpl_table_get_nrow(molecfit_data),stdout);
  
                  if (!err) {
  
                      /* CALL MOLECFIT */
                      results = mf_model( parameters->mf_config,               /* mf_configuration       *config               */
                                          molecules,                           /* cpl_table              *molecules            */
                                          data->v_ext[ext].spectrum_head,      /* const cpl_propertylist *header_spec          */
                                          molecfit_data,                       /* const cpl_table        *spec                 */
                                          inc_wranges,                         /* cpl_table              *inc_wranges          */
                                          exc_wranges,                         /* cpl_table              *exc_wranges          */
                                          exc_pranges,                         /* cpl_table              *exc_pranges          */
                                          header_kernel,                       /* const cpl_propertylist *header_kernel        */
                                          kernel,                              /* const cpl_matrix       *kernel               */
                                          gdas_user,                           /* const cpl_table        *gdas_user            */
                                          atm_profile_standard,                /* const cpl_table        *atm_profile_standard */
                                          atm_profile_combined);               /* const cpl_table        *atm_profile_combined */
  
  
                      /* Cleanup */
                      cpl_table_delete(molecfit_data);
                      err = cpl_error_get_code();
  
                  }
  
  
                  if (!err) {
  
                      /* Write GDAS files on disk */
                      if (results && !gdas_write) {
                      	mf_gdas_write(results, mergedlist, gdas_user,  frameset, parameters, data,suffix);
                  }
  
  
                  /* Write ATM_PROFILE files on disk */
                  if (!err && results && !atm_profile_standard_write) {
                  	mf_atm_write(results, mergedlist, frameset, parameters, data, atm_profile_standard,suffix);
                  }
  
                  /* Save output kernel */
                  if (!err && kernel_data) {
  
                      cpl_matrix *kernel_matrix = NULL;
                      if (results) {
                          if (results->kernel) kernel_matrix = results->kernel;
                      }
                      const char* output_fname = mf_wrap_tag_suffix(MOLECFIT_MODEL_KERNEL_LIBRARY,suffix,CPL_TRUE);
                      err += mf_wrap_save_mf_results(header_kernel, output_fname, CPL_FALSE, kernel_matrix, NULL, NULL);
                  }
              }
  
              /* Save outputs : extensions with/without execute molecfit */
              if (!err && (parameters->use_only_input_pri_ext || ext > 0) ) {
  
  
              	cpl_table *atm_profile_fitted = results ? results->atm_profile_fitted : NULL;
                  cpl_table *best_fit_params    = results ? results->res                : NULL;
                  cpl_table *best_fit_model     = results ? results->spec               : NULL;
  
                  const char* combined_suffix = cpl_sprintf("%s_%s",type_suffix,suffix);
                  const char* output_fname = mf_wrap_tag_suffix(MOLECFIT_ATM_PARAMETERS,combined_suffix,CPL_TRUE);
                  err += mf_wrap_save_mf_results(data->v_ext[ext].header, output_fname,CPL_FALSE, NULL, atm_profile_fitted, NULL);
                  output_fname = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_PARAMETERS,combined_suffix,CPL_TRUE);
                  err += mf_wrap_save_mf_results(data->v_ext[ext].header, output_fname, CPL_FALSE, NULL, best_fit_params,    NULL);
                  output_fname = mf_wrap_tag_suffix(MOLECFIT_BEST_FIT_MODEL,combined_suffix,CPL_TRUE);
                  err += mf_wrap_save_mf_results(data->v_ext[ext].header, output_fname,CPL_FALSE, NULL, best_fit_model,     NULL);
                  if (results) {
                      output_fname = mf_wrap_tag_suffix("MOLECFIT_DATA",combined_suffix,CPL_TRUE);
                      err += mf_wrap_save_mf_results(data->v_ext[ext].header,output_fname,CPL_FALSE, NULL, results->spec,     NULL);
                  }
  
  
              }
  
              /* Cleanup mf_model(...) results */
              if (results) mf_model_results_delete(results);
  
          }
          /* ----------------------- */
          /* END of Execution Loops */
          /* ---------------------- */
  
        }/* End if (!err) */
      }
  
      /* Cleanup */
      if (parameters          ) mf_wrap_model_parameter_delete( parameters           );
      if (data                ) mf_wrap_fits_delete(            data                 );
      if (molecules           ) cpl_table_delete(                molecules            );
      if (inc_wranges         ) cpl_table_delete(                inc_wranges          );
      if (exc_wranges         ) cpl_table_delete(                exc_wranges          );
      if (exc_pranges         ) cpl_table_delete(                exc_pranges          );
      if (kernel_data         ) mf_wrap_fits_delete(            kernel_data          );
      if (mapping_kernel      ) cpl_table_delete(                mapping_kernel       );
      if (gdas_user           ) cpl_table_delete(                gdas_user            );
      if (atm_profile_standard) cpl_table_delete(                atm_profile_standard );
      if (atm_profile_combined) cpl_table_delete(                atm_profile_combined );
  
  
      /* Check Recipe status and end */
      if (!err && cpl_errorstate_is_equal(initial_errorstate)) {
          cpl_msg_info(cpl_func,"Recipe successfully!");
      } else {
          /* Dump the error history */
          cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);
          cpl_msg_error(cpl_func,"Recipe failed!, error(%d)=%s", err, cpl_error_get_message());
      }
  
      return err;

}


cpl_error_code xsh_molecfit_model_spec_data_calcs(mf_wrap_fits* data, const char* is_idp, cpl_parameterlist* ilist,mf_wrap_model_parameter* parameters){
    //work out which extension to access for the spectrum

	cpl_error_code err = CPL_ERROR_NONE;
	cpl_boolean USE_ONLY_INPUT_PRIMARY_DATA = CPL_TRUE;
    if(!strcmp(is_idp,"TRUE")){
        USE_ONLY_INPUT_PRIMARY_DATA = CPL_FALSE;
    }
    

    //code from molecfit_model - to work out which is which...
    //unused //cpl_size n_ext;
    int spec_ext = 0;
    //TO LATER TEST/CUSTOMISE PER XSHOOTER DATA FORMATS...
    if(USE_ONLY_INPUT_PRIMARY_DATA){
        spec_ext = 0;
    } else {
        spec_ext = 1;
    }
    cpl_msg_info(cpl_func,"spec_ext: %d",spec_ext);
    //cpl_msg_info(cpl_func,"flux column: %s",MF_PARAMETERS_COLUMN_FLUX);
    //cpl_table* spectrum = data->v_ext[spec_ext].spectrum_data;
    //algorithm to work out the median continuum flux (perhaps RETURN LATER to do some sigma clipping)
    //median of flux - XSHOOTER_INPUT_DATA_FLUX_COLUMN is defined in xsh_molecfit_model.h
    //TODO: check input data and see what it uses
    //if(data == NULL){
    //    cpl_msg_info(cpl_func,"mf_wrap_fits* data == NULL");
   // }

    cpl_size nrows = cpl_table_get_nrow(data->v_ext[spec_ext].spectrum_data);
    cpl_msg_info(cpl_func,"nrows: %lld",nrows); 
    //cpl_table_dump(data->v_ext[spec_ext].spectrum_data,0,nrows,stdout);

    const char* FLUX_COLUMN = cpl_parameter_get_string(cpl_parameterlist_find(ilist,MF_PARAMETERS_COLUMN_FLUX));
    cpl_msg_info(cpl_func,"flux column: %s",FLUX_COLUMN);

    //TODO: MUST REMOVE NANs HERE...HOW TO DO THAT?

    double median = cpl_table_get_column_median(data->v_ext[spec_ext].spectrum_data,FLUX_COLUMN);
    cpl_msg_info(cpl_func,"spectrum median: %.4e",median); 

    //cpl_propertylist_update_string(ilist,"CONTINUUM_CONST",cpl_sprintf("%.4e",median));
    cpl_parameterlist_append(ilist,cpl_parameter_new_value(MF_PARAMETERS_CONTINUUM_CONST,CPL_TYPE_DOUBLE,NULL,NULL,median));
    //because we are setting up CONTINUUM_CONST **AFTER** mf_wrap_config_model_init, it will be missing from the parameters...
    //update that here...(could also be done in xsh_molecfit_model(), but it's neater and more self-contained to have it here 
    mf_parameters_fitting      *fitting = &(parameters->mf_config->parameters->fitting);
    fitting->fit_continuum.const_val = median;
    cpl_propertylist_update_double(parameters->pl, MF_PARAMETERS_CONTEX_DEFAULT" "MF_PARAMETERS_CONTINUUM_CONST, fitting->fit_continuum.const_val);
    parameters->mf_config->parameters->fitting = *fitting;

    return err;
}


cpl_error_code xsh_molecfit_model_spec_header_calcs(const char* fname,const char* arm, cpl_parameterlist* ilist){

    cpl_msg_info(cpl_func,"xsh_molecfit_model_spec_header_calcs");
    //const char* fname =  cpl_frame_get_filename(f);
    cpl_msg_info(cpl_func,"reading in %s",fname);
    //assume header info is in first extension 
    cpl_size ext = 0;
    //cpl_size n_ext = cpl_frame_get_nextensions(f);
    //Is this always in the first extension????
    //for (cpl_size ext=0;ext< n_ext;ext++){
    cpl_propertylist* hdr= cpl_propertylist_load(fname, ext);
    if(hdr){
        //SLIT_WIDTH_VALUE
        const char* slit_width_kw = "NONE";
        if(!strcmp(arm,"UVB")){
            slit_width_kw = "ESO INS OPTI3 NAME";
        } else if(!strcmp(arm,"VIS")){
            slit_width_kw = "ESO INS OPTI4 NAME";
        } else if(!strcmp(arm,"NIR")){
            slit_width_kw = "ESO INS OPTI5 NAME";
        }
        cpl_msg_info(cpl_func,"to read in %s value",slit_width_kw);
        cpl_property *prop_hval = cpl_propertylist_get_property(hdr, slit_width_kw);
        if(prop_hval){
            const char* hval = cpl_property_get_string(prop_hval);
            if(hval){
                cpl_msg_info(cpl_func,"SLIT_WIDTH_VALUE: %s (read from %s)",strtok(hval,"x"),slit_width_kw);
                //if the keyword value is 0.5x11, return 0.5 as slit width
                //write to SLIT_WIDTH_VALUE parameter
                //set to NONE to ensure the SLIT_WIDTH_VALUE parameter value is utilised
                //cpl_propertylist_update_string(ilist,"SLIT_WIDTH_KEYWORD","NONE");
                //cpl_propertylist_update_string(ilist,"SLIT_WIDTH_VALUE",strtok(hval,"x"));
                cpl_parameterlist_append(ilist,cpl_parameter_new_value("SLIT_WIDTH_KEYWORD",CPL_TYPE_STRING,NULL,NULL,"NONE"));
                cpl_parameterlist_append(ilist,cpl_parameter_new_value("SLIT_WIDTH_VALUE",CPL_TYPE_DOUBLE,NULL,NULL,atof(strtok(hval,"x"))));
            }
        }
        //WAVELENGTH_FRAME *AND* OBS_ERF_RV_KEY
        const char* WAVELENGTH_FRAME = "AIR";
        const char* OBS_ERF_RV_KEY = "NONE";

        cpl_property *prop_tu = cpl_propertylist_get_property(hdr,"TUCD1");
        cpl_property *prop_ss = cpl_propertylist_get_property(hdr,"SPECSYS");
        cpl_property *prop_hb = cpl_propertylist_get_property(hdr,"ESO QC VRAD BARYCOR");
        cpl_property *prop_hh = cpl_propertylist_get_property(hdr,"ESO QC VRAD HELICOR");
        //cpl_msg_info(cpl_func,"HDR values: TUCD1: %s; SPECSYS: %s; ESO QC VRAD BARYCOR: %s; ESO QC VRAD HELICOR: %s;",
        //cpl_property_get_string(prop_tu),cpl_property_get_string(prop_ss),cpl_property_get_string(prop_hb),cpl_property_get_string(prop_hh));


        if(prop_tu){
            const char* val = cpl_property_get_string(prop_tu);
            cpl_msg_info(cpl_func,"TUCD1 found: %s",val);
            if(!strcmp(val,"em.wl;obs.atmos")){
                WAVELENGTH_FRAME="AIR";
            } else if(!strcmp(val,"em.wl")){
                WAVELENGTH_FRAME="VAC";
            }
        } else {
            //if TUCD1 not found, set to AIR (i.e. same as TUCD1 = em.wl;obs.atmos)
            WAVELENGTH_FRAME="AIR";
        }

        if(prop_ss){
            const char* val = cpl_property_get_string(prop_ss);
            cpl_msg_info(cpl_func,"SPECSYS found: %s",val);
            if(!strcmp(val,"TOPOCENT")){
                OBS_ERF_RV_KEY = "NONE";
            } else if(!strcmp(val,"BARYCENT")){
                OBS_ERF_RV_KEY = "ESO QC VRAD BARYCOR";
                WAVELENGTH_FRAME = cpl_sprintf("%s_RV",WAVELENGTH_FRAME);
            } else if(!strcmp(val,"HELIOCENT")){
                OBS_ERF_RV_KEY = "ESO QC VRAD HELICOR";
                WAVELENGTH_FRAME = cpl_sprintf("%s_RV",WAVELENGTH_FRAME);
            }
        } //else {
            //if SPECSYS not found, set to NONE (i.e. same as SPECSYS = TOPOCENT)
           // OBS_ERF_RV_KEY = "NONE";
       // }
        /*if(prop_hb){
            double val = cpl_property_get_double(prop_hb);
            cpl_msg_info(cpl_func,"ESO QC VRAD BARYCOR found: %.2f",val);
        }
        if(prop_hh){
            double val = cpl_property_get_double(prop_hh);
            cpl_msg_info(cpl_func,"ESO QC VRAD HELICOR found: %.2f",val);
        }*/

/*        if(!prop_hb || !prop_hh){
            OBS_ERF_RV_KEY = "NONE";
        }
*/
        if(WAVELENGTH_FRAME){
            //cpl_propertylist_update_string(ilist,"WAVELENGTH_FRAME",WAVELENGTH_FRAME);
            cpl_msg_info(cpl_func,"WAVELENGTH_FRAME: %s",WAVELENGTH_FRAME);
            cpl_parameterlist_append(ilist,cpl_parameter_new_value("WAVELENGTH_FRAME",CPL_TYPE_STRING,NULL,NULL,WAVELENGTH_FRAME));

        }
        if(OBS_ERF_RV_KEY){
            cpl_msg_info(cpl_func,"OBS_ERF_RV_KEY: %s",OBS_ERF_RV_KEY);
            //cpl_propertylist_update_string(ilist,"OBS_ERF_RV_KEY",OBS_ERF_RV_KEY);
            cpl_parameterlist_append(ilist,cpl_parameter_new_value("OBS_ERF_RV_KEY",CPL_TYPE_STRING,NULL,NULL,OBS_ERF_RV_KEY));
        }
    }

    //CONTINUUM_CONST
    //calculate median of spectrum FLUX values...
    //this is handled in xsh_molecfit_model_spec_data_calcs()

    //cleanup - cleanup cpl_property variables too?
    if (hdr) {
    	cpl_propertylist_delete(hdr);
    }


    return CPL_ERROR_NONE;

}



/* -----------------------------------------------------------------------------------------------------/
 *
 * OVERVIEW OF STEPS
 * 		1. DO SOME ERROR CHECKING ON FRAMESET? (may be done later in setup config, but just being thorough)
 * 			1.5 GET *TAG* OF INPUT SCIENCE from FRAMSET using either MOLECFIT_STD_MODEL, MOLECFIT_SCIENCE_CALCTRANS or MOLECFIT_SCIENCE
 * 				1.55 (Check for YYY in this FILENAME, in case we need it later...)
 * 		2. DETERMINE IF SUFFIX NEEDED FROM DATASET (e.g. UVB/VIS/NIR for xshooter)
 * 		3. SETUP INPUT FILE NAME TAGS
 * 		4. SETUP OUTPUT FILE NAME TAGS
 * 		5. SETUP PARAMETERS (e.g. --list_molec, etc.)
 * 			N.B. We expect to be provided a FITS binary table of WAVE_INCLUDE & MOLECULES params,
 * 			but FOR NOW, we setup these values internally via the string parameter versions.
 * 			6. SETUP *EXTRA* PARAMETERS
 *
 *
-------------------------------------------------------------------------------------------------------*/

cpl_error_code xsh_molecfit_model_config(cpl_frameset *frameset, const cpl_parameterlist  *parlist,
    		cpl_parameterlist* ilist, cpl_parameterlist* iframelist){

        cpl_msg_info(cpl_func,"xsh_molecfit_model_config");
        cpl_msg_info(cpl_func,"FRAMESET");
        //cpl_frameset_dump(frameset,stdout);
        cpl_msg_info(cpl_func,"PARLIST");
        cpl_parameterlist_dump(parlist,stdout);
        //cpl_msg_info(cpl_func,"");

        cpl_parameterlist* iframe = cpl_parameterlist_new();
        //INPUTNAME == input tag name
        //ARM == UVB, VIS, NIR
        //OBSMODE == NOD, STARE or OFFSET; If none of these, it is set to DEFAULT
        //IDP (bool) == TRUE or FALSE
        //1. Get all the input params into the iframe parameterlist (INPUTNAME,ARM,OBSMODE,IDP)
        cpl_error_code err= CPL_ERROR_NONE;
        //cpl_msg_info(cpl_func,"pre xsh_molecfit_utils_find_input_frame; %d : %s",err,cpl_error_get_message());
        err=xsh_molecfit_utils_find_input_frame(frameset, iframe);

        //cpl_msg_info(cpl_func,"post xsh_molecfit_utils_find_input_frame; %d : %s",err,cpl_error_get_message());
      //  if(err){
            //do something
      //  }
        //read in the user given params
        //const char* val = cpl_parameter_get_string(cpl_parameterlist_find(parlist,"xsh.xsh_molecfit_model.list_molec"));
        //convert command line args to molecfit parameters
        //cpl_parameter *p;
        //const char* prefix = "xsh.xsh_molecfit_model.";
        //p = cpl_parameterlist_find(cpl_sprintf("%s%s",prefix,"list_molec"));
        //if(p){
        //    LIST_MOLEC



        const char* input_name = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"INPUTNAME"));
        const char* arm = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"ARM"));
        const char* obsmode = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"OBSMODE"));
        const char* is_idp = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"IDP"));
        const char* fname = cpl_parameter_get_string(cpl_parameterlist_find(iframe,"INPUTFILENAME"));

        cpl_msg_info(cpl_func,"iframe details; INPUTNAME: %s; ARM: %s; IDP: %s; OBSMODE: %s; INPUTFILENAME: %s",input_name,arm,is_idp,obsmode,fname);

        //add iframe parameters (INPUTNAME,ARM,OBSMODE,IDP) to iframelist so that we can access them from xsh_molecfit_model
        //these are not added to ilist, as they are only meant to be instrument dependent molecfit parameters
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"INPUTNAME"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"ARM"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"OBSMODE"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"IDP"));
        err = cpl_parameterlist_append(iframelist,cpl_parameterlist_find(iframe,"INPUTFILENAME"));

        //cpl_msg_info(cpl_func,"finishing up xsh_molecfit_model list_molec: %s",val);


        //return err;

          //5. SETUP PARAMETERS (e.g. --list_molec, etc.)
          ///UVB - if we are using the UVB arm, just assume defaults for now.
          //(We may define specific values for its parameters later)
          /*By default, these are all NULL -- just to spell it out here, so we remind ourselves that we are satisfying the requirements*/
  
          const char* LIST_MOLEC;
          const char* FIT_MOLEC;
          const char* REL_COL;
          const char* WAVE_INCLUDE;
          const char* MAP_REGIONS_TO_CHIP;
          //const char* FIT_CONTINUUM;
          //const char* FIT_WLC;
          //const char* CONTINUUM_N;
          //int WLC_N;
          cpl_boolean USE_ONLY_INPUT_PRIMARY_DATA;
          int USE_DATA_EXTENSION_AS_DFLUX;
          int USE_DATA_EXTENSION_AS_MASK;
          double WLG_TO_MICRON;
          cpl_boolean TRANSMISSION;
          //const char* WAVE_EXCLUDE;
          //const char* PIXEL_EXCLUDE;

          //need to add mapping kernel here... + any other (internal ones) as placeholders
          //place to setup INTERNAL PARAMETERS...
          //basically - any parameters we want configurable via input rc file, we need to define in xsh_molecfit_model_create()
          //else - define here...
          //if this is NOT added here for MOLECFIT_MODEL_MAPPING_KERNEL, then in mf_wrap_config_model_init() the cpl_parameter_get_string()
          //function fails and segfaults
          const char* input_tag;
          input_tag = mf_wrap_tag_suffix(MOLECFIT_KERNEL_LIBRARY,arm,CPL_FALSE);
          if(!cpl_frameset_find(frameset,input_tag)){
            cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_MODEL_MAPPING_KERNEL,CPL_TYPE_STRING,NULL,NULL,"NULL"));
            cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_USE_INPUT_KERNEL,CPL_TYPE_BOOL,NULL,NULL,false));
          }
          //if we have an actual input kernel, we will have to set MOLECFIT_MODEL_MAPPING_KERNEL!


          //if the sof does not have WAVE_EXCLUDE_XXX, add this default param
          input_tag = mf_wrap_tag_suffix(MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE,arm,CPL_FALSE);
          if(!cpl_frameset_find(frameset,input_tag)){
            cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE,CPL_TYPE_STRING,NULL,NULL,"NULL"));
          }
          //if the sof does not have PIX_EXCLUDE_XXX, add this default param
          input_tag = mf_wrap_tag_suffix(MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE,arm,CPL_FALSE);
          if(!cpl_frameset_find(frameset,input_tag)){
            cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE,CPL_TYPE_STRING,NULL,NULL,"NULL"));
          }
          //need to add this so parameters->chip_extensions  works 
          cpl_parameterlist_append(ilist,cpl_parameter_new_value(MOLECFIT_PARAMETER_CHIP_EXTENSIONS,CPL_TYPE_BOOL,NULL,NULL,false));
         
          //if the sof does not have GDAS_XXX, add this default param
          input_tag = mf_wrap_tag_suffix(MOLECFIT_GDAS,arm,CPL_FALSE);
          if(!cpl_frameset_find(frameset,input_tag)){
            cpl_parameterlist_append(ilist,cpl_parameter_new_value(MF_PARAMETERS_GDAS_PROFILE,CPL_TYPE_STRING,NULL,NULL,"auto"));
          }

          cpl_parameterlist_append(ilist,cpl_parameter_new_value(MF_PARAMETERS_EXPERT_MODE,CPL_TYPE_BOOL,NULL,NULL,CPL_FALSE));

          TRANSMISSION = CPL_TRUE;
          cpl_parameterlist_append(ilist,cpl_parameter_new_value(MF_PARAMETERS_TRANSMISSION,CPL_TYPE_BOOL,NULL,NULL,TRANSMISSION));

  
          if(!strcmp(arm,"NIR")){
              LIST_MOLEC = "H2O,CO2,CO,CH4,O2";
              FIT_MOLEC = "1,1,0,0,0";
              REL_COL = " 1,1.06,1,1,1";
              WAVE_INCLUDE = "1.16,1.20,1.47,1.48,1.77,1.78,2.06,2.07,2.35,2.36";
              //MAPPED_TO_CHIP
              MAP_REGIONS_TO_CHIP = "1";
              //CONT_FIT_FLAG
              //FIT_CONTINUUM = "1";
              //FIT_WLC="1";
              //CONT_POLY_ORDER
              //CONTINUUM_N = "1";
          }
          if(!strcmp(arm,"VIS")){
              LIST_MOLEC = "H2O,O2";
              FIT_MOLEC = "1,1";
              REL_COL = " 1,1";
              WAVE_INCLUDE = "0.686,0.694,0.725,0.730,0.970,0.980";
              //MAPPED_TO_CHIP
              MAP_REGIONS_TO_CHIP = "1";
              //CONT_FIT_FLAG
              //FIT_CONTINUUM = "1";
              //FIT_WLC= "1";
              //CONT_POLY_ORDER
              //CONTINUUM_N = "1";
          }
          const char* default_list_molec = cpl_parameter_get_string(cpl_parameterlist_find(parlist,"LIST_MOLEC"));
          const char* default_fit_molec = cpl_parameter_get_string(cpl_parameterlist_find(parlist,"FIT_MOLEC"));
          const char* default_rel_col = cpl_parameter_get_string(cpl_parameterlist_find(parlist,"REL_COL"));
          const char* default_wave_include = cpl_parameter_get_string(cpl_parameterlist_find(parlist,"WAVE_INCLUDE"));

          /* Set arm dependent parameters */
          cpl_msg_info(cpl_func,"setting arm dependent parameters");
          if(!strcmp(arm,"VIS") || !strcmp(arm,"NIR")){
              //if MOLECULES_XXX is specified in sof, don't set these params...
              input_tag = mf_wrap_tag_suffix("MOLECULES",arm,CPL_FALSE);
              if(!cpl_frameset_find(frameset,input_tag) && (!strcmp(default_list_molec,"NULL") && !strcmp(default_fit_molec,"NULL") && !strcmp(default_rel_col,"NULL") )){
                  cpl_parameterlist_append(ilist,cpl_parameter_new_value("LIST_MOLEC",CPL_TYPE_STRING,NULL,NULL,LIST_MOLEC));
                  cpl_parameterlist_append(ilist,cpl_parameter_new_value("FIT_MOLEC",CPL_TYPE_STRING,NULL,NULL,FIT_MOLEC));
                  cpl_parameterlist_append(ilist,cpl_parameter_new_value("REL_COL",CPL_TYPE_STRING,NULL,NULL,REL_COL));
              }
              input_tag = mf_wrap_tag_suffix("WAVE_INCLUDE",arm,CPL_FALSE);
              //to set 
              //set default if 
              //1. param == NULL and tag not in frameset (i.e. user has not set param)
              //do not set if 
              //2. param == NULL and tag in framset 
              //3. param != NULL (and tag missing from framset) - the user has set some regions
              if(!cpl_frameset_find(frameset,input_tag) && !strcmp(default_wave_include,"NULL")){
                  cpl_parameterlist_append(ilist,cpl_parameter_new_value("WAVE_INCLUDE",CPL_TYPE_STRING,NULL,NULL,WAVE_INCLUDE));
              }

              cpl_parameterlist_append(ilist,cpl_parameter_new_value("MAP_REGIONS_TO_CHIP",CPL_TYPE_STRING,NULL,NULL,MAP_REGIONS_TO_CHIP));
              //cpl_parameterlist_append(ilist,cpl_parameter_new_value("FIT_CONTINUUM",CPL_TYPE_STRING,NULL,NULL,FIT_CONTINUUM));
              //cpl_parameterlist_append(ilist,cpl_parameter_new_value("FIT_WLC",CPL_TYPE_STRING,NULL,NULL,FIT_WLC));
              //cpl_parameterlist_append(ilist,cpl_parameter_new_value("CONTINUUM_N",CPL_TYPE_STRING,NULL,NULL,CONTINUUM_N));
  
              /*
              cpl_propertylist_update_string(ilist,"LIST_MOLEC",LIST_MOLEC);
              cpl_propertylist_update_string(ilist,"FIT_MOLEC",FIT_MOLEC);
              //MOLECFIT_PARAMETER_RELATIVE_VALUE => MF_COL_REL_COL
              cpl_propertylist_update_string(ilist,"REL_COL",REL_COL);
              cpl_propertylist_update_string(ilist,"WAVE_INCLUDE",WAVE_INCLUDE);
              cpl_propertylist_update_string(ilist,"MAP_REGIONS_TO_CHIP",MAP_REGIONS_TO_CHIP);
              cpl_propertylist_update_string(ilist,"FIT_CONTINUUM",FIT_CONTINUUM);
              cpl_propertylist_update_string(ilist,"CONTINUUM_N",CONTINUUM_N);
              */
  
          }
  
          const char* PIX_SCALE_KEYWORD = "NONE";
          double PIX_SCALE_VALUE;
          //0.16 was in xshooter molecfit params doc/requirements
          //using actual values from https://www.eso.org/sci/facilities/paranal/instruments/xshooter/inst.html
          if(!strcmp(arm,"UVB")){
              PIX_SCALE_VALUE = 0.161;
          }
          if(!strcmp(arm,"NIR")){
              PIX_SCALE_VALUE = 0.158;
          }
          if(!strcmp(arm,"NIR")){
              //0.20 was in xshooter molecfit params doc/requirements
              //PIX_SCALE_VALUE = "0.20";
              //actual value from https://www.eso.org/sci/facilities/paranal/instruments/xshooter/inst.html
              PIX_SCALE_VALUE = 0.248;
          }
          //cpl_propertylist_update_string(ilist,"PIX_SCALE_KEYWORD",PIX_SCALE_KEYWORD);
          //cpl_propertylist_update_string(ilist,"PIX_SCALE_VALUE",PIX_SCALE_VALUE);
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("PIX_SCALE_KEYWORD",CPL_TYPE_STRING,NULL,NULL,PIX_SCALE_KEYWORD));
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("PIX_SCALE_VALUE",CPL_TYPE_DOUBLE,NULL,NULL,PIX_SCALE_VALUE));
  
  
          /* Set arm independent parameters */
          //WLC_N = 2;
          //cpl_propertylist_update_string(ilist,"WLC_N",WLC_N);
          //cpl_parameterlist_append(ilist,cpl_parameter_new_value("WLC_N",CPL_TYPE_INT,NULL,NULL,WLC_N));
  
          //if not IDP, this is TRUE
          USE_ONLY_INPUT_PRIMARY_DATA = CPL_TRUE;
          if(!strcmp(is_idp,"TRUE")){
              USE_ONLY_INPUT_PRIMARY_DATA = CPL_FALSE;
          }
          //cpl_propertylist_update_string(ilist,"USE_ONLY_INPUT_PRIMARY_DATA",USE_ONLY_INPUT_PRIMARY_DATA);
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_ONLY_INPUT_PRIMARY_DATA",CPL_TYPE_BOOL,NULL,NULL,USE_ONLY_INPUT_PRIMARY_DATA));
  
  
  
          //FOR ALL CASES, even if IDP or not
          USE_DATA_EXTENSION_AS_DFLUX = 1;
          //cpl_propertylist_update_string(ilist,"USE_DATA_EXTENSION_AS_DFLUX",USE_DATA_EXTENSION_AS_DFLUX);
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_DATA_EXTENSION_AS_DFLUX",CPL_TYPE_INT,NULL,NULL,USE_DATA_EXTENSION_AS_DFLUX));
  
          //default is 0 - do not need to set?
          USE_DATA_EXTENSION_AS_MASK = 0;
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("USE_DATA_EXTENSION_AS_MASK",CPL_TYPE_INT,NULL,NULL,USE_DATA_EXTENSION_AS_MASK));
  
          WLG_TO_MICRON = 1e-3;//e-3;//0.001;
          //cpl_propertylist_update_string(ilist,"WLG_TO_MICRON",WLG_TO_MICRON);
          cpl_parameterlist_append(ilist,cpl_parameter_new_value("WLG_TO_MICRON",CPL_TYPE_DOUBLE,NULL,NULL,WLG_TO_MICRON));
  

          
          //deal with parameters that require access to input file header....
          //cpl_frame* input_frame = cpl_frameset_find(frameset,input_name);
          //const char* fname =  cpl_frame_get_filename(input_frame);
          //cpl_msg_info(cpl_func,"fname: %s",fname);
          //maybe the pointer is stale or something; just try passing the framset and tag - retrieve inside the function.
          cpl_msg_info(cpl_func,"calling xsh_molecfit_model_spec_header_calcs with fname = %s",fname);
          err = xsh_molecfit_model_spec_header_calcs(fname,arm,ilist);
  
          //CONTINUUM_CONST
          //this is handled in xsh_molecfit_model_spec_data_calcs()
  
        //free up iframe
          //have to do this at the end, otherwise strings (e.g. fname) get deleted
          //cpl_parameterlist_delete(iframe);
  
          return err;
 }


//cpl_error xsh_check_and_set_input_tags(frameset){
	//map the XSHOOTER input frame tags to MOLECFIT SCIENCE etc.

	//check the frameset for the following tags =>
	//SCI_SLIT_FLUX_IDP_XXX, SCI_SLIT_FLUX_MERGE1D_XXX,
	//TELL_SLIT_FLUX_MERGE_XXX, TELL_SLIT_MERGE1D_XXX,
	//STD_SLIT_FLUX_IDP_YYY_XXX
	//where XXX = VIS/NIR/ UBB, and YYY= NOD, STARE, OFFSET

	//call a function in Telluriccorr that maps out the input frame names to molecfit defaults();


//}


/// -----------------------------------INSTRUMENT SIDE END-----------------------------------///
