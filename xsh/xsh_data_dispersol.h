/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_DISPERSOL_H
#define XSH_DATA_DISPERSOL_H

#include <cpl.h>
#include <xsh_data_instrument.h>
#include <xsh_data_pre.h>

#define XSH_DISPERSOL_TABLE_NBCOL 4
#define XSH_DISPERSOL_TABLE_COLNAME_AXIS "AXIS"
#define XSH_DISPERSOL_TABLE_COLNAME_ORDER "ORDER"
#define XSH_DISPERSOL_TABLE_COLNAME_DEGX "DEGX"
#define XSH_DISPERSOL_TABLE_COLNAME_DEGY "DEGY"
#define XSH_DISPERSOL_AXIS_SLIT   "SLIT"
#define XSH_DISPERSOL_AXIS_LAMBDA "LAMBDA"
typedef struct{
  int absorder; /* absolute index of order */
  cpl_polynomial *lambda_poly;
  cpl_polynomial *slit_poly;
}xsh_dispersol;


typedef struct{
  int size;
  int degx; /* degree of 2D polynomial in X */
  int degy; /* degree of 2D polynomial in Y */
  int binx;
  int biny;
  xsh_dispersol *list;
  cpl_propertylist *header;
}xsh_dispersol_list;


xsh_dispersol_list* 
xsh_dispersol_list_new( int size, int deg_x, int deg_y,
  xsh_instrument *instr);

xsh_dispersol_list* 
xsh_dispersol_list_load( cpl_frame *frame, xsh_instrument *instr);

void 
xsh_dispersol_list_add( xsh_dispersol_list *list, 
                        int idx, 
                        int absorder,
                        cpl_polynomial *lambda_poly, 
                        cpl_polynomial *slit_poly);

cpl_frame* 
xsh_dispersol_list_to_wavemap( xsh_dispersol_list *list,
                               cpl_frame *order_frame, 
                               xsh_pre *pre,
                               xsh_instrument *instr,
                               const char* tag);

cpl_frame* 
xsh_dispersol_list_to_slitmap( xsh_dispersol_list *list,
                               cpl_frame *order_frame, 
                               xsh_pre *pre,
                               xsh_instrument *instr,
			       const char* tag);

double xsh_dispersol_list_eval( xsh_dispersol_list *list, 
  cpl_polynomial *poly, cpl_vector *pos);

void 
xsh_dispersol_list_free( xsh_dispersol_list **list);

cpl_frame* xsh_dispersol_list_save( xsh_dispersol_list *list,const char* tag);

#endif  /* XSH_DISPERSOL_H */
