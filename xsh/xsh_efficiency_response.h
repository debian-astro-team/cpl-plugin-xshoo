/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */


/*
 * $Author: amodigli $
 * $Date: 2013-03-07 16:53:21 $
 * $Revision: 1.7 $
 */
#ifndef XSH_EFFICIENCY_RESPONSE_H_
#define XSH_EFFICIENCY_RESPONSE_H_
#include <xsh_data_instrument.h>
static const double STAR_MATCH_DEPSILON=0.0166667; /*60 arcsecs */

/* Definition of the regions with a too high absorption; in these regions
   the response is computed by a simple linear interpolation between the
   valuse at the edges of the region
*/
typedef struct {
  double lambda_min ;
  double lambda_max ;
} HIGH_ABS_REGION ;

HIGH_ABS_REGION *
xsh_fill_high_abs_regions(
   xsh_instrument* instrument,
   cpl_frame* high_abs_frame);

HIGH_ABS_REGION *
xsh_fill_tell_fit_regions(
   xsh_instrument* instrument,
   cpl_frame* high_abs_frame);

HIGH_ABS_REGION *
xsh_fill_tell_compute_resid_regions(
   xsh_instrument* instrument,
   cpl_frame* high_abs_frame);




double *
xsh_bspline_fit_data(double* lambda, double* flux,const int size,
    HIGH_ABS_REGION * phigh,xsh_instrument* inst,const int fit_region);

double *
xsh_bspline_fit_data2(double* lambda, double* flux,const int size,
    HIGH_ABS_REGION * phigh,xsh_instrument* inst,const int fit_region);
double *
xsh_bspline_interpolate_data_at_pos(double* wave, double* flux,const int size, 
				    double* pos, const int size_pos);

#endif
