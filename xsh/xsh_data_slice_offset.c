/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-01-10 09:02:01 $
 * $Revision: 1.7 $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*/
/**
 * @defgroup xsh_data_slice_offset Slice Offset Data Handling
 * @ingroup data_handling
 */
/*---------------------------------------------------------------------------*/

/**@{*/


/*-----------------------------------------------------------------------------
                                 Includes
 ----------------------------------------------------------------------------*/

#include <math.h>
#include <xsh_dfs.h>
#include <xsh_data_slice_offset.h>
#include <xsh_utils_table.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_pfits.h>
#include <cpl.h>

/*----------------------------------------------------------------------------
                                 Function implementation
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @brief
    Create an empty slice_offset
  @return
    The slice offset structure

*/
/*---------------------------------------------------------------------------*/
xsh_slice_offset* xsh_slice_offset_create(void)
{
  xsh_slice_offset* result = NULL;

  XSH_CALLOC(result, xsh_slice_offset, 1);

  XSH_NEW_PROPERTYLIST(result->header);

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_slice_offset_free( &result);
    }  
    return result;
}
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @brief 
    Load a slice_offset from a frame
  @param[in] frame
    The table wich contains polynomials coefficients of the orders
  @return
    The slice_offset structure

 */
/*---------------------------------------------------------------------------*/
xsh_slice_offset* xsh_slice_offset_load(cpl_frame* frame){
  cpl_table *table = NULL; 
  cpl_propertylist* header = NULL;
  const char* tablename = NULL;
  xsh_slice_offset *result = NULL;

  /* check input parameters */
  XSH_ASSURE_NOT_NULL(frame);

  /* get table filename */
  check(tablename = cpl_frame_get_filename(frame));

  XSH_TABLE_LOAD( table, tablename);
  
  check( header = cpl_propertylist_load(tablename,0));

  check(result = xsh_slice_offset_create());

  check( cpl_propertylist_append(result->header, header));

  check( xsh_get_table_value(table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_UP,
    CPL_TYPE_DOUBLE, 0, &(result->cen_up)));
  check( xsh_get_table_value(table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_DOWN,
    CPL_TYPE_DOUBLE, 0, &(result->cen_down)));

  cleanup:
    if (cpl_error_get_code () != CPL_ERROR_NONE) {
      xsh_error_msg("can't load frame %s",cpl_frame_get_filename(frame));
      xsh_slice_offset_free(&result);
    }
    xsh_free_propertylist( &header);
    XSH_TABLE_FREE( table);
    return result;  
}
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @brief
    Free memory associated to a slice_offset
  @param[in] list
    The slice_offset to free
 */
/*---------------------------------------------------------------------------*/
void xsh_slice_offset_free( xsh_slice_offset** list)
{
  if (list && *list){
    xsh_free_propertylist( &((*list)->header));
    cpl_free( *list);
    *list = NULL;
  }
}
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/**
  @brief
    Get header of the table
  @param[in] list
    The slice_offset
  @return
    The header associated to the table
 */
/*---------------------------------------------------------------------------*/
cpl_propertylist* xsh_slice_offset_get_header(xsh_slice_offset *list)
{
  cpl_propertylist *res = NULL;

  XSH_ASSURE_NOT_NULL( list);
  res = list->header;
  cleanup:
    return res;
}
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
  @brief 
    Save an slice_offset list to a frame
  @param[in] list
    The slice_offset structure
  @param[in] filename 
    The name of the save file on disk
  @param instrument instrument arm setting
  @return a newly allocated frame

 */
/*---------------------------------------------------------------------------*/
cpl_frame* xsh_slice_offset_save(xsh_slice_offset *list,
  const char* filename, xsh_instrument *instrument)
{
  cpl_table *table = NULL;
  cpl_frame *result = NULL;
  const char *tag = NULL ;

  /* check input parameters */
  XSH_ASSURE_NOT_NULL( list);
  XSH_ASSURE_NOT_NULL( filename);
  XSH_ASSURE_NOT_NULL( instrument);

  /* create a table */
  check( table = cpl_table_new( 1));

  /* create column names */
  XSH_TABLE_NEW_COL( table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_UP,
    XSH_SLICE_OFFSET_TABLE_UNIT_CEN_UP, CPL_TYPE_DOUBLE);
  XSH_TABLE_NEW_COL( table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_DOWN,
    XSH_SLICE_OFFSET_TABLE_UNIT_CEN_DOWN, CPL_TYPE_DOUBLE);

  check( cpl_table_set_double( table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_UP,
      0, list->cen_up));
  check( cpl_table_set_double( table, XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_DOWN,
      0, list->cen_down));

  /* create fits file */
  check( cpl_table_save( table, list->header, NULL, filename, 
    CPL_IO_DEFAULT));

  /* Create the frame */
  tag = XSH_GET_TAG_FROM_ARM( XSH_SLICE_OFFSET, instrument);

  check( result = xsh_frame_product( filename, tag,
    CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
    CPL_FRAME_LEVEL_TEMPORARY));

  cleanup:
    XSH_TABLE_FREE(table);
    return result ;
}
/*---------------------------------------------------------------------------*/

/**@}*/
