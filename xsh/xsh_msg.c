/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_msg        Messaging
 * @ingroup xsh_tools
 *
 * This module exists in order to save the user from typing the function name
 * when calling a CPL message function (by using the ANSI-C99 @em __func__ 
 * identifier).
 *
 * Also, a record is kept on the number of warnings printed 
 * using @c xsh_msg_warning().
 *
 * These messaging functions never set the CPL error code.
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*-----------------------------------------------------------------------------
              Includes
 -----------------------------------------------------------------------------*/

#include "xsh_msg.h"

#include <cpl.h>
#include <stdarg.h>

/*-----------------------------------------------------------------------------
              Defines
 -----------------------------------------------------------------------------*/

#define MAXSTRINGLENGTH  1024  /* Used to pass a va_list to cpl_msg_warning */

/*-----------------------------------------------------------------------------
              Variables
 -----------------------------------------------------------------------------*/
static int number_of_warnings = 0;  
/* Coun't the number of warnings since initialization */

/*----------------------------------------------------------------------------*/
/**
   @brief    Initialize messaging

   This function 
   - resets the number of warnings printed,
   - sets the message output format.
  
*/
/*----------------------------------------------------------------------------*/
void
xsh_msg_init (void)
{
  /* Initialize per recipe: */
  number_of_warnings = 0;

  cpl_msg_set_indentation (2);

  /*  CPL message format is
   *  [Time][Verbosity][domain][component] message
   *
   *  Don't show the (variable length and wildly
   *  fluctuating) component. It interferes with
   *  indentation. The component is available anyway
   *  on CPL_MSG_DEBUG level.
   *
   *  Don't show the time. This is available on
   *  the DEBUG level. Use esorex --time to time
   *  a recipe.
   */

  cpl_msg_set_time_off ();
  /* Leave it to the recipe caller
   * application to define the verbosity
   * level    cpl_msg_set_level(...);
   */
  cpl_msg_set_domain_on ();
  cpl_msg_set_component_off ();
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Get number of warnings printed so far
   @return Number of warnings since initialization of messaging
*/
/*----------------------------------------------------------------------------*/
int
xsh_msg_get_warnings ( void )
{
  return number_of_warnings;
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a warning message
   @param    fct             Identity of calling function
   @param    format          A printf()-like format string

   Don't call this function directly, use the macro @c xsh_msg_warning().

   This function is used instead of @c cpl_msg_warning(), and saves
   the user from typing the calling function name.

   Additionally, record is kept on the total number of warnings printed
   (see @c xsh_msg_get_warnings()).
*/
/*----------------------------------------------------------------------------*/
void
xsh_msg_warning_macro (const char *fct, const char *format, ...)
{
  char printbuffer[MAXSTRINGLENGTH];
  /* Message functions should never fail, so don't rely on
     dynamic memory allocation */

  va_list al;

  va_start (al, format);
  vsnprintf (printbuffer, MAXSTRINGLENGTH - 1, format, al);
  va_end (al);

  printbuffer[MAXSTRINGLENGTH - 1] = '\0';

  cpl_msg_warning (fct, "%s", printbuffer);

  number_of_warnings += 1;
}

/**@}*/
