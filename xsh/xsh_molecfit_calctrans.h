/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef XSH_MOLECFIT_MODEL_H
#define XSH_MOLECFIT_MODEL_H
/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/

/* Include both telluriccorr *and* our extra wrapper codes, since
   we deliberately don't want to have them (pre-included) in telluriccorr.h 
   to ensure telluriccorr is still comptabile with molecfit_model
*/

#include <string.h>
#include <math.h>

#include <cpl.h>

//#include <telluriccorr.h>
#include "mf_wrap.h"
#include "mf_wrap_calc.h"
#include "mf_spectrum.h"
#include "xsh_molecfit_utils.h"
#include <telluriccorr.h>

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Enumeration types
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Defines
 */
/*----------------------------------------------------------------------------*/


//#define XSHOOTER_INPUT_DATA_FLUX_COLUMN "Flux"

//descriptions for the input parameters...
//by default grab the DESC already defined in molecfit_config or telluriccorr
//in case we want to define our own xshooter specific help

#define XSH_MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC    MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC
#define XSH_MF_PARAMETERS_APPLY_WLC_CORR_DESC       	MF_PARAMETERS_APPLY_WLC_WORR_DESC
#define XSH_MF_PARAMETERS_SCALE_TO_OBS_PWV_DESC		    MF_PARAMETERS_SCALE_TO_OBS_PWV_DESC
#define XSH_MF_PARAMETERS_USE_MOLEC_DATABASE_DESC       MF_PARAMETERS_USE_MOLEC_DATABASE_DESC



/*----------------------------------------------------------------------------*/
/**
 *                 Global variables
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Macros
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Structured types
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 *                 Functions prototypes
 */
/*----------------------------------------------------------------------------*/

int xsh_molecfit_calctrans(cpl_frameset *frameset, const cpl_parameterlist  *parlist);

cpl_error_code xsh_molecfit_calctrans_config(cpl_frameset *frameset,
		const cpl_parameterlist  *parlist,
		cpl_parameterlist* ilist,
		cpl_parameterlist* iframelist);

cpl_error_code xsh_molecfit_setup_frameset(cpl_frameset* frameset,cpl_parameterlist* list,const char* arm,const char* input_name);


#endif /*XSH_MOLECFIT_MODEL_H*/
