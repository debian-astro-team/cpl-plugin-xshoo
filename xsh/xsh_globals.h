/*                                                                           a
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-07-15 08:10:54 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_GLOBALS_H
#define XSH_GLOBALS_H

/*----------------------------------------------------------------------------
  Defines
  ----------------------------------------------------------------------------*/
/* compat macro */
#if defined CPL_VERSION_CODE && CPL_VERSION_CODE >= CPL_VERSION(6, 3, 0)
#define cpl_frameset_get_frame cpl_frameset_get_position
#define cpl_frameset_get_frame_const cpl_frameset_get_position_const
#endif

#define XSH_UVB_DICHROIC_WAVE_CUT 556.0 

#endif
