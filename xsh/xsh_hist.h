/* $Id: xsh_hist.h,v 1.3 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002, 2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02111-1307 USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_HIST_H
#define XSH_HIST_H

#include <cpl.h>

typedef struct _xsh_hist_ xsh_hist;

/* Creation/Destruction functions */

xsh_hist *
xsh_hist_new(void);

void
xsh_hist_delete(xsh_hist *);

/* Initialisation function */

cpl_error_code
xsh_hist_init(xsh_hist   *,
                 unsigned long  ,
                 double         ,
                 double         );

/* Accessor functions */

unsigned long
xsh_hist_get_value(const xsh_hist *,
                      const unsigned long);

unsigned long
xsh_hist_get_nbins(const xsh_hist *);

double
xsh_hist_get_bin_size(const xsh_hist *);

double
xsh_hist_get_range(const xsh_hist *);

double
xsh_hist_get_start(const xsh_hist *);

/* Histogram computing function */

cpl_error_code
xsh_hist_fill(xsh_hist     *,
                 const cpl_image *);

/* Statistics functions */

unsigned long
xsh_hist_get_max(const xsh_hist *,
                    unsigned long     *);

/* Casting function */

cpl_table *
xsh_hist_cast_table(const xsh_hist *);

/* Functions for operations on histograms */

cpl_error_code
xsh_hist_collapse(xsh_hist *,
                     unsigned long);

#endif /* IRPLIB_HIST_H */

