/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-07-10 16:17:40 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_UTILS_TABLE_H
#define XSH_UTILS_TABLE_H

/*----------------------------------------------------------------------------
                                    Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_error.h>

/*----------------------------------------------------------------------------
                                    Macros                                   
 ----------------------------------------------------------------------------*/
#define XSH_TABLE_LOAD( TABLE, NAME) \
  check_msg( TABLE = cpl_table_load( NAME, 1, 0),\
    "Can't load %s FITS table", NAME)

#define XSH_TABLE_FREE( TABLE)\
  if (TABLE != NULL){\
    cpl_table_delete ( TABLE);\
    TABLE = NULL;\
  }


#define XSH_TABLE_GET_ARRAY( TYPE) \
  void xsh_table_get_array_##TYPE( cpl_table* table, const char* colname, \
    TYPE* pointer, int nb)\
  {\
    const cpl_array* array = NULL;\
    int array_size = 0, k=0;\
    const TYPE* data = NULL;\
    \
    XSH_ASSURE_NOT_NULL( pointer);\
    check( array = cpl_table_get_array( table, colname, 0));\
    check( array_size = cpl_array_get_size( array));\
    XSH_ASSURE_NOT_ILLEGAL( nb == array_size);\
    check( data = cpl_array_get_data_##TYPE##_const( array));\
    for( k=0; k< array_size; k++){\
      pointer[k] = data[k];\
    }\
    cleanup:\
      return;\
  }

/*----------------------------------------------------------------------------
   				    Prototypes
 ----------------------------------------------------------------------------*/
cpl_error_code xsh_get_table_value(const cpl_table* table,
  const char *colname, cpl_type coltype, int i, void *result);
void xsh_table_get_array_int( cpl_table* table, const char* colname, 
  int* pointer, int nb);
void xsh_table_get_array_float( cpl_table* table, const char* colname, 
  float* pointer, int nb);
void xsh_table_get_array_double( cpl_table* table, const char* colname, 
  double* pointer, int nb);

cpl_error_code xsh_sort_table_1(cpl_table *t, const char *column1,
                 cpl_boolean reverse1);
cpl_error_code xsh_sort_table_2(cpl_table *t, const char *column1, 
                 const char *column2, cpl_boolean reverse1, 
                 cpl_boolean reverse2);

cpl_error_code
xsh_sort_table_3(cpl_table *t,
                 const char *column1, const char *column2, const char *column3,
              cpl_boolean reverse1, cpl_boolean reverse2,cpl_boolean reverse3);
double
xsh_data_interpolate(
		     double wav,
		     int nrow,
		     double* pw,
		     double* pe
		     );

double
xsh_table_interpolate(cpl_table* tbl,
		  double wav,
		  const char* colx,
		      const char* coly);


cpl_error_code
xsh_frame_table_monitor_flux_qc(cpl_frame* frm,
				const char* colw,
				const char* colf,
				const char* prefix,
				xsh_instrument* instrument);
cpl_error_code
xsh_wavecal_qclog_intmon(cpl_frame* table_check,
                         const cpl_frame *line_intmon,
                         const double exptime,
                         xsh_instrument* inst);

cpl_error_code
xsh_table_merge_clean_and_resid_tabs(cpl_frame* frm_resid,cpl_frame* frm_clean);

cpl_table*
xsh_table_shift_rv(cpl_table* orig, const char* col_wave,const double offset);


cpl_table*
xsh_histogram(const cpl_table* data, const char* cname, const int nbins,
                const double min, const double max);

#endif
