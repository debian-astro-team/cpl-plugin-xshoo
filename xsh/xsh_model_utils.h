/* $Id: xsh_model_utils.h,v 1.5 2011-12-02 14:15:28 amodigli Exp $
 *
 *   This file is part of the ESO X-shooter Pipeline                          
 *   Copyright (C) 2006 European Southern Observatory  
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_MODEL_UTILS_H
#define XSH_MODEL_UTILS_H
#include <xsh_drl.h>
#include <cpl.h>
cpl_frame*
xsh_util_physmod_model_THE_create(cpl_frame* config_frame,
				    xsh_instrument* instrument,
				    cpl_frame* wave_list,
				  const int binx, const int biny,
				  const int num_ph, const int sky);
cpl_error_code
xsh_model_temperature_update_structure(struct xs_3* p_xs_3_config,
                                       cpl_frame* ref_frame,
                                       xsh_instrument* instrument);

cpl_error_code
xsh_model_temperature_update_frame(cpl_frame** model_config_frame,
                                   cpl_frame* header,
                                   xsh_instrument* instrument,
                                   int* found_temp);

#endif
