/*
 * This file is part of the ESO X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:37:36 $
 * $Revision: 1.9 $
 */

#ifndef XSH_QC_HANDLING_H
#define XSH_QC_HANDLING_H

#include "xsh_data_instrument.h"
#include "xsh_data_pre.h"




typedef struct {
  const char *kw_name;  /**< QC name */
  const char * kw_recipes;  /**< List of recipes creating this QC parameter*/
  const char * kw_recipes_tbw; /**< List of recipes to be written ... */
  const char * kw_function;   /**< DRL functions */
  const char * kw_help ;   /**< Comment */
  cpl_type kw_type ;		/**< CPL Type (INT, DOUBLE< ...) */
  const char * arms ;		/**< List of arms for which the QC should be
				 created */
  const char * pro_catg ;	/**< The PRO.CATG(s) relevant for this KW */
} qc_description ;

qc_description * xsh_get_qc_desc_by_kw( const char *kw ) ;
qc_description * xsh_get_qc_desc_by_recipe( const char *recipe,
					    qc_description *prev ) ;
qc_description * xsh_get_qc_desc_by_function( char *function,
					      qc_description *prev ) ;

qc_description * xsh_get_qc_desc_by_pro_catg( const char * pro_catg ) ;

int xsh_qc_in_recipe( qc_description * pqc, xsh_instrument * instrument ) ;
int xsh_is_qc_for_arm( const char * arm, qc_description * pqc ) ;
int xsh_is_qc_for_pro_catg( const char * pro_catg, qc_description * pqc ) ;

void xsh_add_qc_crh (xsh_pre* pre, int nbcrh, int nframes);


#endif
