/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-06-21 07:39:48 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_ARCLIST_H
#define XSH_DATA_ARCLIST_H

#include <cpl.h>


#define XSH_ARCLIST_TABLE_NB_COL 4
#define XSH_ARCLIST_TABLE_COLNAME_WAVELENGTH "WAVELENGTH"
#define XSH_ARCLIST_TABLE_UNIT_WAVELENGTH "none"
#define XSH_ARCLIST_TABLE_COLNAME_NAME "NAME"
#define XSH_ARCLIST_TABLE_UNIT_NAME "none"
#define XSH_ARCLIST_TABLE_COLNAME_FLUX "FLUX"
#define XSH_ARCLIST_TABLE_UNIT_FLUX "none"
#define XSH_ARCLIST_TABLE_COLNAME_COMMENT "COMMENT"
#define XSH_ARCLIST_TABLE_UNIT_COMMENT "none"

typedef struct{
  /* wavelength of arc line */
  float wavelength;
  /* name of arc line*/
  char* name;
  /* value of flux */
  int flux;
  /* comment */
  char* comment;
}xsh_arcline;


typedef struct{
  int size;
  int nbrejected;
  int* rejected;
  xsh_arcline** list;
  cpl_propertylist* header;
}xsh_arclist;

xsh_arclist* xsh_arclist_load(cpl_frame* frame);
void xsh_arcline_free(xsh_arcline** line);
void xsh_arclist_free(xsh_arclist** list);
void xsh_arclist_reject(xsh_arclist* l, int i);
void xsh_arclist_restore(xsh_arclist* l, int i);
int xsh_arclist_get_nbrejected(xsh_arclist* l);
float xsh_arclist_get_wavelength(xsh_arclist* l, int i);
int xsh_arclist_get_size(xsh_arclist* list);
int xsh_arclist_is_rejected(xsh_arclist* list, int idx);
void xsh_arclist_clean(xsh_arclist* list);
void xsh_arclist_clean_from_list( xsh_arclist* list, double* lambda, int size);
void xsh_arclist_clean_from_list_not_flagged( xsh_arclist* list, double* lambda, int* flag, int size);
cpl_propertylist* xsh_arclist_get_header(xsh_arclist* list);
void xsh_arclist_lambda_sort(xsh_arclist* list);
cpl_frame* xsh_arclist_save(xsh_arclist* list,const char* filename,const char* tag);
void xsh_dump_arclist( xsh_arclist* list ) ;
#endif  /* XSH_ARCLIST_H */
