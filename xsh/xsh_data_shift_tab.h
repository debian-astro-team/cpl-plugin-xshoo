/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_SHIFT_TAB_H
#define XSH_DATA_SHIFT_TAB_H


#include <cpl.h>
#include <stdbool.h>
#include <xsh_data_instrument.h>

#define XSH_SHIFT_TABLE_NB_COL_IFU 3
#define XSH_SHIFT_TABLE_COLNAME_YSHIFT_DOWN "SHIFT_Y_DOWN"
#define XSH_SHIFT_TABLE_UNIT_YSHIFT_DOWN "pixel"
#define XSH_SHIFT_TABLE_COLNAME_YSHIFT_CEN "SHIFT_Y_CEN"
#define XSH_SHIFT_TABLE_UNIT_YSHIFT_CEN "pixel"
#define XSH_SHIFT_TABLE_COLNAME_YSHIFT_UP "SHIFT_Y_UP"
#define XSH_SHIFT_TABLE_UNIT_YSHIFT_UP "pixel"

#define XSH_SHIFT_TABLE_NB_COL_SLIT 1
#define XSH_SHIFT_TABLE_COLNAME_YSHIFT "SHIFT_Y"
#define XSH_SHIFT_TABLE_UNIT_YSHIFT "pixel"

typedef struct{
  /* shift y (slit mode) */
  bool is_ifu;
  float shift_y;
  float shift_y_down;
  float shift_y_cen;
  float shift_y_up;
  cpl_propertylist * header ; 
} xsh_shift_tab;


xsh_shift_tab* xsh_shift_tab_load( cpl_frame *frame, xsh_instrument *instr);
void xsh_shift_tab_free( xsh_shift_tab **tab);
xsh_shift_tab * xsh_shift_tab_create( xsh_instrument * instrument ) ;
cpl_frame * xsh_shift_tab_save(xsh_shift_tab *tab, const char* tag,const int clean_tmp);

#endif  /* XSH_SHIFT_TAB_H */
