/*                                                                           a
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-02-20 18:04:57 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_UTILS_VECTOR_H
#define XSH_UTILS_VECTOR_H

/*----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/

#include <cpl.h>

cpl_vector*
xsh_vector_fit_slope(cpl_vector* vec_wave,cpl_vector* vec_flux,const double wmin_max,const double wmax_min,const int degree);
cpl_vector*
xsh_vector_upsample(cpl_vector* vin,const int factor);

cpl_vector*
xsh_vector_extract_range(cpl_vector* vin,const cpl_size pos, const int hsize);

#endif
