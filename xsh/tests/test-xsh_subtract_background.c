/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-08-16 11:45:53 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_subtract_bkg Test Subtract Background 
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/

#include <xsh_data_instrument.h>
#include <xsh_pfits.h>
#include <xsh_msg.h>
#include <xsh_utils.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>

#include <getopt.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_SUBTRACT_BACKGROUND"


enum {
  NB_Y_OPT, RADIUS_X_OPT, RADIUS_Y_OPT, HELP_OPT, MAX_FRAC_GRID_OPT,
  METHOD_OPT, SMOOTH_X_OPT, SMOOTH_Y_OPT, MIN_FRAC_OPT
};

static const char * Options = "" ;

static struct option long_options[] = {
  {"nb-y", required_argument, 0, NB_Y_OPT},
  {"radius-x", required_argument, 0, RADIUS_X_OPT},
  {"radius-y", required_argument, 0, RADIUS_Y_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};



static void Help( void )
{
  puts( "Unitary test of xsh_subtract_background" ) ;
  puts( "Usage: test_xsh_subtract_background [options] <input_files>" ) ;
  puts( "Options" ) ;
  puts( " --nb-y=<n>       : Number of points of the grid in y direction" ) ;
  puts( " --radius-x=<n>   : Half size of the subwindow in x direction" ) ;
  puts( " --radius-y=<n>   : Half size of the subwindow in y direction" ) ;
  puts( " --help           : What you see" ) ;
  puts( "\nInput Files" ) ;
  puts( "The input files argument MUST be in this order:" ) ;
  puts( " 1. Science frame in PRE format" ) ;
  puts( " 2. SOF [ORDER_TAB_EDGES]\n" ) ;
  TEST_END();
}

static void HandleOptions( int argc, char **argv,
  xsh_background_param *backg_par)
{
  int opt ;
  int option_index = 0;


  while (( opt = getopt_long (argc, argv, Options,
                              long_options, &option_index)) != EOF ){
    switch ( opt ) {
    case NB_Y_OPT:
      backg_par->sampley = atoi( optarg);
      break;
    case RADIUS_X_OPT:
      backg_par->radius_x = atoi( optarg);
      break;
    case RADIUS_Y_OPT:
      backg_par->radius_y = atoi( optarg);
      break;
    default: Help() ; exit( 0 ) ;
    }
  }
}
/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of XSH_PREPARE
  @return   0 if tests passed successfully

  Test the Data Reduction Library function XSH_PREPARE
 */
/*--------------------------------------------------------------------------*/

int main( int argc, char **argv)
{
  int ret = 0;

  xsh_instrument* instrument = NULL;

  const char *sof_name = NULL;
  cpl_frameset *set = NULL;
  const char * sci_name = NULL ;

  cpl_frame* flat_frame = NULL;
  cpl_frame* flat_rmbckg = NULL;
  cpl_frame* guess_order_tab_frame = NULL;

  xsh_background_param backg;
  cpl_frame* frame_grid=NULL;
  cpl_frame* frame_back=NULL;


  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  backg.sampley = 800;
  backg.radius_x = 2;
  backg.radius_y = 2;
  backg.debug = CPL_TRUE;

  HandleOptions( argc, argv, &backg);

  if ( (argc - optind) >=2 ) {
    sci_name = argv[optind];
    sof_name = argv[optind+1];
  }
  else {
    Help();
    exit(0);
  }

  /* Create frameset from sof */
  check( set = sof_to_frameset( sof_name));
  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));

  check( guess_order_tab_frame = xsh_find_order_tab_edges( set, instrument));
  TESTS_XSH_FRAME_CREATE( flat_frame, "FLAT", sci_name);

  xsh_msg("FRAME          : %s",
    cpl_frame_get_filename( flat_frame));
  xsh_msg("ORDERLIST      : %s",
    cpl_frame_get_filename( guess_order_tab_frame));

  xsh_msg(" Parameters ");
  xsh_msg(" sample Y %d", backg.sampley);
  xsh_msg(" radius X %d", backg.radius_x);
  xsh_msg(" radius Y %d", backg.radius_y);

  check( flat_rmbckg = xsh_subtract_background( flat_frame,
    guess_order_tab_frame, &backg, instrument, "",
						&frame_grid, &frame_back,1,1,1)); 

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    xsh_free_frameset( &set);
    xsh_instrument_free( &instrument);
    xsh_free_frame( &flat_frame);
    xsh_free_frame( &flat_rmbckg);
    xsh_free_frame( &frame_grid);
    xsh_free_frame( &frame_back);

    TEST_END();
    return ret;
}

/**@}*/
