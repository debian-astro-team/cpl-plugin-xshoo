/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:35:23 $
 * $Revision: 1.2 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_create_spectralformat  Create a spectral format
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_resid_tab.h>
#include <xsh_model_io.h>
#include <xsh_model_kernel.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_CREATE_SPECTRALFORMAT"
#define SYNTAX "Create a spectral format file from a model config file\n"\
  "use : ./the_xsh_create_spectralformat MODEL_CONFIG\n"\
  "MODEL_CONFIG   => the model config frame\n"

  
/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_resid_tab
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
  xsh_xs_3 config;
  char* model_config_name = NULL;
  cpl_frame* model_config_frame = NULL;
  cpl_frame* spec_form_frame = NULL;
  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    model_config_name = argv[1];
  }
  else{
    printf(SYNTAX);
    return 0;  
  }
  
  /* Create instrument structure and fill */
  instrument = xsh_instrument_new() ;
  xsh_instrument_set_mode( instrument, XSH_MODE_IFU );
  xsh_instrument_set_arm( instrument, XSH_ARM_VIS);

  /* Create frames */
  XSH_ASSURE_NOT_NULL( model_config_name);
  model_config_frame = cpl_frame_new();
  cpl_frame_set_filename( model_config_frame, model_config_name) ;
  cpl_frame_set_level( model_config_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( model_config_frame, CPL_FRAME_GROUP_RAW ) ;
  cpl_frame_set_tag( model_config_frame, "MODEL_CONFIG_TAB_VIS");
  check( xsh_model_config_load_best( model_config_frame, &config));
  check(spec_form_frame= xsh_model_spectralformat_create( &config,"spectral_format.fits"));

  cleanup:
  xsh_free_frame(&spec_form_frame);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else return ret ;
}

/**@}*/
