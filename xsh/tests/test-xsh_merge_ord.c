/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2013-05-14 07:02:49 $
 * $Revision: 1.22 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_merge_ord  Test Object merging
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>
#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_rec.h>
#include <xsh_data_spectrum.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_badpixelmap.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>
#include <xsh_ifu_defs.h>
/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_MERGE_ORD"

static void Help( void )
{
  puts( "Unitary test of xsh_merge_ord" ) ;
  puts( "Usage: test_xsh_merge_ord [options] <input_files>" ) ;
  puts( "Options" );
  puts( "   --method         : WEIGHT or MEAN [MEAN]");
  puts( "   --slitlet=n>     : Num of the slitlet" ) ;
  puts( " --debug=<n>        : Level of debug LOW | MEDIUM | HIGH [MEDIUM]" );
  puts( "\nInput Files" ) ;
  puts( "  REC_ORDER_nD_*_arm_DRL frame" ) ;
  TEST_END();
}

enum {
  SLITLET_OPT, METHOD_OPT, DEBUG_OPT
} ;

/*
static struct option long_options[] = {
  {"slitlet", required_argument, 0, SLITLET_OPT},
  {"method", required_argument, 0, METHOD_OPT},
  {"debug", required_argument, 0, DEBUG_OPT},
  {0,0,0,0}
};
*/


static void analyse_merge_ord( cpl_frame* spectrum_frame,  xsh_instrument* instr);

/*--------------------------------------------------------------------------
 *   Implementation
 *--------------------------------------------------------------------------*/
static void analyse_merge_ord( cpl_frame* spectrum_frame,  xsh_instrument* instr)
{
  const char* spectrum_name = NULL;
  xsh_spectrum *spectrum = NULL;
  int i;
  FILE* fulldatfile = NULL;
  double* flux = NULL;

  XSH_ASSURE_NOT_NULL( spectrum_frame);
  XSH_ASSURE_NOT_NULL( instr);

  check (spectrum_name = cpl_frame_get_filename( spectrum_frame));

  xsh_msg("Spectrum frame : %s", spectrum_name);

  check( spectrum = xsh_spectrum_load( spectrum_frame));
  check( flux = xsh_spectrum_get_flux( spectrum));

  fulldatfile = fopen("merge_ord.dat","w");
  for(i=0; i< spectrum->size; i++){
    fprintf( fulldatfile, "%f %f\n", spectrum->lambda_min+i*spectrum->lambda_step, flux[i]);
  }
  xsh_msg("Save file merge_ord.dat");
  fclose( fulldatfile);

  cleanup:
    xsh_spectrum_free( &spectrum);
    return;  
}

/**
  @brief
    Unit test of xsh_merge_ord
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
  XSH_ARM arm = XSH_ARM_UNDEFINED;
  int merge_par = 0 ;

  cpl_frame* result = NULL;
  char* rec_name = NULL;
  cpl_frame* rec_frame = NULL;
  cpl_propertylist *plist = NULL;
  int slitlet = 0;
  const int decode_bp=2147483647;
  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  if ( (argc - optind) >=1 ) {
    rec_name = argv[optind]; 
  }
  else{
    Help();
    return 0;  
  }

  /* Create frames */
  TESTS_XSH_FRAME_CREATE( rec_frame, "REC_arm", rec_name);

  check( plist = cpl_propertylist_load( rec_name, 0));

  /* Create instrument structure and fill */
  check( arm = xsh_pfits_get_arm( plist));

  instrument = xsh_instrument_new();

  if ( arm == XSH_ARM_UVB){
    xsh_instrument_set_arm( instrument, XSH_ARM_UVB); 
    cpl_frame_set_tag( rec_frame, "REC_UVB");
  } 
  else if ( arm == XSH_ARM_VIS){
    xsh_instrument_set_arm( instrument, XSH_ARM_VIS);
    cpl_frame_set_tag( rec_frame, "REC_VIS");
  }
  else if ( arm == XSH_ARM_NIR){
    xsh_instrument_set_arm( instrument, XSH_ARM_NIR);
    cpl_frame_set_tag( rec_frame, "REC_NIR");
  }
  else{
    xsh_msg( "invalid arm of %s", 
      xsh_instrument_arm_tostring( instrument));
  }
  xsh_instrument_set_decode_bp( instrument, decode_bp ) ;

  if (slitlet == 0){
    xsh_instrument_set_mode( instrument, XSH_MODE_SLIT);
  }
  else{
    xsh_instrument_set_mode( instrument, XSH_MODE_IFU);
  }

  xsh_msg("---Input Frames");
  xsh_msg("Rec frame : %s", rec_name);
  xsh_msg("---Parameters");
  xsh_msg("  method WEIGTHED");

  check( result = xsh_merge_ord_slitlet( rec_frame, instrument, merge_par, slitlet,"test"));

  if (result != NULL){
    check( analyse_merge_ord( result, instrument));
  }

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    xsh_instrument_free( &instrument);
    xsh_free_propertylist( &plist);
    xsh_free_frame( &rec_frame);
    xsh_free_frame( &result);

    TEST_END();
    return ret;
}

/**@}*/
