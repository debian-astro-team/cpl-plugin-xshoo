/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-01-16 21:09:42 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup xsh_test_pre     Test Data Type PRE functions
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/



#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <xsh_utils_image.h>
#include <tests.h>

#include <cpl.h>
#include <math.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_IFU_TRACE_SLICES"

#define SYNTAX "Computes the ifu slices positio \n"\
  "use : ./test_xsh_ifu_trace_slices IFU_FLAT.fits SLIT_FLATS.fits ORDER_TAB_CENTR\n"




/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of PRE module
  @return   0 if tests passed successfully

  Test the PRE module.
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  char *ifu_flat_name = NULL;
  char *slit_flat_name = NULL;


  cpl_image* lx_ima=NULL;
  cpl_image* ly_ima=NULL;
  cpl_image* scharr_x_ima=NULL;
  cpl_image* scharr_y_ima=NULL;
  cpl_image* ifu_flat_ima=NULL;
  cpl_image* slit_flat_ima=NULL;
  cpl_image* ratio_ima=NULL;
  //int sx=0;
  //int sy=0;
  double max=0;
  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    ifu_flat_name = argv[1];
    slit_flat_name = argv[2];
  }
  else{
    printf(SYNTAX);
    return 0;
  }
  
  XSH_ASSURE_NOT_NULL( ifu_flat_name);
  XSH_ASSURE_NOT_NULL( slit_flat_name);

  check(ifu_flat_ima=cpl_image_load(ifu_flat_name,CPL_TYPE_FLOAT,0,0));
  check(slit_flat_ima=cpl_image_load(slit_flat_name,CPL_TYPE_FLOAT,0,0));
  //sx=cpl_image_get_size_x(ifu_flat_ima);
  //sy=cpl_image_get_size_y(ifu_flat_ima);

  check(lx_ima=xsh_sobel_lx(ifu_flat_ima));
  check(ly_ima=xsh_sobel_ly(ifu_flat_ima));
  check(cpl_image_save(lx_ima,"lx.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(ly_ima,"ly.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));


  check(scharr_x_ima=xsh_scharr_x(ifu_flat_ima));
  check(scharr_y_ima=xsh_scharr_y(ifu_flat_ima));

  check(cpl_image_save(scharr_x_ima,"scharr_x.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(scharr_y_ima,"scharr_y.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

  /* this is not robust despite more useful
  for(j=1;j<sy-1;j++) {
    check(max=cpl_image_get_max_window(scharr_x_ima,0,j,sx,j));
    check(cpl_image_divide_scalar(scharr_x_ima,max));
  }
  */

  check(max=cpl_image_get_max(scharr_x_ima));
  check(cpl_image_divide_scalar(scharr_x_ima,max));

  /* this is not robust despite more useful
  for(j=1;j<sy-1;j++) {
    check(max=cpl_image_get_max_window(scharr_y_ima,0,j,sy,j));
    check(cpl_image_divide_scalar(scharr_y_ima,max));
  }
  */

  check(max=cpl_image_get_max(scharr_y_ima));
  check(cpl_image_divide_scalar(scharr_y_ima,max));


  check(cpl_image_save(scharr_x_ima,"scharr_x_n.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(scharr_y_ima,"scharr_y_n.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));


  check(cpl_image_divide(ifu_flat_ima,slit_flat_ima));
  xsh_free_image(&lx_ima);
  xsh_free_image(&ly_ima);

  check(lx_ima=xsh_sobel_lx(ifu_flat_ima));
  check(ly_ima=xsh_sobel_ly(ifu_flat_ima));

  xsh_free_image(&scharr_x_ima);
  xsh_free_image(&scharr_y_ima);
  check(scharr_x_ima=xsh_scharr_x(ifu_flat_ima));
  check(scharr_y_ima=xsh_scharr_y(ifu_flat_ima));



  check(cpl_image_save(lx_ima,"lx_norm.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(ly_ima,"ly_norm.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(scharr_x_ima,"scharr_x_norm.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check(cpl_image_save(scharr_y_ima,"scharr_y_norm.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

  check(cpl_image_save(ifu_flat_ima,"ifu_norm.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

cleanup:
  xsh_free_image(&ifu_flat_ima);
  xsh_free_image(&slit_flat_ima);
  xsh_free_image(&ratio_ima);
  xsh_free_image(&lx_ima);
  xsh_free_image(&ly_ima);
  xsh_free_image(&scharr_x_ima);
  xsh_free_image(&scharr_y_ima);


  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    xsh_error_dump(CPL_MSG_ERROR);
    return 1;
  } else {
    return 0;
  }

}

/**@}*/
