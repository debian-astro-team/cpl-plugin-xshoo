/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif


/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_test_tools Testing of the startup
 * @ingroup unit_tests
 */
/*----------------------------------------------------------------------------*//*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_fit.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>

#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_STARTUP"


static void xsh_gfit_tests(void);

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main(void)
{
    TESTS_INIT_WORKSPACE(MODULE_ID);
    TESTS_INIT(MODULE_ID);

    check( xsh_gfit_tests() );

  cleanup:
    TESTS_CLEAN_WORKSPACE(MODULE_ID);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
	xsh_error_dump(CPL_MSG_ERROR);
	return 1;
    } else {
	return 0;
    }
}


static void xsh_gfit_tests(void)
{
  cpl_image* img_raw=NULL;
  cpl_image* img_raw1=NULL;
  cpl_image* img_noise=NULL;
  double sx=128;
  double sy=128;
  double sigx=4;
  double sigy=4;
  double sigx1=2;
  double sigy1=2;

  int xc=sx/2;
  int yc=sy/2;
  int offx1=20;
  int offy1=20;

  int xc1=sx/2+offx1;
  int yc1=sy/2+offy1;
  double flux1=5.e4;
  double flux=5.e4;

  double noise=1.e2;
  double min=-noise;
  double max=+noise;
  const char* name="raw_ima.fits";
  int offx=12;
  int offy=12;
  int size=20;
  int xg=(int)(xc+offx);
  int yg=(int)(yc+offy);
  double fwhm_x=0;
  double fwhm_y=0;
  double xcen=0;
  double ycen=0;
  double sig_x=0;
  double sig_y=0;
  double norm=0;

  check(img_raw=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));
  check(img_raw1=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));
  check(img_noise=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));


  check(cpl_image_fill_gaussian(img_raw,xc,yc,flux,sigx,sigy));
  check(cpl_image_fill_gaussian(img_raw1,xc1,yc1,flux1,sigx1,sigy1));
  check(cpl_image_fill_noise_uniform(img_noise,min,max));
  check(cpl_image_add(img_raw,img_noise));
  check(cpl_image_add(img_raw,img_raw1));

  check(cpl_image_save(img_raw,name,CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

  //check(cpl_image_fit_gaussian(img_raw,xg,yg,size,&norm,&xcen,&ycen,
  //     &sig_x,&sig_y,&fwhm_x,&fwhm_y));
  check(xsh_image_find_barycenter(img_raw,xg,yg,size,&norm,&xcen,&ycen,
  	       &sig_x,&sig_y,&fwhm_x,&fwhm_y));
  xsh_msg("xc=%d yc=%d xg=%d yg=%d xcen=%f ycen=%f S/N=%f",
	  xc,yc,xg,yg,xcen,ycen,norm/noise);

  xsh_free_image(&img_raw);
  xsh_free_image(&img_raw1);
  xsh_free_image(&img_noise);

  cleanup:
    return;
}



