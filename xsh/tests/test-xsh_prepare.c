/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-04-26 14:10:08 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_prepare  Test Prepare functions 
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_PREPARE"

#define SYNTAX \
  "Test the xsh_prepare function \n"\
  "  transform RAW frames from a sof in PRE files\n"\
  "use : ./test_xsh_prepare SOF\n"\
  "  SOF   => the raw data FITS file\n"

/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of XSH_PREPARE
  @return   0 if tests passed successfully

  Test the Data Reduction Library function XSH_PREPARE
 */
/*--------------------------------------------------------------------------*/

int main(int argc, char** argv)
{
  xsh_instrument *instrument = NULL ;
  xsh_pre *pre = NULL;
  const char *sof_name = NULL;
  cpl_frame *master_bias_frame = NULL;
  cpl_frame *bp_map_frame = NULL; 
  cpl_frameset *set = NULL;
  cpl_frameset *raws = NULL;
  cpl_frameset *calib = NULL;
  int ret=0;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    sof_name = argv[1];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    return 0;
  }

  XSH_ASSURE_NOT_NULL( sof_name);
  check( set = sof_to_frameset( sof_name));

  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));
  XSH_NEW_FRAMESET( raws);
  XSH_NEW_FRAMESET( calib);
  check( xsh_dfs_split_in_group( set, raws, calib));
  check( bp_map_frame = xsh_find_bpmap( calib));
  check( master_bias_frame = xsh_find_master_bias( calib, instrument));

#if 0
  for(i=0;i<3;i++){
    cpl_frame * frame = NULL;
    char framename[256];

    sprintf(framename,"frame%d.fits",i);
    frame = xsh_test_create_frame(framename,10,10,
       XSH_BIAS_UVB,CPL_FRAME_GROUP_RAW, instrument);
    cpl_frameset_insert(set,frame);
  }
#endif
  /* USE prepare function */
  check(xsh_prepare( raws, bp_map_frame, master_bias_frame, 
		     MODULE_ID, instrument,0,CPL_FALSE));
#if 0
  /* TEST1 : load the result with PRE structure */
  for(i=0;i<3;i++){
    cpl_frame * frame = NULL;
    frame = cpl_frameset_get_frame(set,i);
    check(pre = xsh_pre_load(frame,instrument));
    xsh_pre_free(&pre);
  }
#endif
  xsh_msg("load all prepare frame success");

  cleanup:
    xsh_free_frameset(&set);
    xsh_free_frameset( &raws);
    xsh_free_frameset( &calib);
    xsh_instrument_free(&instrument);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_pre_free(&pre);
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    } 
    TEST_END();
    return ret;
} 

/**@}*/
