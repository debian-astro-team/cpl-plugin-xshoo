/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "CPL_IMAGE_FIT_GAUSSIAN"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int test_catalog()
{
  int ret = 0;

  char namei[180];
  char nameo[80];
  cpl_table* tbl=NULL;
  cpl_propertylist* plist=NULL;
  int next=2; //72
  int nrow;
  double* wave=NULL;
  double lambda=0;
  double lambda2=0;
  double lambda4=0;

  double lam_c=0;
  int i=0;
  int j=0;
  double um2angstrom=1.e4;

  cpl_frame* frame=NULL;
  
  sprintf(namei,"/home/amodigli/pipelines/workspace/xshoop/xsh/tests/telluric_model_catalog_vis.fits");
  sprintf(nameo,"telluric_model_catalog_vis_new.fits");
  frame=cpl_frame_new();
  cpl_frame_set_filename(frame,namei);
  cpl_frame_set_type(frame,CPL_FRAME_TYPE_TABLE);
  cpl_frame_set_tag(frame,"TEST");
  cpl_frame_set_group(frame,CPL_FRAME_GROUP_CALIB);
  next=cpl_frame_get_nextensions(frame);

  for(i=0;i<next;i++) {
     xsh_msg_warning("process ext %d",i);
     tbl=cpl_table_load(namei, i+1, 1);
     plist=cpl_propertylist_load(namei,i);
     nrow=cpl_table_get_nrow(tbl);
     xsh_msg("nrow %d",nrow);
     wave=cpl_table_get_data_double(tbl,"lam");
     for(j=0;j<nrow;j++) {
         /* the coefficient below refers to Angstrom units while the telluric
          * model catalog has wavelengths expressed in um
          */
        lambda=1.e4*wave[j];
        lambda2=lambda*lambda;
        lambda4=lambda2*lambda2;
        /* to come back to um we multiply by 10-4 */
        lam_c=1.e-4*lambda/(1.0002735182+131.4182/lambda2+2.76249e8/lambda4);
        wave[j]=lam_c;
     }
     xsh_msg("saving ext %d",i);
     if(i==0) {
        cpl_table_save(tbl,plist,NULL,nameo,CPL_IO_DEFAULT);
     } else {
        cpl_table_save(tbl,plist,NULL,nameo,CPL_IO_EXTEND);
     }


     xsh_msg("saved ext %d",i);
     xsh_free_table(&tbl);
     xsh_free_propertylist(&plist);
  }
  cpl_frame_delete(frame);
 
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}


int main(int argc, char* argv[])
{

    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    //test_catalog();


    return cpl_test_end(0);
}


/**@}*/
