/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_prepare  Test Prepare functions 
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_spectralformat.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_DATA_SPECTRALFORMAT"

#define SYNTAX \
  "Test the xsh_data_spectralformat function \n"\
  "  analyse SPECTRAL_FORMAT tab frames\n"\
  "use : ./test_xsh_data_spectralformat SOF\n"\
  "  SOF   => [SPECTRAL_FORMAT, ORDER_TAB_EDGES, WAVE_TAB_2D|MODEL ]\n"

/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief
    Test the spectral format
  @return
    0 if tests passed successfully
*/
/*--------------------------------------------------------------------------*/

int main(int argc, char** argv)
{
  xsh_instrument *instrument = NULL ;
  const char *sof_name = NULL;
  cpl_frame *spectralformat_frame = NULL;
  cpl_frame *orderlist_frame = NULL;
  cpl_frame *wavesol_frame = NULL;
  cpl_frame *model_frame = NULL;
  cpl_frameset *set = NULL;
  int ret=0;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    sof_name = argv[1];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    return 0;
  }

  /* Create frameset from sof */
  check( set = sof_to_frameset( sof_name));

  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));

  check( spectralformat_frame = xsh_find_spectral_format( set, instrument));
  check( orderlist_frame = xsh_find_order_tab_edges( set, instrument));
  wavesol_frame = xsh_find_wave_tab_2d( set, instrument);

  /* USE load function */
  xsh_msg("SPECTRALFORMAT : %s", 
    cpl_frame_get_filename( spectralformat_frame));
  xsh_msg("ORDERLIST      : %s",
    cpl_frame_get_filename( orderlist_frame));
  if (wavesol_frame != NULL){
    xsh_msg("WAVESOL        : %s",
      cpl_frame_get_filename( wavesol_frame));
  }
  else{
    check( model_frame = 
      xsh_find_frame_with_tag( set, XSH_MOD_CFG_TAB, instrument));
    xsh_msg("MODEL          : %s",
      cpl_frame_get_filename( model_frame));
  }
  check( xsh_data_check_spectralformat( spectralformat_frame,
    orderlist_frame, wavesol_frame, model_frame, instrument));

  cleanup:
    xsh_free_frameset( &set);
    xsh_instrument_free( &instrument);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    } 
    TEST_END();
    return ret;
} 

/**@}*/
