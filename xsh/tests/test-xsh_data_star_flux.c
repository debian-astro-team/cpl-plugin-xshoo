/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:35:23 $
 * $Revision: 1.3 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_test_order     Test Data type ORDER functions
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_star_flux.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_DATA_STAR_FLUX"

#define SYNTAX "Test the order table\n"\
  "usage : test_xsh_data_star_flux std_star_flux_table \n"\
  "std_star_flux_table   => the Standard Star Flux tabe FITS file\n"


/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief
    Unit test of xsh_data_star_flux
  @return
    0 if success

*/
/*--------------------------------------------------------------------------*/
int main(int argc, char** argv)
{
  int ret = 0;

  char * star_tab_name = NULL;
  cpl_frame * star_tab_frame = NULL;
  xsh_star_flux_list * star_list = NULL ;
  double * plambda = NULL, *pflux = NULL ;
  int star_tab_size, i ;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if ( optind < argc ) {
    star_tab_name = argv[optind] ;
  }
  else{
    printf(SYNTAX);
    return 0;
  }

  /* Create frames */
  XSH_ASSURE_NOT_NULL( star_tab_name);
  star_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( star_tab_frame, star_tab_name) ;
  cpl_frame_set_level( star_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( star_tab_frame, CPL_FRAME_GROUP_CALIB );

  check( star_list = xsh_star_flux_list_load( star_tab_frame ) ) ;
  star_tab_size = star_list->size ;
  xsh_msg( "Star Table size: %d", star_tab_size ) ;

  /* Dump star data */
  plambda = star_list->lambda ;
  pflux = star_list->flux ;

  for ( i = 0 ; i < star_tab_size ; i++, plambda++, pflux++ ) {
    xsh_msg( "   %3d: %lf %lf", i, *plambda, *pflux ) ;
  } 
  /* dump to file */
  {
    FILE * fout ;

    fout = fopen( "star_flux.dat", "w" ) ;
    plambda = star_list->lambda ;
    pflux = star_list->flux ;
    for ( i = 0 ; i < star_tab_size ; i++, plambda++, pflux++ )
      fprintf( fout, "%lf %lf\n", *plambda, *pflux ) ;
    fclose( fout ) ;
  }

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    xsh_star_flux_list_free( &star_list);

    return ret ;
}

/**@}*/
