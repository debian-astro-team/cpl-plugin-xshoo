/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "CPL_IMAGE_FIT_GAUSSIAN"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  char name1[256];
  char name2[256];

  cpl_table* tab1=NULL;
  cpl_table* tab2=NULL;
  cpl_table* tabo=NULL;
  cpl_propertylist* plist=NULL;

  int nrows;
  double* pratio=NULL;
  double* praw1=NULL;
  double* praw2=NULL;
  float* pwav1=NULL;
  double* pwavo=NULL;
  double mean=0;
  double rms=0;
  double median=0;
  double min=0;
  double max=0;
  int i=0;
  xsh_msg("argc=%d",argc);
  if((size_t)argc != 3){
      xsh_msg_error("Provide two inputs: resp1, resp2 tables");
  } else {
      sprintf(name1,argv[1]);
      sprintf(name2,argv[2]);
  }

  tab1=cpl_table_load(name1, 1, 1);
  tab2=cpl_table_load(name1, 1, 1);
  nrows=cpl_table_get_nrow(tab1);

  tabo=cpl_table_new(nrows);
  cpl_table_new_column(tabo,"wave",CPL_TYPE_DOUBLE);
  cpl_table_new_column(tabo,"ratio",CPL_TYPE_DOUBLE);

  cpl_table_fill_column_window_double(tabo, "ratio", 0, nrows, 0);
  cpl_table_fill_column_window_double(tabo, "wave", 0, nrows, 0);
  pratio=cpl_table_get_data_double(tabo,"ratio");
  praw1=cpl_table_get_data_double(tab1,"REF_DIV_OBS");

  praw2=cpl_table_get_data_double(tab2,"REF_DIV_OBS");
  pwav1=cpl_table_get_data_float(tab1,"LAMBDA");
  pwavo=cpl_table_get_data_double(tabo,"wave");
  for(i=0;i<nrows;i++) {
     pwavo[i]=(double)pwav1[i];
     pratio[i]=praw1[i]/praw2[i];
  }

  mean=cpl_table_get_column_mean(tabo,"ratio");
  median=cpl_table_get_column_median(tabo,"ratio");
  min=cpl_table_get_column_min(tabo,"ratio");
  max=cpl_table_get_column_max(tabo,"ratio");
  rms=cpl_table_get_column_stdev(tabo,"ratio");

  plist=cpl_propertylist_new();
  cpl_propertylist_append_double(plist,"MEAN",mean);
  cpl_propertylist_append_double(plist,"MEDIAN",median);
  cpl_propertylist_append_double(plist,"RMS",rms);
  cpl_propertylist_append_double(plist,"MIN",min);
  cpl_propertylist_append_double(plist,"MAX",max);

  cpl_table_save(tabo,plist,NULL,"ratio.fits",CPL_IO_DEFAULT);

  cpl_table_delete(tab1);
  cpl_table_delete(tab2);
  cpl_table_delete(tabo);
  cpl_propertylist_delete(plist);
  xsh_msg_warning("statistic: mean: %g median: %g rms: %g min: %g max: %g",
                  mean,median,rms,min,max);
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}

/**@}*/
