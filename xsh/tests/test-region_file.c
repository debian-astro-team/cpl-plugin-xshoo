/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>
#include <cpl.h>
#include <xsh_msg.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main(int argc, char **argv)
{
   
    cpl_image* ima=NULL;
    int sx=100;
    int sy=100;
    double value=100;
    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    ima=cpl_image_new(sx,sy,CPL_TYPE_DOUBLE);
    cpl_image_add_scalar(ima,1);

    FILE* region_file = fopen( "ima.reg", "w");
    fprintf( region_file, "# Region file format: DS9 version 4.0\n\
    global color=red font=\"helvetica 4 normal\"\
    select=1 highlite=1 edit=1 move=1 delete=1 include=1 fixed=0 source \nimage\n");

    for(int j=0.4*sy;j<0.5*sy;j++) {
      for(int i=0.4*sy;i<0.5*sy;i++) {
        value=i*j;
	cpl_image_set(ima,i,j,value);
        xsh_msg_debug("ima[%d,%d]=%g",i,j,value);
        fprintf( region_file, "point(%d,%d) #point=cross color=yellow "\
		 "font=\"helvetica 10 normal\"  text={%d %d}\n", i, j, i,j);
      }
    }
    cpl_image_save(ima,"ima.fits",CPL_TYPE_FLOAT,NULL,CPL_IO_DEFAULT);
    fclose(region_file);
    cpl_image_delete(ima);
    return cpl_test_end(0);
}


/**@}*/
