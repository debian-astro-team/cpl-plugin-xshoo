/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: rhaigron $
 * $Date: 2010-09-25 13:45:38 $
 * $Revision: 1.6 $
 *
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_slitmap     Test Data type ORDER functions
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_order.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>
#include <xsh_drl.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_SLITMAP"

enum {
  BINX_OPT, BINY_OPT,DEBUG_OPT, HELP_OPT
} ;

static struct option LongOptions[] = {
  {"debug", required_argument, 0, DEBUG_OPT},
  {"help", 0, 0, HELP_OPT},
  {NULL, 0, 0, 0}
} ;


static void Help( void )
{
  puts ("Computes the slilet sizes with Order Table and SLIT MAP");
  puts( "Usage : ./test_xsh_slitmap <sof>");

  puts( "Options" ) ;
  puts( " --debug=<n>        : Level of debug LOW | MEDIUM | HIGH [MEDIUM]" );
  puts( " --help             : What you see" ) ;

  puts( "The input files argument MUST be in this order:" ) ;
  puts( " 1. SOF [IFU_MAP|SLIT_MAP]");

  TEST_END();
  exit(0);
}

static void HandleOptions( int argc, char ** argv)
{
  int opt ;
  int option_index = 0;

  while( (opt = getopt_long( argc, argv, "debug:help",
                             LongOptions, &option_index )) != EOF )
    switch( opt ) {
    case DEBUG_OPT:
      if ( strcmp( optarg, "LOW")==0){
        xsh_debug_level_set( XSH_DEBUG_LEVEL_LOW);
      }
      else if ( strcmp( optarg, "HIGH")==0){
        xsh_debug_level_set( XSH_DEBUG_LEVEL_HIGH);
      }
      break;
    case HELP_OPT:
      Help();
      break;
    default:
      break;
    }
}

/*--------------------------------------------------------------------------*/
/**
  @brief
    Unit test of xsh_slitmap
  @return
    0 if success

*/
/*--------------------------------------------------------------------------*/
int main(int argc, char** argv)
{
  int ret = 0;
  xsh_instrument *instrument = NULL ;

  const char *sof_name = NULL;
  cpl_frameset *set = NULL;
  cpl_frame * slitmap_frame = NULL ;
  double sd, su, sld, slu;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  HandleOptions( argc, argv);

  if ( (argc-optind) >= 1 ) {
    sof_name = argv[optind];
  }
  else {
    Help();
    exit( 0);
  }

  /* Create frameset from sof */
  check( set = sof_to_frameset( sof_name));

  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));

  check( slitmap_frame = xsh_find_slitmap( set, instrument));
  xsh_msg("SLITMAP        : %s",
    cpl_frame_get_filename( slitmap_frame));

  check( xsh_get_slit_edges( slitmap_frame, &sd, &su, &sld, &slu, instrument));

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    xsh_free_frameset( &set);
    xsh_instrument_free( &instrument);
    TEST_END();
    return ret ;
}

/**@}*/
