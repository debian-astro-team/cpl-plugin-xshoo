/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "CPL_IMAGE_FIT_GAUSSIAN"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  char name1[256];
  cpl_table* tab1=NULL;

  double mean=0;
  double rms=0;
  double median=0;
  double min=0;
  double max=0;
  int i=0;

  xsh_msg("argc=%d",argc);
  if((size_t)argc != 2){
      xsh_msg_warning("Provide one inputs: spectra tables");
      return 0;
  } else {
      sprintf(name1,argv[1]);
  }

  tab1=cpl_table_load(name1, 1, 1);

  mean=cpl_table_get_column_mean(tab1,"counts_bkg");
  median=cpl_table_get_column_median(tab1,"counts_bkg");
  min=cpl_table_get_column_min(tab1,"counts_bkg");
  max=cpl_table_get_column_max(tab1,"counts_bkg");
  rms=cpl_table_get_column_stdev(tab1,"counts_bkg");

  xsh_msg_warning("Spectrum %s mean: %g median: %g min: %g max: %g rms: %g",
          name1,mean,median,min,max,rms);
  cpl_table_delete(tab1);

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}

/**@}*/
