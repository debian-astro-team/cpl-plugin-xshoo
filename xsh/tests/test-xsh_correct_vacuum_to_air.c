/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-08-02 13:29:37 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Apply formula:
    lam_air = lam_vac/(1.0002735182+131.4182/lam_vac2+2.76249E8/lam_vac4)
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <sys/time.h>
#include <xsh_cpl_size.h>
#include <getopt.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_CORRECT_VACUUM_TO_AIR"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;

  cpl_table* table_names = NULL;
  cpl_table* table_spect = NULL;
  cpl_frame* std_catalog = NULL;
  cpl_propertylist* phead = NULL;
  cpl_propertylist* xhead = NULL;

  //xsh_instrument* instrument = NULL;
  const char* fname_out = "catalog_new.fits";
  const char* fname_inp;

  int nb_frames = 0;
  int next = 0;
  int i = 0;
  int nrow = 0;
  int j = 0;

  double* plambda = NULL;
  //double* pflux = NULL;
  //double* pbin = NULL;
  double c1 = 1.0002735182;
  double c2 = 131.4182;
  double c4 = 2.76249E8;
  double w = 0;
  double w2 = 0;
  double w4 = 0;
  double wair=0;

  double nm2AA=10.;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  nb_frames = argc - optind;
  if (nb_frames == 1) {
    fname_inp = argv[optind];
  } else {
    xsh_msg( "********** NOT ENOUGH INPUT FRAMES **********" );
    exit(0);
  }

  std_catalog = cpl_frame_new();
  cpl_frame_set_filename(std_catalog, fname_inp);
  next = cpl_frame_get_nextensions(std_catalog);
  phead = cpl_propertylist_load(fname_inp, 0);
  table_names = cpl_table_load(fname_inp, 1, 0);
  xhead = cpl_propertylist_load(fname_inp, 1);
  cpl_table_save(table_names, phead, xhead, fname_out, CPL_IO_DEFAULT);

  for (i = 2; i <= next; i++) {

    table_spect = cpl_table_load(fname_inp, i, 0);
    xhead = cpl_propertylist_load(fname_inp, i);
    plambda = cpl_table_get_data_double(table_spect, "LAMBDA");
    nrow = cpl_table_get_nrow(table_spect);

    if (i == 2 || i == 4) {
      /* only for GD71,GD153, that are at extention 2,4 of catalogue apply correction */
      for (j = 0; j < nrow; j++) {
        w = nm2AA * plambda[j];
        w2 = w * w;
        w4 = w2*w2;
        wair = w / (c1 + c2 / w2 + c4 / w4 );
        plambda[j] = wair / nm2AA;
        //xsh_msg("w_cor star[%d]= %g [AA]",i, w-wair);
      }
    }
    xsh_msg("save out tab on file %s",fname_out);
    check(cpl_table_save(table_spect, xhead, NULL, fname_out, CPL_IO_EXTEND));
    xsh_free_propertylist(&xhead);
    xsh_free_table(&table_spect);

  }


  cleanup: xsh_free_table(&table_names);
  xsh_free_table(&table_spect);
  xsh_free_propertylist(&xhead);
  xsh_free_propertylist(&phead);
  xsh_free_frame(&std_catalog);

  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    xsh_error_dump(CPL_MSG_ERROR);
    ret = 1;
  }
  return ret;
}

/**@}*/
