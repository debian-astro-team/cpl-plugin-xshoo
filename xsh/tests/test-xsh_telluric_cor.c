/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2013-05-14 07:02:49 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_data_spectrum.h>
#include <xsh_data_instrument.h>
#include <xsh_utils_response.h>
#include <xsh_msg.h>
#include <xsh_error.h>
#include <cpl.h>



#include <xsh_utils.h>
#include <tests.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_TELLURIC_COR"


/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;
  char *name_model = NULL;
  char *name_spectrum = NULL;
  xsh_spectrum* s=NULL;
  cpl_frame* frame_s=NULL;
  cpl_frame* frame_m=NULL;
  //int i=0;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  xsh_instrument* instrument = NULL;
  //int status=0;
  //double model_mean=0;
  //double model_rms=0;
  cpl_size model_idx=0;

  //double wstp=0;
  cpl_table* tab_res=NULL;

  if (argc > 1){
    name_spectrum = argv[1];
    name_model = argv[2];
  }
  else{
    return 0;
  }

  instrument = xsh_instrument_new();
  xsh_instrument_set_arm(instrument, XSH_ARM_NIR);

  frame_m=cpl_frame_new();
  cpl_frame_set_filename(frame_m,name_model);
  cpl_frame_set_type(frame_m,CPL_FRAME_TYPE_TABLE);

  /* load input observed spectrum and convert it into a table 
     to easy to perform following corrections */
  frame_s=cpl_frame_new();

  cpl_frame_set_filename(frame_s,name_spectrum);
  cpl_frame_set_type(frame_s,CPL_FRAME_TYPE_IMAGE);
  //xsh_msg("no extension=%d",(int)cpl_frame_get_nextensions(frame));
  check(s=xsh_spectrum_load(frame_s));

  check(tab_res=xsh_telluric_model_eval(frame_m,s,instrument,&model_idx));

  cleanup:
  xsh_instrument_free(&instrument);
  xsh_spectrum_free(&s);
  xsh_free_table(&tab_res);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}

/**@}*/
