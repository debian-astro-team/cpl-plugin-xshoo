/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_test_dispersol
 *   Test the dispersol list
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_order.h>
#include <xsh_data_dispersol.h>
#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_DATA_DISPERSOL"

#define SYNTAX "Test the dispersion file by producing WAVEMAP and SLITMAP\n"\
  "usage : ./the_xsh_data_dispersol DISPERSOL_TAB ORDER_TAB PRE\n"\
  "DISPERSOL_TAB => the dispersol table\n"\
  "ORDER_TAB     => the order table\n"\
  "PRE           => the pre image\n"

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
 *  @brief
      Unit test of xsh_data_dispersol
  @return
    0 if success
*/
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;
  xsh_instrument * instrument = NULL ;
  XSH_INSTRCONFIG* iconfig = NULL;

  const char* order_tab_name = NULL;
  const char* dispersol_tab_name = NULL;
  const char* pre_name = NULL;
  cpl_frame *order_tab_frame = NULL;
  cpl_frame *dispersol_tab_frame = NULL; 
  cpl_frame *pre_frame = NULL;
  cpl_table* table = NULL;
  cpl_frame *wavemap_frame = NULL;
  cpl_frame *slitmap_frame = NULL;
  int nbcol;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if ( argc > 3) {
    dispersol_tab_name = argv[1];
    order_tab_name = argv[2];
    pre_name = argv[3];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    return 0;
  }
  xsh_msg("Dispersol tab file %s", dispersol_tab_name);
  xsh_msg("Order tab file     %s", order_tab_name);
  xsh_msg("Pre file           %s", pre_name);


  /* Create frames */
  instrument = xsh_instrument_new() ;
  xsh_instrument_set_arm( instrument, XSH_ARM_UVB); 

  XSH_ASSURE_NOT_NULL( pre_name);
  pre_frame = cpl_frame_new();
  cpl_frame_set_filename( pre_frame, pre_name) ;
  cpl_frame_set_level( pre_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( pre_frame, CPL_FRAME_GROUP_RAW);
  cpl_frame_set_tag( pre_frame, "BIAS_UVB");

  XSH_ASSURE_NOT_NULL( order_tab_name);
  order_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( order_tab_frame, order_tab_name) ;
  cpl_frame_set_level( order_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( order_tab_frame, CPL_FRAME_GROUP_RAW );
  
  XSH_TABLE_LOAD( table, order_tab_name);
  check( nbcol =  cpl_table_get_nrow(table));
  
  XSH_ASSURE_NOT_NULL( instrument);

  check (iconfig = xsh_instrument_get_config( instrument));
  iconfig->orders = nbcol;

  XSH_ASSURE_NOT_NULL( dispersol_tab_name);
  dispersol_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( dispersol_tab_frame, dispersol_tab_name) ;
  cpl_frame_set_level( dispersol_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( dispersol_tab_frame, CPL_FRAME_GROUP_RAW);

  xsh_msg("Create wavemap and slitmap");
  check( xsh_create_map( dispersol_tab_frame, order_tab_frame, 
			 pre_frame, instrument, 
                         &wavemap_frame, &slitmap_frame,"test"));
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    XSH_TABLE_FREE( table);
    xsh_instrument_free( &instrument);
    xsh_free_frame( &pre_frame);
    xsh_free_frame( &order_tab_frame);
    xsh_free_frame( &dispersol_tab_frame);
    xsh_free_frame( &wavemap_frame);
    xsh_free_frame( &slitmap_frame);
    TEST_END();
    return ret ;
}

/**@}*/
