/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2013-01-31 08:05:57 $
 * $Revision: 1.22 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_tests_create_master_bias Test Create Master Bias
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <string.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_CREATE_MASTER_DARK"
/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of XSH_CREATE_MASTER
  @return   0 iff success

  Test the Data Reduction Library function XSH_CREATE_MASTER
 */
/*--------------------------------------------------------------------------*/

int main(void)
{
  xsh_instrument* instrument = NULL;
  cpl_frame* frame = NULL;
  cpl_frame* med_frame = NULL;
  cpl_frame* mbias = NULL;
  cpl_frameset* set = NULL;
  xsh_pre* pre = NULL;
  float mean = 0.0,median = 0.0, stdev = 0.0;
  xsh_clipping_param crh_clipping = {0.3, 4, 2, 0.7, 0};
  xsh_hot_cold_pix_param hp_clip_param = {0, 3.0, 3, 3.0, 3};
  xsh_fpn_param fpn_param = {1,1,9,9,4,10};
  xsh_ron_param ron_param = {"ALL",
                             4,100,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,25};
  xsh_struct_param struct_param = {-1,-1};
  const char* MBIAS_RECIPE_ID="xsh_mbias";
  cpl_parameterlist* parameters=NULL;
  xsh_stack_param stack_param = {"median",5.,5.};
  const char* ftag=NULL;
  cpl_frame* product=NULL;
  char* name=NULL;
  cpl_propertylist* header=NULL;
  cpl_image* ima=NULL;
  int nframes=5;
  const int decode_bp=2147483647;
  /* Initialize libraries */
  TESTS_INIT_WORKSPACE(MODULE_ID);
  TESTS_INIT(MODULE_ID);

  /* Create valid instrument */
  instrument = xsh_instrument_new() ;
  xsh_instrument_set_mode( instrument, XSH_MODE_IFU ) ;
  xsh_instrument_set_arm( instrument, XSH_ARM_UVB ) ;
  xsh_instrument_set_lamp( instrument, XSH_LAMP_QTH ) ;
  xsh_instrument_set_recipe_id(instrument, "xsh_mbias");

  ftag=XSH_GET_TAG_FROM_ARM(XSH_MASTER_BIAS,instrument);

  /* Set generic parameters (common to all recipes) */
  parameters = cpl_parameterlist_new();
  check(xsh_parameters_generic( MBIAS_RECIPE_ID, parameters ));

  /* crh clipping params */
  check(xsh_parameters_clipping_crh_create(MBIAS_RECIPE_ID,parameters,crh_clipping));

  check(xsh_parameters_hot_cold_pix_create(MBIAS_RECIPE_ID,parameters,hp_clip_param));


  check(xsh_parameters_fpn_create(MBIAS_RECIPE_ID,parameters,fpn_param));
  check(xsh_parameters_ron_create(MBIAS_RECIPE_ID,parameters,ron_param));
  check(xsh_parameters_struct_create(MBIAS_RECIPE_ID,parameters,struct_param));


  /* Create a frame and add it to frameset*/
  /* Create a frame and add it to frameset*/
  set = cpl_frameset_new();
  for(int i=0;i<nframes;i++) {
     name=cpl_sprintf("frame%d.fits",i);
     frame = xsh_test_create_frame(name,20,20,
       XSH_DARK_UVB,CPL_FRAME_GROUP_RAW, instrument);
     cpl_frameset_insert(set,cpl_frame_duplicate(frame));
     xsh_free_frame(&frame);
     cpl_free(name);
  }

  xsh_instrument_set_decode_bp( instrument, decode_bp ) ;
  /* Create a PRE frame */
  check(xsh_prepare(set,NULL, NULL, "PRE",instrument,0,CPL_FALSE));

  /* Remove cosmics */
  check (med_frame = xsh_remove_crh_multiple( set, "remove_crh.fits",
                                              &stack_param,NULL,
                                              instrument,
                                              NULL,NULL,0 ));
  /* Convert this frame in MASTER BIAS */
  check(mbias = xsh_create_master_bias2(set,&stack_param,instrument,ftag,0));
  check(product=xsh_compute_qc_on_master_bias(set,mbias,instrument,parameters));
  name=cpl_strdup(cpl_frame_get_filename(product));
  header=cpl_propertylist_load(name,0);
  ima=cpl_image_load(name,CPL_TYPE_FLOAT,0,0);
  xsh_free_frame(&product);
  check(product=xsh_frame_product(name,ftag,
                                   CPL_FRAME_TYPE_IMAGE,
                                   CPL_FRAME_GROUP_PRODUCT,
                                   CPL_FRAME_LEVEL_FINAL));
  cpl_free(name);

  /* Check the format of new frame */
  //check(pre = xsh_pre_load(mbias,instrument));
  
  /* check the header */
  assure( strcmp(xsh_pfits_get_pcatg(header),"MASTER_BIAS_UVB") 
    == 0,CPL_ERROR_ILLEGAL_OUTPUT,"Wrong pcatg keyword");
  /* QC parameters */
  check(mean = xsh_pfits_get_qc_mbiasavg (header));
  check(median = xsh_pfits_get_qc_mbiasmed(header));
  check(stdev = xsh_pfits_get_qc_mbiasrms (header));

  assure( mean - cpl_image_get_mean(ima) < XSH_FLOAT_PRECISION,
    CPL_ERROR_ILLEGAL_OUTPUT,"Wrong mean value in QC");
  assure( median - cpl_image_get_median(ima) < XSH_FLOAT_PRECISION,
    CPL_ERROR_ILLEGAL_OUTPUT,"Wrong median value in QC"); 
  assure( stdev - cpl_image_get_stdev(ima) < XSH_FLOAT_PRECISION,
    CPL_ERROR_ILLEGAL_OUTPUT,"Wrong rms value in QC");
  xsh_msg("header is ok");
 
  /* verify tag and group */
  assure(cpl_frame_get_group(product) == CPL_FRAME_GROUP_PRODUCT,
    CPL_ERROR_ILLEGAL_OUTPUT,"Wrong group for MASTER BIAS frame");
  assure(strcmp(cpl_frame_get_tag(product),XSH_MASTER_BIAS_UVB) == 0,
    CPL_ERROR_ILLEGAL_OUTPUT,"Wrong tag (%s) for MASTER BIAS frame ",
    cpl_frame_get_tag(product));
  xsh_msg("frame is ok");
cleanup:
  xsh_free_parameterlist(&parameters);
  xsh_free_frame(&med_frame);
  xsh_pre_free(&pre);  
  xsh_free_frameset(&set);
  xsh_free_frame(&mbias);
  xsh_free_frame(&product);
  xsh_free_propertylist(&header);
  xsh_free_image(&ima);

  xsh_instrument_free(&instrument);
  xsh_free_temporary_files();
  TESTS_CLEAN_WORKSPACE(MODULE_ID);
  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    xsh_error_dump(CPL_MSG_ERROR);
    return 1;
  } else {
    return 0;
  }
}

/**@}*/
