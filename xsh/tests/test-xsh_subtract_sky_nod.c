/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-04-26 14:10:08 $
 * $Revision: 1.12 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_subtract_sky_nod     Test xsh_subtract_sky_nod function
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_order.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_SUBTRACT_SKY_NOD"

#define SYNTAX "Test xsh_subtract_sky_nod\n"\
  "usage : ./test_xsh_subtract_sky_nod [<o[tions>] <raw_file1> <raw_file2> ... \n"\
  "  Subtract noded files 2 by 2).\n"\
  "<raw_files> the list of raw files, 2 by 2\n"\

#if 0
static const char * Options = "?" ;

enum {
  POINT_OPT, SIZE_OPT, STEP_OPT, C_COLOR_OPT
} ;

static struct option LongOptions[] = {
  {"point", required_argument, 0, POINT_OPT},
  {"size", required_argument, 0, SIZE_OPT},
  {"step",  required_argument, 0, STEP_OPT},
  {"c-color", required_argument, 0, C_COLOR_OPT},
  {NULL, 0, 0, 0}
} ;

static char PointType[16] ;
static char PointSize[8] ;
static int PointStep = 8 ;
static char CentralColor[32] ;
#endif

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/
#if 0
static void HandleOptions( int argc, char ** argv )
{
  int opt ;
  int option_index = 0;

  /* Set default values */
  strcpy( PointType, "cross" ) ;
  strcpy( PointSize, "" ) ;
  strcpy( CentralColor, "green" ) ;

  while( (opt = getopt_long( argc, argv, Options,
			     LongOptions, &option_index )) != EOF )
    switch( opt ) {
    case POINT_OPT:
      strcpy( PointType, optarg ) ;
      break ;
    case SIZE_OPT:
      strcpy( PointSize, optarg ) ;
      break ;
    case STEP_OPT:
      sscanf( optarg, "%d", &PointStep ) ;
      break ;
    case C_COLOR_OPT:
      strcpy( CentralColor, optarg ) ;
      break ;
    default:
      printf( SYNTAX ) ;
      exit( 0 ) ;
    }
}
#endif

/*--------------------------------------------------------------------------*/
/**
  @brief
    Unit test of xsh_subtract_sky_node
  @return
    0 if success

*/
/*--------------------------------------------------------------------------*/
int main(int argc, char** argv)
{
  int ret = 1;
  xsh_instrument * instrument = NULL ;
  cpl_frameset *set = NULL;
  cpl_frameset *ord_set = NULL;
  int nb_raw_frames , raw_modulo;
  cpl_frameset * pairs = NULL ;
  int mode=0;
  const int decode_bp=2147483647;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;
#if 0
  HandleOptions( argc, argv );
#endif

  /* Analyse parameters */
  if ( argc < 2 ) {
    printf(SYNTAX);
    TEST_END();
    return 0;
  }
  else {
    /* Create frameset from all raw frames */
    int iarg = 1 ;

    check( set = cpl_frameset_new() ) ;
    nb_raw_frames = 0 ;

    for( iarg = 1 ; iarg < argc ; iarg++ ) {
      cpl_frame * input_frame = NULL ;

      check( input_frame = cpl_frame_new() ) ;
      check(cpl_frame_set_filename( input_frame, argv[iarg] ) );
      check(cpl_frame_set_tag(input_frame, "OBJECT_SLIT_NOD_VIS" ) ) ;
      check(cpl_frame_set_group( input_frame, CPL_FRAME_GROUP_RAW ) ) ;
      check(cpl_frameset_insert( set, input_frame ) ) ;
      nb_raw_frames++ ;
    }
  }
  xsh_msg( "Nb of frames: %d", nb_raw_frames ) ;

  instrument = xsh_instrument_new() ;
  XSH_ASSURE_NOT_NULL( instrument);
  xsh_instrument_set_mode( instrument, XSH_MODE_SLIT );
  xsh_instrument_set_arm( instrument, XSH_ARM_VIS); 
  xsh_instrument_set_decode_bp( instrument, decode_bp ) ;

  /* Create frames */
  XSH_ASSURE_NOT_NULL( set );

  raw_modulo = nb_raw_frames % 2;
  XSH_ASSURE_NOT_ILLEGAL( raw_modulo == 0);

  check(xsh_prepare( set, NULL, (cpl_frame *)-1, XSH_OBJECT_SLIT_NOD,
		     instrument,0,CPL_FALSE));
  xsh_msg( "Now call xsh_subtract_sky_nod with %d raw frames",
	   nb_raw_frames ) ;

  check( pairs = xsh_subtract_sky_nod( set,
				       instrument, mode)) ;
  ret = 0 ;

  cleanup:
    xsh_instrument_free( &instrument);
    xsh_free_frameset( &pairs);
    xsh_free_frameset( &set);
    xsh_free_frameset( &ord_set);

    if ( cpl_error_get_code() != CPL_ERROR_NONE){
      ret = 1;
    }
    TEST_END();
    return ret ;
}

/**@}*/
