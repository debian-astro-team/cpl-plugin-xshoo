/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*/
/**
 * @defgroup xsh_multiply_flat Multiply a frame by the flat (xsh_multiply_flat)
 * @ingroup drl_functions
 *
 * This module is a DRL function 
 */
/*---------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/
#include <math.h>

#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_badpixelmap.h>
#include <xsh_data_order.h>
#include <xsh_data_pre.h>
#include <xsh_data_grid.h>

/*---------------------------------------------------------------------------
                            Typedefs
  ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                            Functions prototypes
  ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                              Implementation
  ---------------------------------------------------------------------------*/


/** 
 * @brief multiply PRE frame with the master FLAT frame
 * @param[in] frame frame to multiply
 * @param[in] flat the master flat frame
 * @param[in] tag  pro catg of the result frame
 * @param[in] instr the xsh instrument
 * @return the multiply frame
 */

cpl_frame * xsh_multiply_flat( cpl_frame * frame, cpl_frame * flat,
  const char* tag, xsh_instrument* instr)
{
  /* MUST BE DEALLOCATED in caller */
  cpl_frame * result = NULL;
  /* ALLOCATED locally */
  xsh_pre* xframe = NULL;
  xsh_pre* xflat = NULL;
  char filename[256];

  /* check input */  
  XSH_ASSURE_NOT_NULL(frame);
  XSH_ASSURE_NOT_NULL(flat);
  XSH_ASSURE_NOT_NULL(instr);

  /* load the frames */
  check( xframe = xsh_pre_load( frame, instr));
  check( xflat = xsh_pre_load( flat, instr)); 
  
  /* do the divide operation */
  check( xsh_pre_multiply( xframe, xflat, XSH_MULTIPLY_FLAT_THRESH));
  sprintf( filename, "%s.fits", tag);
  /* save result */

  check(result = xsh_pre_save (xframe, filename, tag,1));

  
  check( cpl_frame_set_tag (result, tag));
  
  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_free_frame(&result);
    }
    xsh_pre_free( &xframe);
    xsh_pre_free( &xflat);
    return result;
}

/**@}*/
