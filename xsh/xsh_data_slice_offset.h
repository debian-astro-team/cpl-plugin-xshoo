/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2009-12-30 15:05:32 $
 * $Revision: 1.5 $
 */
#ifndef XSH_DATA_SLICE_OFFSET_H
#define XSH_DATA_SLICE_OFFSET_H

#include <cpl.h>
#include <xsh_data_instrument.h>


#define XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_UP "CEN_UP_OFFSET"
#define XSH_SLICE_OFFSET_TABLE_UNIT_CEN_UP "arcsec"

#define XSH_SLICE_OFFSET_TABLE_COLNAME_CEN_DOWN "CEN_DOWN_OFFSET"
#define XSH_SLICE_OFFSET_TABLE_UNIT_CEN_DOWN "arcsec"

typedef struct{
  double cen_up;
  double cen_down; 
  cpl_propertylist *header;
} xsh_slice_offset ;


xsh_slice_offset* xsh_slice_offset_create(void);

xsh_slice_offset* xsh_slice_offset_load( cpl_frame* frame);

cpl_propertylist* xsh_slice_offset_get_header(xsh_slice_offset *slice);

void xsh_slice_offset_free(xsh_slice_offset **slice);

cpl_frame* xsh_slice_offset_save( xsh_slice_offset * list, 
  const char* filename, xsh_instrument* instr);

#endif  /* XSH_SLICE_OFSET_H */
