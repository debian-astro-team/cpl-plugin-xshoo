/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-01-10 09:01:58 $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*/
/**
 * @defgroup xsh_data_check Check data format
 * @ingroup data_handling
 */
/*---------------------------------------------------------------------------*/

/**@{*/


/*-----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/
#include <math.h>
#include <xsh_data_spectralformat.h>
#include <xsh_data_order.h>
#include <xsh_data_wavesol.h>
#include <xsh_model_io.h>
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_pfits.h>
#include <cpl.h>
#include <xsh_utils_table.h>
#include <xsh_drl.h>

/*----------------------------------------------------------------------------
  Function implementation
  ----------------------------------------------------------------------------*/

/*****************************************************************************/
/** 
  @brief
    Check the borders of wavelength foreach orders in spectral format
 @param[in] spectralformat_frame
   The spectral format to check
 @param[in] orderlist_frame
   The order list frame
  @param[in] wavesol_frame
    The wave solution frame
  @param[in] model_config_frame
    The model configuration frame
  @param[in] instr
    The instrument
*/
/*****************************************************************************/
void 
xsh_data_check_spectralformat( cpl_frame *spectralformat_frame,
  cpl_frame *orderlist_frame, cpl_frame *wavesol_frame, 
  cpl_frame *model_config_frame, xsh_instrument* instr)
{
  xsh_spectralformat_list *spectralformat = NULL;
  xsh_order_list *orderlist = NULL;
  xsh_wavesol *wavesol = NULL;
  xsh_xs_3 model;  

  XSH_ASSURE_NOT_NULL( spectralformat_frame);
  XSH_ASSURE_NOT_NULL( orderlist_frame);
  XSH_ASSURE_NOT_NULL( instr);

  check( spectralformat = xsh_spectralformat_list_load( spectralformat_frame,
    instr));
  check( orderlist = xsh_order_list_load( orderlist_frame, instr));

  if ( model_config_frame != NULL) {
    check( xsh_model_config_load_best( model_config_frame, &model));
  }
  if ( wavesol_frame != NULL) {
    check( wavesol = xsh_wavesol_load( wavesol_frame, instr));
  }

  check( xsh_spectralformat_check_wlimit( spectralformat, orderlist,
    wavesol, &model, instr));

  cleanup:
    xsh_spectralformat_list_free( &spectralformat);
    xsh_order_list_free( &orderlist);
    xsh_wavesol_free( &wavesol);
    return;
}
/*****************************************************************************/
/**@}*/

