/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:37:36 $
 * $Revision: 1.3 $
 */

#ifndef XSH_DATA_PRE_3D_H
#define XSH_DATA_PRE_3D_H

/*----------------------------------------------------------------------------
                                    Includes
 ----------------------------------------------------------------------------*/
#include <xsh_data_instrument.h>

#include <cpl.h>
#include <xsh_error.h>
#include <xsh_data_image_3d.h>

/*----------------------------------------------------------------------------
                                    Typedefs
 ----------------------------------------------------------------------------*/

#define XSH_PRE_DATA_TYPE CPL_TYPE_FLOAT
#define XSH_PRE_DATA_BPP CPL_BPP_IEEE_FLOAT
#define XSH_PRE_ERRS_TYPE CPL_TYPE_FLOAT
#define XSH_PRE_ERRS_BPP CPL_BPP_IEEE_FLOAT
#define XSH_PRE_QUAL_TYPE CPL_TYPE_INT
#define XSH_PRE_QUAL_BPP CPL_BPP_32_SIGNED

typedef struct {
  xsh_image_3d *data;
  cpl_propertylist *data_header;

  xsh_image_3d *errs;
  cpl_propertylist *errs_header;
    
  xsh_image_3d *qual;
  cpl_propertylist *qual_header;

  cpl_frame_group group;
  /* instrument related with PRE */
  xsh_instrument* instrument; 
  /* NAXIS1 and NAXIS2 keywords */
  int naxis1, naxis2, naxis3 ;
  /* BINX et BINY keywords */
  int binx, biny;
  /* image size (DATA,ERRS,QUAL) in pixels */
  int nx, ny, nz;
} xsh_pre_3d;

#include <xsh_badpixelmap.h>


/*----------------------------------------------------------------------------
                                    Methods
 ----------------------------------------------------------------------------*/

/* Create/destroy */
void xsh_pre_3d_free(xsh_pre_3d ** pre);
xsh_pre_3d * xsh_pre_3d_load(cpl_frame * frame);

cpl_frame* xsh_pre_3d_save( const xsh_pre_3d *pre, const char *filename,
			    int temp);

xsh_pre_3d * xsh_pre_3d_new(int size_x, int size_y, int size_z );

/* get */
int xsh_pre_3d_get_nx(const xsh_pre_3d *pre);
int xsh_pre_3d_get_ny(const xsh_pre_3d *pre);
int xsh_pre_3d_get_nz(const xsh_pre_3d *pre);

cpl_frame_group xsh_pre_3d_get_group(const xsh_pre_3d * pre);
cpl_mask * xsh_pre_3d_get_bpmap(const xsh_pre_3d * pre);

/* Accessor functions
 * (const/non-const variants of each) 
 */
xsh_image_3d * xsh_pre_3d_get_data( xsh_pre_3d * pre_3d ) ;
xsh_image_3d * xsh_pre_3d_get_errs( xsh_pre_3d * pre_3d ) ;
xsh_image_3d * xsh_pre_3d_get_qual( xsh_pre_3d * pre_3d ) ;

const
cpl_propertylist *xsh_pre_3d_get_header_const(const xsh_pre_3d *pre);
cpl_propertylist *xsh_pre_3d_get_header      (      xsh_pre_3d *pre);

#endif
