/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-03-13 09:31:15 $
 * $Revision: 1.11 $
 */

#if !defined(XSH_DATA_STAR_FLUX_H)
#define XSH_DATA_STAR_FLUX_H


#include <cpl.h>

#define XSH_STAR_FLUX_LIST_COLNAME_WAVELENGTH "LAMBDA"
#define XSH_STAR_FLUX_LIST_COLNAME_FLUX "FLUX"

typedef struct {
  int size ;
  cpl_propertylist * header ;
  double * lambda ;
  double * flux ;
} xsh_star_flux_list ;


cpl_error_code 
xsh_star_flux_list_filter_median(xsh_star_flux_list * result, 
                                 int hsize );

cpl_error_code 
xsh_star_flux_list_filter_lowpass(xsh_star_flux_list * result, 
				  cpl_lowpass        filter_type,
                                  int hsize );

xsh_star_flux_list * xsh_star_flux_list_load( cpl_frame * star_frame ) ;
xsh_star_flux_list * xsh_star_flux_list_load_spectrum( cpl_frame * star_frame );

void xsh_star_flux_list_free( xsh_star_flux_list ** list ) ;
xsh_star_flux_list * xsh_star_flux_list_create( int size ) ;
cpl_frame * xsh_star_flux_list_save( xsh_star_flux_list * list,
				     const char * filename,
				     const char * tag ) ;
cpl_frame * 
xsh_star_flux_list_save_order( xsh_star_flux_list * list,
                               const char * filename, 
                               const char * tag,
                               const int order );

double * xsh_star_flux_list_get_lambda( xsh_star_flux_list * list ) ;
double * xsh_star_flux_list_get_flux( xsh_star_flux_list * list ) ;
cpl_error_code 
xsh_star_flux_list_dump_ascii( xsh_star_flux_list * list, const char* filename );
cpl_error_code 
xsh_star_flux_list_divide( xsh_star_flux_list * result,  xsh_star_flux_list * factor);
xsh_star_flux_list* xsh_star_flux_list_duplicate( xsh_star_flux_list * list );
void xsh_star_flux_list_extrapolate_wave_end( xsh_star_flux_list * list , const double wmax);

cpl_error_code
xsh_star_flux_list_to_frame( xsh_star_flux_list* list, cpl_frame* frame);
#endif
