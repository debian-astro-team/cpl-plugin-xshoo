/* $Id: xsh_model_cputime.h,v 1.3 2008-05-23 09:05:30 amodigli Exp $
 *
 * This file is part of the ESO X-shooter Pipeline                          
 * Copyright (C) 2006 European Southern Observatory   
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* cputime.h           declarations for CPU timing call  */
/* rcsid: %W% %U% %G%   EFC */
/* if this is used while in SYSV you need to add     -lbsd   to the link step */

#ifndef XSH_MODEL_CPUTIME_H
#define XSH_MODEL_CPUTIME_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef NO_PROTO
void xsh_report_cpu();
void get_cpu_time();
#else
void xsh_report_cpu(FILE *, char *);
void get_cpu_time(double *usertime, double *systime);
#endif

#ifdef __cplusplus
}
#endif


#endif
