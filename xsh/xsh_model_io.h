/* $Id: xsh_model_io.h,v 1.7 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the ESO X-shooter Pipeline                          
 * Copyright (C) 2006 European Southern Observatory 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_MODEL_IO_H
#define XSH_MODEL_IO_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "xsh_model_kernel.h"

/*-----------------------------------------------------------------------------
                                Define
 -----------------------------------------------------------------------------*/

#define     DEG2RAD     0.017453292519943295

/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

void xsh_model_io_dump( xsh_xs_3 *) ;
cpl_error_code xsh_model_config_load_best(cpl_frame *,  xsh_xs_3 *) ;
int  xsh_model_readfits(double * abest, double * amin, double * amax,
                        int * aname, const char *,  const char *,
                        xsh_xs_3 *, ann_all_par *);
cpl_table * xsh_model_io_output_cfg( xsh_xs_3 *p_xs_3);
cpl_table * xsh_load_table_check(const char *, const char *) ; 
void xsh_model_io_output_cfg_txt( xsh_xs_3 *p_xs_3);
#endif
