/* $Id: xsh_model_metric.h,v 1.7 2011-12-02 14:15:28 amodigli Exp $
 *
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_MODEL_METRIC_H
#define XSH_MODEL_METRIC_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

cpl_table * xsh_model_anneal_comp(ann_all_par *p_all_par, int adim, double * abest, double * amin, double * amax,int * aname, struct xs_3 *p_xs_3, int DS_size, coord *msp_coord, DOUBLE *p_wlarray, DOUBLE ** ref_ind, int maxit); 
float xsh_3_energy (double *a);
void xsh_model_assign_xsh(int loc, double val);
void xsh_model_output_data(double *);

#endif
