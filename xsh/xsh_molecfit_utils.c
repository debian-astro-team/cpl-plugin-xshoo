#include <xsh_molecfit_utils.h>

cpl_error_code xsh_molecfit_model_check_extensions_and_ranges(cpl_size extension, double min_wav, double max_wav, cpl_table* range) {
  //cpl_msg_info(cpl_func,"fors_molecfit_model_check_extensions_and_ranges");
  //cpl_msg_info(cpl_func,"selected rows: %lld",cpl_table_count_selected(range));

  //cpl_msg_info (cpl_func,"CHECK FOR extension %lld, min %f max %f",  extension, min_wav,max_wav);
  //cpl_table_dump(range,0,cpl_table_get_nrow(range),NULL);
  
  /* Check all ranges that are mapped to this extension */
  
  cpl_size nranges=cpl_table_get_nrow(range);
  cpl_boolean check_ok= CPL_TRUE;
  for (cpl_size i=0;i<nranges;i++) {
      
      //cpl_size range_idx=i+1;
     
      int    range_assigned_extension =  cpl_table_get_int   (range,MF_COL_WAVE_RANGE_MAP2CHIP,i,NULL);
      double range_low_limit          =  cpl_table_get_double(range,MF_COL_WAVE_RANGE_LOWER,        i, NULL);
      double range_upp_limit          =  cpl_table_get_double(range,MF_COL_WAVE_RANGE_UPPER,        i,NULL);
      
      /* Only check ranges that have been mapped to this extension data */
      if (range_assigned_extension!=extension) continue;

      if(range_upp_limit<=min_wav || range_low_limit>=max_wav){
        cpl_table_unselect_row(range,i);
      }
  }

  cpl_msg_info(cpl_func,"selected rows: %lld",cpl_table_count_selected(range));
  if(cpl_table_count_selected(range) == 0){
      check_ok = CPL_FALSE;
  }
     
      /* If the check is not ok then set error message and return illegal input flag */
      if (!check_ok) {
         cpl_msg_error(cpl_func,"No valid ranges");
         //cpl_msg_error(cpl_func,"Range %lld [%f , %f] Assigned to Extension %d [%f , %f]. Check Overlap NOT OK",
         //                    range_idx, range_low_limit, range_upp_limit, range_assigned_extension,min_wav,max_wav);
         return CPL_ERROR_ILLEGAL_INPUT;
      }
      
      /* Output that this range has been checked and show details */ 
      //cpl_msg_info(cpl_func,"Range %lld [%f , %f] Assigned to Extension %d [%f , %f]. Check Overlap OK", 
      //                         range_idx, range_low_limit, range_upp_limit, range_assigned_extension,min_wav,max_wav);

  

  return CPL_ERROR_NONE;
}

/* Is it always only one input spectrum per frameset?*/
cpl_error_code xsh_molecfit_utils_find_input_frame(cpl_frameset *frameset,cpl_parameterlist* iframe){
    /* The cpl_parameterlist* of results - iframe - should already have a size of 3 allocated*/

    //cpl_frameset_dump(frameset,stdout);
    /* X-SHOOTER has 3 distinct arms - XXX */
    cpl_size n_arms = 3;
    cpl_array* arms = cpl_array_new(n_arms,CPL_TYPE_STRING);
    cpl_array_set_string(arms,0,"UVB");
    cpl_array_set_string(arms,1,"VIS");
    cpl_array_set_string(arms,2,"NIR");

    /* X-SHOOTER has 3 distinct observation modes - YYY */
    cpl_size n_omodes = 3;
    cpl_array* omodes = cpl_array_new(n_omodes,CPL_TYPE_STRING);
    cpl_array_set_string(omodes,0,"NOD");
    cpl_array_set_string(omodes,1,"STARE");
    cpl_array_set_string(omodes,2,"OFFSET");

    /* First consider SCI and TELL combinations */
    cpl_size n_bases= 7;
    cpl_array* bases = cpl_array_new(n_bases,CPL_TYPE_STRING);
    cpl_array_set_string(bases,0,"SCI_SLIT_FLUX_IDP");
    cpl_array_set_string(bases,1,"SCI_SLIT_FLUX_MERGE1D");
    cpl_array_set_string(bases,2,"SCI_SLIT_MERGE1D");
    cpl_array_set_string(bases,3,"SCI_SLIT_FLUX_MERGE2D");
    cpl_array_set_string(bases,4,"TELL_SLIT_MERGE1D");
    cpl_array_set_string(bases,5,"TELL_SLIT_FLUX_MERGE1D");
    cpl_array_set_string(bases,6,"TELL_SLIT_FLUX_IDP");
    //structure for setting results in iframe 
    //idx 0 == input tag name
    //idx 1 == ARM (UVB, VIS, NIR)
    //idx 2 == OBSMODE (NOD, STARE or OFFSET); If none of these, it is set to DEFAULT 
    //idx 3 == IDP (TRUE or FALSE) 
    for (int i=0;i<n_bases;i++){
        const char* base = cpl_array_get_string(bases,i);
        const char* idp = (strstr(base,"IDP")) ? "TRUE" : "FALSE";
        for (int j=0;j<n_arms;j++){
            const char* arm = cpl_array_get_string(arms,j);
            const char* tag = cpl_sprintf("%s_%s",base,arm);
            cpl_msg_info(cpl_func,"Looking for TAG %s",tag);
            cpl_frame* input_frame = NULL;
            input_frame = cpl_frameset_find(frameset, tag);
            if(input_frame){
                const char* fname =  cpl_frame_get_filename(input_frame);
                cpl_msg_info(cpl_func,"FOUND frame with TAG %s and filename %s",tag,fname);
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTNAME",CPL_TYPE_STRING,NULL,NULL,tag));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("ARM",CPL_TYPE_STRING,NULL,NULL,arm));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("OBSMODE",CPL_TYPE_STRING,NULL,NULL,"DEFAULT"));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("IDP",CPL_TYPE_STRING,NULL,NULL,idp));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTFILENAME",CPL_TYPE_STRING,NULL,NULL,fname));

                cpl_array_delete(arms);
                cpl_array_delete(omodes);
                cpl_array_delete(bases);
                return CPL_ERROR_NONE;
            }
        }
    }
    /* Handle STD_SLIT_FLUX_IDP_YYY_XXX combinations*/
    for (int i=0;i<n_omodes;i++){
        const char* omode = cpl_array_get_string(omodes,i);
        for (int j=0;j<n_arms;j++){
            const char* arm = cpl_array_get_string(arms,j);
            const char* tag = cpl_sprintf("STD_SLIT_FLUX_IDP_%s_%s",omode,arm);
            cpl_msg_info(cpl_func,"Looking for TAG %s",tag);
            cpl_frame* input_frame = NULL;
            input_frame = cpl_frameset_find(frameset, tag);
            if(input_frame){
                const char* fname =  cpl_frame_get_filename(input_frame);
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTNAME",CPL_TYPE_STRING,NULL,NULL,tag));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("ARM",CPL_TYPE_STRING,NULL,NULL,arm));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("OBSMODE",CPL_TYPE_STRING,NULL,NULL,omode));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("IDP",CPL_TYPE_STRING,NULL,NULL,"TRUE"));
                cpl_parameterlist_append(iframe,cpl_parameter_new_value("INPUTFILENAME",CPL_TYPE_STRING,NULL,NULL,fname));
                cpl_array_delete(arms);
                cpl_array_delete(omodes);
                cpl_array_delete(bases);
                return CPL_ERROR_NONE;
            }
        }
    }
    cpl_array_delete(arms);
    cpl_array_delete(omodes);
    cpl_array_delete(bases);
    return CPL_ERROR_NULL_INPUT;
}


