/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*---------------------------------------------------------------------------*/
/**
 * @defgroup xsh_ifu
 * @ingroup xsh_ifu
 */
/*---------------------------------------------------------------------------*/

/**@{*/


/*-----------------------------------------------------------------------------
                                 Includes
 ----------------------------------------------------------------------------*/

#include <xsh_data_shift_tab.h>
#include <xsh_utils.h>
#include <xsh_utils_table.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_pfits.h>
#include <cpl.h>
#include <xsh_drl.h>
#include <math.h>

/*----------------------------------------------------------------------------
                                 Function implementation
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/** 
  @brief
    Create an IFU wave tab frame set
  @param wavetab_frame
    The wave table frame
  @param shifttab_frame
    The shift table frame
  @return
    An IFU wave tab frame set
*/
/*---------------------------------------------------------------------------*/
cpl_frameset* xsh_ifu_wavetab_create( cpl_frame *wavetab_frame, 
  cpl_frame *shifttab_frame, xsh_instrument *instr)
{
  cpl_frameset *result = NULL;
  xsh_shift_tab *shift_tab = NULL;
  xsh_wavesol *wavesol_cen = NULL;
  xsh_wavesol *wavesol_up = NULL;
  xsh_wavesol *wavesol_down = NULL;
  char result_name[256];
  const char *tag = NULL;
  cpl_table *trace = NULL;
  cpl_frame *down_frame = NULL;
  cpl_frame *cen_frame = NULL;
  cpl_frame *up_frame = NULL;

  XSH_ASSURE_NOT_NULL( wavetab_frame);
  XSH_ASSURE_NOT_NULL( instr);

  check( wavesol_down = xsh_wavesol_load( wavetab_frame, instr));
  check( wavesol_cen = xsh_wavesol_load( wavetab_frame, instr));
  check( wavesol_up = xsh_wavesol_load( wavetab_frame, instr)); 

  if ( shifttab_frame != NULL){
    check( shift_tab = xsh_shift_tab_load( shifttab_frame, instr));
    check( xsh_wavesol_apply_shift( wavesol_down, 0.0, 
      shift_tab->shift_y_down)); 
    check( xsh_wavesol_apply_shift( wavesol_cen, 0.0, 
      shift_tab->shift_y_cen));
    check( xsh_wavesol_apply_shift( wavesol_up, 0.0, 
      shift_tab->shift_y_up));
  }
  check( trace = cpl_table_new(1));

  check( result = cpl_frameset_new());

  tag = XSH_GET_TAG_FROM_ARM( XSH_WAVE_TAB_ARC_DOWN_IFU, instr);
  sprintf( result_name, "%s.fits", tag);
  check( down_frame = xsh_wavesol_save( wavesol_down, 
    trace, result_name, tag));
  check( cpl_frameset_insert( result, down_frame));

  tag = XSH_GET_TAG_FROM_ARM( XSH_WAVE_TAB_ARC_CEN_IFU, instr);
  sprintf( result_name, "%s.fits", tag);
  check( cen_frame = xsh_wavesol_save( wavesol_cen,
    trace, result_name, tag));
  check( cpl_frameset_insert( result, cen_frame));

  tag = XSH_GET_TAG_FROM_ARM( XSH_WAVE_TAB_ARC_UP_IFU, instr);
  sprintf( result_name, "%s.fits", tag);
  check( up_frame = xsh_wavesol_save( wavesol_up,
    trace, result_name, tag));
  check( cpl_frameset_insert( result, up_frame));

  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE){
      xsh_free_frameset( &result);
    }
    xsh_shift_tab_free( &shift_tab);
    xsh_wavesol_free( &wavesol_down);
    xsh_wavesol_free( &wavesol_cen);
    xsh_wavesol_free( &wavesol_up);
    xsh_free_table( &trace);
    return result;
}
/*---------------------------------------------------------------------------*/

/**@}*/
