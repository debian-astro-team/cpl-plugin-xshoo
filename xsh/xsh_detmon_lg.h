/* $Id: xsh_detmon_lg.h,v 1.4 2013-06-18 13:34:19 jtaylor Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002, 2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02111-1307 USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-06-18 13:34:19 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_IRPLIB_DETMON_LG_H
#define XSH_IRPLIB_DETMON_LG_H

/*----------------------------------------------------------------------------
                                   Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_cpl_size.h>

/*----------------------------------------------------------------------------
                                   Prototypes
 ----------------------------------------------------------------------------*/
/* Define here the DO.CATG keywords */
#define DETMON_LG_ON_RAW_OLD                    "ON_RAW"
#define DETMON_LG_OFF_RAW_OLD			"OFF_RAW"
#define DETMON_LG_ON_RAW_NEW                    "DETMON_LAMP_ON"
#define DETMON_LG_OFF_RAW_NEW			"DETMON_LAMP_OFF"


#define NIR TRUE
#define OPT FALSE

#define DETMON_QC_COUNTS_MIN  "ESO QC COUNTS MIN"
#define DETMON_QC_COUNTS_MIN_C     "Minimum median value used in linearity test (in a user defined region) [ADU]"

#define DETMON_QC_COUNTS_MAX  "ESO QC COUNTS MAX"
#define DETMON_QC_COUNTS_MAX_C     "Maximum median value used in linearity test (in a user defined region) [ADU]"

#define DETMON_QC_CONAD       "ESO QC CONAD"
#define DETMON_QC_CONAD_C     "Conversion from e- to ADUs [ADU/e-]"

#define DETMON_QC_CONAD_CORR       "ESO QC CONAD CORR"
#define DETMON_QC_CONAD_CORR_C     "CONAD value taken QC.AUTOCORR into account [ADU/e-]"

#define DETMON_QC_GAIN        "ESO QC GAIN"
#define DETMON_QC_GAIN_C      "GAIN (see QC.METHOD) [e-/ADU]"

#define DETMON_QC_GAIN_MSE    "ESO QC GAIN MSE"
#define DETMON_QC_GAIN_MSE_C  "Measured Squared error in GAIN computation"

#define DETMON_QC_RON         "ESO QC RON"
#define DETMON_QC_RON_C       "RON obtained as independent term (PTC method)"

#define DETMON_QC_AUTOCORR    "ESO QC AUTOCORR"
#define DETMON_QC_AUTOCORR_C  "Autocorrelation factor computed as sum of " \
                              "all pixels in autocorrelation image"
#define DETMON_QC_GAIN_CORR   "ESO QC GAIN CORR"
#define DETMON_QC_GAIN_CORR_C "GAIN taken QC.AUTOCORR into account [e-/ADU]"

#define DETMON_QC_LAMP_FLUX   "ESO QC LAMP FLUX"
#define DETMON_QC_LAMP_FLUX_C "Lamp flux"

#define DETMON_QC_NUM_BPM     "ESO QC NUM BPM"
#define DETMON_QC_NUM_BPM_C   "Number of bad pixels detected according to "\
                              "polynomial information"

#define DETMON_QC_LAMP_STAB   "ESO QC LAMP STAB"
#define DETMON_QC_LAMP_STAB_C "Lamp stability"

#define DETMON_QC_METHOD      "ESO QC METHOD"
#define DETMON_QC_METHOD_C    "Method applied to compute GAIN"

#define DETMON_QC_LIN_EFF     "ESO QC LIN EFF"
#define DETMON_QC_LIN_EFF_C   "Effective non-linearity correction"

#define DETMON_QC_LIN_EFF_FLUX     "ESO QC LIN EFF FLUX"
#define DETMON_QC_LIN_EFF_FLUX_C   "FLux level at which effective non-linearity correction is computed"

#define DETMON_QC_LIN_COEF    "ESO QC LIN COEF"
#define DETMON_QC_LIN_COEF_C  "Linearity coefficient value"


#define DETMON_QC_LIN_COEF_ERR     "ESO QC LIN EFF ERR"
#define DETMON_QC_LIN_COEF_ERR_C   "Linearity coefficient error value"
#define DETMON_QC_LIN_COEF_MSE_ERR_C   "Linearity coefficient measured square error value"


#define DETMON_QC_ERRFIT     "ESO QC ERRFIT"
#define DETMON_QC_ERRFIT_C   "Error of fit"
#define DETMON_QC_ERRFIT_MSE_C   "Measured square error of fit"


#define DETMON_QC_CONTAM     "ESO QC CONTAM"
#define DETMON_QC_CONTAM_C   "Detector contamination in a region"

#define DETMON_QC_FPN     "ESO QC FPN"
#define DETMON_QC_FPN_C   "Flat pattern noise"

/* Macros to generate the recipe(s) description(s) */
#define xsh_detmon_lg_get_description(RECIPE_NAME, PIPE_NAME,              \
				  DETMON_LG_ON_RAW_NEW,                 \
				  DETMON_LG_OFF_RAW_NEW,                \
				  DETMON_LG_ON_RAW_OLD,                \
                                  DETMON_LG_OFF_RAW_OLD)                \
    RECIPE_NAME " -- " PIPE_NAME " linearity/gain recipe for OPT/IR.\n"           \
    "The files listed in the Set Of Frames must be tagged:\n"                 \
    "the raw-on-file.fits  "DETMON_LG_ON_RAW_NEW"  and\n"                      \
    "the raw-off-file.fits "DETMON_LG_OFF_RAW_NEW"\n"                          \
    "For backward compatibility are alternatively still supported the following tags:\n" \
    "the raw-on-file.fits  "DETMON_LG_ON_RAW_OLD"  and\n"                      \
    "the raw-off-file.fits "DETMON_LG_OFF_RAW_OLD"\n"                          \
    "The recipe requires at least order+1 valid pairs of ON frames\n"         \
    "and order+1 valid pairs of OFF frames, \n"                               \
    "where order is the value of the 'order' parameter.\n"                    \
    "There is no requirement for pairs of OFF frames if collapse parameter "  \
    "is set to true.\n"                                                       \
    "Frames with median flux over --filter will be excluded from the "        \
    "computation.\n"                                                          \
    "To perform a stability check you need frames with at least 2 DIT "       \
    "values\n"                                                                \
    "NOTE for multiextension cases: it is mandatory to modify the --exts "    \
    "parameter, either to -1 (all extensions) or to any valid extension nb.\n" \
    "\n" \
    "The output PRO.CATG are:\n" \
    "GAIN_INFO    - the gain table\n" \
    "DET_LIN_INFO - the linearity table\n" \
    "BP_MAP_NL    - the bad pixel map (only produced with --pix2pix=TRUE)\n" \
    "COEFFS_CUBE  - the fit coefficient cube (only produced with --pix2pix=TRUE)\n"

#define REGEXP "ARCFILE|MJD-OBS|ESO TPL ID|DATE-OBS|ESO DET DIT|ESO DET NDIT"

typedef unsigned long irplib_gain_flag;

#define IRPLIB_GAIN_PTC           ((irplib_gain_flag) 1 << 1)
#define IRPLIB_GAIN_MEDIAN        ((irplib_gain_flag) 1 << 2)
#define IRPLIB_GAIN_NO_COLLAPSE   ((irplib_gain_flag) 1 << 3)
#define IRPLIB_GAIN_COLLAPSE      ((irplib_gain_flag) 1 << 4)
#define IRPLIB_GAIN_WITH_AUTOCORR ((irplib_gain_flag) 1 << 5)
#define IRPLIB_GAIN_WITH_RESCALE  ((irplib_gain_flag) 1 << 6)
#define IRPLIB_GAIN_OPT           ((irplib_gain_flag) 1 << 7)
#define IRPLIB_GAIN_NIR           ((irplib_gain_flag) 1 << 8)

typedef unsigned long irplib_lin_flag;

#define IRPLIB_LIN_NO_COLLAPSE   ((irplib_lin_flag) 1 << 10)
#define IRPLIB_LIN_COLLAPSE      ((irplib_lin_flag) 1 << 11)
#define IRPLIB_LIN_PIX2PIX       ((irplib_lin_flag) 1 << 12)
#define IRPLIB_LIN_WITH_RESCALE  ((irplib_lin_flag) 1 << 13)
#define IRPLIB_LIN_OPT           ((irplib_lin_flag) 1 << 14)
#define IRPLIB_LIN_NIR           ((irplib_lin_flag) 1 << 15)

/*----------------------------------------------------------------------------
                                   Prototypes
 ----------------------------------------------------------------------------*/

cpl_error_code
xsh_detmon_lg(cpl_frameset            * frameset,
                 const cpl_parameterlist * parlist,
                 const char              * tag_on,
                 const char              * tag_off,
                 const char              * recipe_name,
                 const char              * pipeline_name,
                 const char              * pafregexp,
                 const cpl_propertylist  * pro_lintbl,
                 const cpl_propertylist  * pro_gaintbl,
                 const cpl_propertylist  * pro_coeffscube,
                 const cpl_propertylist  * pro_bpm,
                 const cpl_propertylist  * pro_corr,
                 const cpl_propertylist  * pro_diff,
                 const char              * package,
                 int                    (* compare) (const cpl_frame *,
						     const cpl_frame *),
		 int                    (* load_fset) (const cpl_frameset *,
						       cpl_type,
		                                       cpl_imagelist *),
                 const cpl_boolean         opt_nir);


cpl_image * xsh_detmon_image_correlate(const cpl_image       *,
                                          const cpl_image       *,
                                          const int              ,
                                          const int              );

cpl_error_code
xsh_detmon_lg_fill_parlist_nir_default(cpl_parameterlist *,
                              const char        *,
                              const char        *);

cpl_error_code
xsh_detmon_lg_fill_parlist_nir_default_mr(cpl_parameterlist * parlist,
                                      const char *recipe_name,
                                      const char *pipeline_name);

cpl_error_code
xsh_detmon_lg_fill_parlist_opt_default(cpl_parameterlist *,
                              const char        *,
                              const char        *);

cpl_error_code
xsh_detmon_lg_fill_parlist_opt_default_mr(cpl_parameterlist * parlist,
                                      const char *recipe_name,
                                      const char *pipeline_name);

cpl_error_code
xsh_detmon_lg_fill_parlist(cpl_parameterlist * parlist,
                          const char *recipe_name, const char *pipeline_name,
			  const char *method,
                          int order,
                          double kappa,
                          int niter,
                          int llx,
                          int lly,
                          int urx,
                          int ury,
                          int ref_level,
                          const char * intermediate,
                          const char * autocorr,
                          const char * collapse,
                          const char * rescale,
			      const char * pix2pix,
			      const char * bpmbin,
                          int filter,
                          int m,
                          int n,
			      double tolerance,
			      const char * pafgen,
			      const char * pafname,
                          int llx1,
                          int lly1,
                          int urx1,
                          int ury1,
                          int llx2,
                          int lly2,
                          int urx2,
                          int ury2,
                          int llx3,
                          int lly3,
                          int urx3,
                          int ury3,
                          int llx4,
                          int lly4,
                          int urx4,
                          int ury4,
                          int llx5,
                          int lly5,
                          int urx5,
                          int ury5,
			               int exts,
                          cpl_boolean opt_nir);

cpl_image *
xsh_detmon_autocorrelate(const cpl_image *,
                            const int, const int);

cpl_table *
xsh_detmon_gain(const cpl_imagelist  *,
		   const cpl_imagelist  *,
		   const cpl_vector     *,
		   const cpl_vector     *,
		   double                ,
		   int                   ,
		   int                   ,
		   int                   ,
		   int                   ,
                   double                ,
                   int                   ,
                   int                   ,
                   int                   ,
		   cpl_propertylist     *,
		   unsigned              ,
		   cpl_imagelist       **,
		   cpl_imagelist       **);

cpl_table *
xsh_detmon_lin(const cpl_imagelist  *,
		  const cpl_imagelist  *,
		  const cpl_vector     *,
		  double                ,
		  int                   ,
		  int                   ,
		  int                   ,
		  int                   ,
		  int                   ,
		  int                   ,
                  double,
                  cpl_boolean,
		  cpl_propertylist     *,
		  unsigned              ,
		  cpl_imagelist       **,
		  cpl_image           **);

cpl_error_code
xsh_detmon_lg_set_tag(cpl_frameset* set, const char** tag_on, const char** tag_off);
#endif
