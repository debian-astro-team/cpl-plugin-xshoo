/* $Id: xsh_model_io.c,v 1.41 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the CRIRES Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.41 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_msg.h>
#include <xsh_utils_table.h>
#include "xsh_model_io.h"
#include "xsh_dfs.h"
#include "xsh_error.h"
#include "xsh_data_spectralformat.h"
#include "xsh_model_arm_constants.h"

/*-----------------------------------------------------------------------------
   								Functions prototypes
 -----------------------------------------------------------------------------*/

static int xsh_table_check_column(const cpl_table *, const char *) ;

/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_model_io    IO functions for the model
 */
/*----------------------------------------------------------------------------*/

/**@{*/

void
xsh_model_io_dump(xsh_xs_3 * p_xs_3){
   int i=0;

   xsh_msg("temper=%g",p_xs_3->temper); 
   xsh_msg("arm=%d",  p_xs_3->arm); 
   xsh_msg("t_ir_p2=%g", p_xs_3->t_ir_p2); 
   xsh_msg("t_ir_p3=%g", p_xs_3->t_ir_p3); 
   xsh_msg("es_x=%g", p_xs_3->es_x); 
   xsh_msg("es_y=%g", p_xs_3->es_y); 
   xsh_msg("mues=%g", p_xs_3->mues); 
   xsh_msg("nues=%g", p_xs_3->nues); 
   xsh_msg("taues=%g",p_xs_3->taues); 
   xsh_msg("slit_scale=%g",p_xs_3->slit_scale); 
   xsh_msg("es_s=%g",p_xs_3->es_s); 
   xsh_msg("es_w=%g",p_xs_3->es_w); 
   xsh_msg("fcol=%g",p_xs_3->fcol); 
   xsh_msg("cmup1=%g",p_xs_3->cmup1); 
   xsh_msg("mup1=%g", p_xs_3->mup1); 
   xsh_msg("nup1=%g", p_xs_3->nup1); 
   xsh_msg("taup1=%g",p_xs_3->taup1); 
   xsh_msg("mup2=%g", p_xs_3->mup2);
   xsh_msg("nup2=%g", p_xs_3->nup2); 
   xsh_msg("taup2=%g", p_xs_3->taup2); 
   xsh_msg("mup3=%g", p_xs_3->mup3);
   xsh_msg("nup3=%g", p_xs_3->nup3);
   xsh_msg("taup3=%g",p_xs_3->taup3); 
   xsh_msg("mup4=%g", p_xs_3->mup4); 
   xsh_msg("nup4=%g", p_xs_3->nup4); 
   xsh_msg("taup4=%g", p_xs_3->taup4); 
   xsh_msg("mup5=%g",p_xs_3->mup5); 
   xsh_msg("nup5=%g", p_xs_3->nup5); 
   xsh_msg("taup5=%g", p_xs_3->taup5); 
   xsh_msg("mup6=%g", p_xs_3->mup6); 
   xsh_msg("nup6=%g", p_xs_3->nup6); 
   xsh_msg("taup6=%g",p_xs_3->taup6); 
   xsh_msg("mug=%g",p_xs_3->mug); 
   xsh_msg("nug=%g",p_xs_3->nug); 
   xsh_msg("taug=%g", p_xs_3->taug); 
   xsh_msg("sg=%g", p_xs_3->sg); 
   xsh_msg("fdet=%g", p_xs_3->fdet); 
   xsh_msg("mud=%g", p_xs_3->mud); 
   xsh_msg("nud=%g", p_xs_3->nud); 
   xsh_msg("taud=%g",p_xs_3->taud); 
   xsh_msg("pix=%g", p_xs_3->pix); 
   xsh_msg("chipx=%g",p_xs_3->chipx);
   xsh_msg("chipy=%g",p_xs_3->chipy);
   xsh_msg("chiprot=%g", p_xs_3->chiprot); 
   xsh_msg("pc_x_xx=%g", p_xs_3->pc_x_xx);
   xsh_msg("pc_x_x1=%g", p_xs_3->pc_x_x1);
   xsh_msg("pc_x_yy=%g", p_xs_3->pc_x_yy);
   xsh_msg("pc_x_y1=%g", p_xs_3->pc_x_y1);
   xsh_msg("pc_x_xy=%g", p_xs_3->pc_x_xy);
   xsh_msg("pc_x_x3=%g", p_xs_3->pc_x_x3);
   xsh_msg("pc_x_x2y=%g", p_xs_3->pc_x_x2y);
   xsh_msg("pc_x_y2x=%g", p_xs_3->pc_x_y2x);
   xsh_msg("pc_x_y3=%g", p_xs_3->pc_x_y3);
   xsh_msg("pc_y_xx=%g", p_xs_3->pc_y_xx);
   xsh_msg("pc_y_x1=%g", p_xs_3->pc_y_x1);
   xsh_msg("pc_y_yy=%g", p_xs_3->pc_y_yy);
   xsh_msg("pc_y_y1=%g", p_xs_3->pc_y_y1);
   xsh_msg("pc_y_xy=%g", p_xs_3->pc_y_xy);
   xsh_msg("pc_y_x3=%g", p_xs_3->pc_y_x3);
   xsh_msg("pc_y_x2y=%g", p_xs_3->pc_y_x2y);
   xsh_msg("pc_y_y2x=%g", p_xs_3->pc_y_y2x);
   xsh_msg("pc_y_y3=%g", p_xs_3->pc_y_y3);
   xsh_msg("pc4_x_xy3=%g", p_xs_3->pc4_x_xy3);
   xsh_msg("pc4_x_x3y=%g",p_xs_3->pc4_x_x3y);
   xsh_msg("pc4_x_x2y2=%g",p_xs_3->pc4_x_x2y2);
   xsh_msg("pc4_x_x4=%g", p_xs_3->pc4_x_x4);
   xsh_msg("pc4_x_y4=%g", p_xs_3->pc4_x_y4);
   xsh_msg("pc4_y_xy3=%g",p_xs_3->pc4_y_xy3);
   xsh_msg("pc4_y_x3y=%g", p_xs_3->pc4_y_x3y);
   xsh_msg("pc4_y_x2y2=%g", p_xs_3->pc4_y_x2y2);
   xsh_msg("pc4_y_x4=%g", p_xs_3->pc4_y_x4);
   xsh_msg("pc4_y_y4=%g", p_xs_3->pc4_y_y4);
   xsh_msg("ca_x0=%g", p_xs_3->ca_x0);
   xsh_msg("ca_x=%g1", p_xs_3->ca_x1);
   xsh_msg("ca_y0=%g", p_xs_3->ca_y0);
   xsh_msg("ca_y1=%g", p_xs_3->ca_y1);
   xsh_msg("d2_x1=%g", p_xs_3->d2_x1);
   xsh_msg("d2_x=%g2", p_xs_3->d2_x2);
   xsh_msg("d2_x3=%g", p_xs_3->d2_x3);
   xsh_msg("d2_y1x0=%g", p_xs_3->d2_y1x0);
   xsh_msg("d2_y1x1=%g", p_xs_3->d2_y1x1);
   xsh_msg("d2_y1x2=%g", p_xs_3->d2_y1x2);
   xsh_msg("d2_y1x3=%g", p_xs_3->d2_y1x3);
   xsh_msg("d2_y2x0=%g", p_xs_3->d2_y2x0);
   xsh_msg("d2_y2x1=%g", p_xs_3->d2_y2x1);
   xsh_msg("d2_y2x2=%g", p_xs_3->d2_y2x2);
   xsh_msg("d2_y2x3=%g", p_xs_3->d2_y2x3);
   xsh_msg("d2_y3x0=%g", p_xs_3->d2_y3x0);
   xsh_msg("d2_y3x1=%g", p_xs_3->d2_y3x1);
   xsh_msg("d2_y3x2=%g", p_xs_3->d2_y3x2);
   xsh_msg("d2_y3x3=%g", p_xs_3->d2_y3x3);
   xsh_msg("offx=%g", p_xs_3->offx);
   xsh_msg("offy=%g", p_xs_3->offy); 
   xsh_msg("flipx=%g", p_xs_3->flipx); 
   xsh_msg("flipy=%g", p_xs_3->flipy);
   for(i=0;i<9;i++){ 
      xsh_msg("slit=%g", p_xs_3->slit[i]);
   }

   return;

}
/*----------------------------------------------------------------------------*/
/**
  @brief    Load the config model table and fill the struct 
  @param    config_frame    Model Config frame
  @param    p_xs_3    xs_3 structure
  @return   cpl_error_code
 */
/*----------------------------------------------------------------------------*/
cpl_error_code
xsh_model_config_load_best(
        cpl_frame      *   config_frame,
        xsh_xs_3   *   p_xs_3)
{
  cpl_table   *   tab = NULL;
  int             nlines ;
  char        *   name ;
  int             i;
  const char * config=NULL;
  const char * tag=NULL;
  XSH_INSTRCONFIG* instr_config=NULL;

  xsh_instrument* instr = NULL;

  instr = xsh_instrument_new();

  XSH_ASSURE_NOT_NULL( config_frame);
  XSH_ASSURE_NOT_NULL( p_xs_3);
  check( config=cpl_frame_get_filename(config_frame));
  check( tag=cpl_frame_get_tag(config_frame));
  xsh_msg_dbg_high("file %s tag=%s",config,tag);
  /* Load the table */
  check( tab = xsh_load_table_check(config,tag));
  XSH_ASSURE_NOT_NULL( tab);
  check( nlines = cpl_table_get_nrow(tab));

  p_xs_3->config_mjd=0.0;

  /*default values for the two non-zero dist coeff.s
    (missing from older config files, if present in new files they will be overwritten) */
  p_xs_3->pc_x_x1=1.0;
  p_xs_3->pc_y_y1=1.0;
  p_xs_3->pc_x_xx=0.0;
  p_xs_3->pc_x_xy=0.0;
  p_xs_3->pc_x_x3=0.0;
  p_xs_3->pc_x_y1=0.0;
  p_xs_3->pc_x_yy=0.0;
  p_xs_3->pc_x_y3=0.0;
  p_xs_3->pc_x_x2y=0.0;
  p_xs_3->pc_x_y2x=0.0;
  p_xs_3->pc_y_xx=0.0;
  p_xs_3->pc_y_xy=0.0;
  p_xs_3->pc_y_x3=0.0;
  p_xs_3->pc_y_x1=0.0;
  p_xs_3->pc_y_yy=0.0;
  p_xs_3->pc_y_y3=0.0;
  p_xs_3->pc_y_x2y=0.0;
  p_xs_3->pc_y_y2x=0.0;

  p_xs_3->d2_x1=0.0;
  p_xs_3->d2_x2=0.0;
  p_xs_3->d2_x3=0.0;
  p_xs_3->d2_y1x0=0.0;
  p_xs_3->d2_y1x1=0.0;
  p_xs_3->d2_y1x2=0.0;
  p_xs_3->d2_y1x3=0.0;
  p_xs_3->d2_y2x0=0.0;
  p_xs_3->d2_y2x1=0.0;
  p_xs_3->d2_y2x2=0.0;
  p_xs_3->d2_y2x3=0.0;
  p_xs_3->d2_y3x0=0.0;
  p_xs_3->d2_y3x1=0.0;
  p_xs_3->d2_y3x2=0.0;
  p_xs_3->d2_y3x3=0.0;
  p_xs_3->pc4_x_x3y=0.0;
  p_xs_3->pc4_x_xy3=0.0;
  p_xs_3->pc4_x_x2y2=0.0;
  p_xs_3->pc4_x_x4=0.0;
  p_xs_3->pc4_x_y4=0.0;
  p_xs_3->pc4_y_xy3=0.0;
  p_xs_3->pc4_y_x3y=0.0;
  p_xs_3->pc4_y_x2y2=0.0;
  p_xs_3->pc4_y_x4=0.0;
  p_xs_3->pc4_y_y4=0.0;
  p_xs_3->ca_x0=0.0;
  p_xs_3->ca_x1=0.0;
  p_xs_3->ca_y0=0.0;
  p_xs_3->ca_y1=0.0;
  p_xs_3->offx=0.0;
  p_xs_3->offy=0.0;

  if (strncmp(tag,XSH_MOD_CFG_NIR,16)==0 ||
      strncmp(tag,XSH_MOD_CFG_TAB_NIR,20)==0) {
    p_xs_3->pc4_x_x3y=0.0;
    p_xs_3->pc4_x_xy3=0.0;
    p_xs_3->pc4_x_x2y2=0.0;
    p_xs_3->pc4_x_x4=0.0;
    p_xs_3->pc4_x_y4=0.0;
    p_xs_3->pc4_y_xy3=0.0;
    p_xs_3->pc4_y_x3y=0.0;
    p_xs_3->pc4_y_x2y2=0.0;
    p_xs_3->pc4_y_x4=0.0;
    p_xs_3->pc4_y_y4=0.0;
    p_xs_3->ca_x0=0.0;
    p_xs_3->ca_x1=0.0;
    p_xs_3->ca_y0=0.0;
    p_xs_3->ca_y1=0.0;
    p_xs_3->offx=0.0;
    p_xs_3->offy=0.0;
  }
  else if (strncmp(tag,XSH_MOD_CFG_VIS,16)==0 ||
	   strncmp(tag,XSH_MOD_CFG_TAB_VIS,20)==0) {
    p_xs_3->d2_x1=0.00287311525385538;
    p_xs_3->d2_x2=1.55125984130817E-05;
    p_xs_3->d2_x3=-4.00980399239143E-05;
  }
  else if (strncmp(tag,XSH_MOD_CFG_UVB,16)==0 ||
	   strncmp(tag,XSH_MOD_CFG_TAB_UVB,20)==0) {
    p_xs_3->d2_x1=0.0;
    p_xs_3->d2_x2=0.0;
    p_xs_3->d2_x3=0.0;
  }


    /* Loop on lines */
  for (i=0 ; i<nlines ; i++) {
    name = (char*) cpl_table_get_string(tab, XSH_COL_MODEL_CONF_NAME, i);
    //    printf("param %s : %g\n", name, cpl_table_get_double(tab,
    //            XSH_COL_MODEL_CONF_BEST, i, NULL)) ;
    if (!strcmp("temper", name)) p_xs_3->temper = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("arm", name)) p_xs_3->arm = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("config_mjd", name)) p_xs_3->config_mjd = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("t_ir_p2", name)) p_xs_3->t_ir_p2 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("t_ir_p3", name)) p_xs_3->t_ir_p3 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("es_x", name)) p_xs_3->es_x = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("es_y", name)) p_xs_3->es_y = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mues", name)) p_xs_3->mues = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nues", name)) p_xs_3->nues = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taues", name)) p_xs_3->taues = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("slit_scale", name)) p_xs_3->slit_scale = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("es_s", name)) p_xs_3->es_s = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("es_w", name)) p_xs_3->es_w = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("fcol", name)) p_xs_3->fcol = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("cmup1", name)) p_xs_3->cmup1 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup1", name)) p_xs_3->mup1 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup1", name)) p_xs_3->nup1 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup1", name)) p_xs_3->taup1 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup2", name)) p_xs_3->mup2 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup2", name)) p_xs_3->nup2 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup2", name)) p_xs_3->taup2 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup3", name)) p_xs_3->mup3 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup3", name)) p_xs_3->nup3 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup3", name)) p_xs_3->taup3 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup4", name)) p_xs_3->mup4 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup4", name)) p_xs_3->nup4 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup4", name)) p_xs_3->taup4 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup5", name)) p_xs_3->mup5 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup5", name)) p_xs_3->nup5 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup5", name)) p_xs_3->taup5 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mup6", name)) p_xs_3->mup6 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nup6", name)) p_xs_3->nup6 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taup6", name)) p_xs_3->taup6 = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mug", name)) p_xs_3->mug = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nug", name)) p_xs_3->nug = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taug", name)) p_xs_3->taug = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("sg", name)) p_xs_3->sg = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("fdet", name)) p_xs_3->fdet = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("mud", name)) p_xs_3->mud = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("nud", name)) p_xs_3->nud = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("taud", name)) p_xs_3->taud = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pix", name)) p_xs_3->pix = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("chipx", name)) p_xs_3->chipx=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("chipy", name)) p_xs_3->chipy=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("chiprot", name)) p_xs_3->chiprot = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_xx", name)) p_xs_3->pc_x_xx=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_x1", name)) p_xs_3->pc_x_x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_yy", name)) p_xs_3->pc_x_yy=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_y1", name)) p_xs_3->pc_x_y1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_xy", name)) p_xs_3->pc_x_xy=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_x3", name)) p_xs_3->pc_x_x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_x2y", name)) p_xs_3->pc_x_x2y=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_y2x", name)) p_xs_3->pc_x_y2x=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_x_y3", name)) p_xs_3->pc_x_y3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_xx", name)) p_xs_3->pc_y_xx=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_x1", name)) p_xs_3->pc_y_x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_yy", name)) p_xs_3->pc_y_yy=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_y1", name)) p_xs_3->pc_y_y1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_xy", name)) p_xs_3->pc_y_xy=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_x3", name)) p_xs_3->pc_y_x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_x2y", name)) p_xs_3->pc_y_x2y=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_y2x", name)) p_xs_3->pc_y_y2x=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc_y_y3", name)) p_xs_3->pc_y_y3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_x_xy3", name)) p_xs_3->pc4_x_xy3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_x_x3y", name)) p_xs_3->pc4_x_x3y=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_x_x2y2", name)) p_xs_3->pc4_x_x2y2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_x_x4", name)) p_xs_3->pc4_x_x4=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_x_y4", name)) p_xs_3->pc4_x_y4=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_y_xy3", name)) p_xs_3->pc4_y_xy3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_y_x3y", name)) p_xs_3->pc4_y_x3y=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_y_x2y2", name)) p_xs_3->pc4_y_x2y2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_y_x4", name)) p_xs_3->pc4_y_x4=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("pc4_y_y4", name)) p_xs_3->pc4_y_y4=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("ca_x0", name)) p_xs_3->ca_x0=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("ca_x1", name)) p_xs_3->ca_x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("ca_y0", name)) p_xs_3->ca_y0=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("ca_y1", name)) p_xs_3->ca_y1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_x1", name)) p_xs_3->d2_x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_x2", name)) p_xs_3->d2_x2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_x3", name)) p_xs_3->d2_x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y1x0", name)) p_xs_3->d2_y1x0=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y1x1", name)) p_xs_3->d2_y1x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y1x2", name)) p_xs_3->d2_y1x2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y1x3", name)) p_xs_3->d2_y1x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y2x0", name)) p_xs_3->d2_y2x0=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y2x1", name)) p_xs_3->d2_y2x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y2x2", name)) p_xs_3->d2_y2x2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y2x3", name)) p_xs_3->d2_y2x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y3x0", name)) p_xs_3->d2_y3x0=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y3x1", name)) p_xs_3->d2_y3x1=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y3x2", name)) p_xs_3->d2_y3x2=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("d2_y3x3", name)) p_xs_3->d2_y3x3=
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("offx", name)) p_xs_3->offx = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("offy", name)) p_xs_3->offy = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("flipx", name)) p_xs_3->flipx = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strcmp("flipy", name)) p_xs_3->flipy = 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
    else if (!strncmp("slit", name,4)){
      int islit;
      for(islit=0; islit <9; islit++){
        char temp[10];
        
        sprintf(temp,"slit[%d]",islit);
        if (!strcmp(name, temp)){
          p_xs_3->slit[islit]= 
            cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST, i, NULL) ;
        }
      }
    }
  } 
  p_xs_3->pix_X=p_xs_3->pix_Y=p_xs_3->pix;
  if (p_xs_3->arm==0) {
    p_xs_3->morder=UVB_morder;
    p_xs_3->blaze_pad=UVB_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_UVB);
    p_xs_3->xsize_corr=UVB_xsize_corr;
    p_xs_3->ysize_corr=UVB_ysize_corr;
  }
  else if (p_xs_3->arm==1) {
    p_xs_3->morder=VIS_morder;
    p_xs_3->blaze_pad=VIS_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_VIS);
    p_xs_3->xsize_corr=VIS_xsize_corr;
    p_xs_3->ysize_corr=VIS_ysize_corr;
  }
  else if (p_xs_3->arm==2) {
    p_xs_3->morder=NIR_morder;
    p_xs_3->blaze_pad=NIR_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_NIR);
    p_xs_3->xsize_corr=NIR_xsize_corr;
    p_xs_3->ysize_corr=NIR_ysize_corr;
  }
  else {
    return CPL_ERROR_UNSUPPORTED_MODE;
  }
  instr_config=xsh_instrument_get_config(instr);
  p_xs_3->BSIZE=instr_config->ny;
  p_xs_3->ASIZE=instr_config->nx;
  p_xs_3->SIZE=p_xs_3->BSIZE;
  p_xs_3->chipypix=(float)(p_xs_3->BSIZE);
  p_xs_3->chipxpix=(float)(p_xs_3->ASIZE);
  p_xs_3->morder_min=instr_config->order_min;
  p_xs_3->morder_max=instr_config->order_max;

    /* Conversion from DEG to RAD */
  p_xs_3->mues *= DEG2RAD ;
  p_xs_3->nues *= DEG2RAD ;
  p_xs_3->taues *= DEG2RAD ;
  p_xs_3->cmup1 *= DEG2RAD ;
  p_xs_3->mup1 *= DEG2RAD ;
  p_xs_3->nup1 *= DEG2RAD ;
  p_xs_3->taup1 *= DEG2RAD ;
  p_xs_3->mup2 *= DEG2RAD ;
  p_xs_3->nup2 *= DEG2RAD ;
  p_xs_3->taup2 *= DEG2RAD ;
  p_xs_3->mup3 *= DEG2RAD ;
  p_xs_3->nup3 *= DEG2RAD ;
  p_xs_3->taup3 *= DEG2RAD ;
  p_xs_3->mup4 *= DEG2RAD ;
  p_xs_3->nup4 *= DEG2RAD ;
  p_xs_3->taup4 *= DEG2RAD ;
  p_xs_3->mup5 *= DEG2RAD ;
  p_xs_3->nup5 *= DEG2RAD ;
  p_xs_3->taup5 *= DEG2RAD ;
  p_xs_3->mup6 *= DEG2RAD ;
  p_xs_3->nup6 *= DEG2RAD ;
  p_xs_3->taup6 *= DEG2RAD ;
  p_xs_3->mug *= DEG2RAD ;
  p_xs_3->nug *= DEG2RAD ;
  p_xs_3->taug *= DEG2RAD ;
  p_xs_3->mud *= DEG2RAD ;
  p_xs_3->nud *= DEG2RAD ;
  p_xs_3->taud *= DEG2RAD ;
  p_xs_3->chiprot *= DEG2RAD ;

 cleanup:
   XSH_TABLE_FREE( tab) ;
   xsh_instrument_free(&instr);
   return cpl_error_get_code();

}

int xsh_model_readfits(double * abest,double * amin,
                          double * amax, int * aname,
                       const char * xs_config_file,
                       const char* xs_config_tag,
                          struct xs_3 *p_xs_3,
                          ann_all_par *p_all_par)
{
  int ii,jj,kk,compa, par_found, indlen;
  char tempstr[10];
  double d2r;

  cpl_table *   tab ;
  int           nlines ;
  XSH_INSTRCONFIG* instr_config=NULL;
  xsh_instrument* instr = NULL;

  instr = xsh_instrument_new();
  p_xs_3->config_mjd=0.0;
  /*default distortion coefficients in case they are not present */
  p_xs_3->pc_x_x1=1.0;
  p_xs_3->pc_y_y1=1.0;
  p_xs_3->pc_x_xx=0.0;
  p_xs_3->pc_x_xy=0.0;
  p_xs_3->pc_x_x3=0.0;
  p_xs_3->pc_x_y1=0.0;
  p_xs_3->pc_x_yy=0.0;
  p_xs_3->pc_x_y3=0.0;
  p_xs_3->pc_x_x2y=0.0;
  p_xs_3->pc_x_y2x=0.0;
  p_xs_3->pc_y_xx=0.0;
  p_xs_3->pc_y_xy=0.0;
  p_xs_3->pc_y_x3=0.0;
  p_xs_3->pc_y_x1=0.0;
  p_xs_3->pc_y_yy=0.0;
  p_xs_3->pc_y_y3=0.0;
  p_xs_3->pc_y_x2y=0.0;
  p_xs_3->pc_y_y2x=0.0;
  p_xs_3->pc4_x_x3y=0.0;
  p_xs_3->pc4_x_xy3=0.0;
  p_xs_3->pc4_x_x2y2=0.0;
  p_xs_3->pc4_x_x4=0.0;
  p_xs_3->pc4_x_y4=0.0;
  p_xs_3->pc4_y_xy3=0.0;
  p_xs_3->pc4_y_x3y=0.0;
  p_xs_3->pc4_y_x2y2=0.0;
  p_xs_3->pc4_y_x4=0.0;
  p_xs_3->pc4_y_y4=0.0;
  p_xs_3->ca_x0=0.0;
  p_xs_3->ca_x1=0.0;
  p_xs_3->ca_y0=0.0;
  p_xs_3->ca_y1=0.0;
  p_xs_3->d2_x1=0.0;
  p_xs_3->d2_x2=0.0;
  p_xs_3->d2_x3=0.0;
  p_xs_3->d2_y1x0=0.0;
  p_xs_3->d2_y1x1=0.0;
  p_xs_3->d2_y1x2=0.0;
  p_xs_3->d2_y1x3=0.0;
  p_xs_3->d2_y2x0=0.0;
  p_xs_3->d2_y2x1=0.0;
  p_xs_3->d2_y2x2=0.0;
  p_xs_3->d2_y2x3=0.0;
  p_xs_3->d2_y3x0=0.0;
  p_xs_3->d2_y3x1=0.0;
  p_xs_3->d2_y3x2=0.0;
  p_xs_3->d2_y3x3=0.0;
  p_xs_3->offx=0.0;
  p_xs_3->offy=0.0;

  if (strncmp(xs_config_tag,XSH_MOD_CFG_NIR,16)==0 ||
      strncmp(xs_config_tag,XSH_MOD_CFG_TAB_NIR,20)==0) {
    p_xs_3->pc4_x_x3y=0.0;
    p_xs_3->pc4_x_xy3=0.0;
    p_xs_3->pc4_x_x2y2=0.0;
    p_xs_3->pc4_x_x4=0.0;
    p_xs_3->pc4_x_y4=0.0;
    p_xs_3->pc4_y_xy3=0.0;
    p_xs_3->pc4_y_x3y=0.0;
    p_xs_3->pc4_y_x2y2=0.0;
    p_xs_3->pc4_y_x4=0.0;
    p_xs_3->pc4_y_y4=0.0;
    p_xs_3->ca_x0=0.0;
    p_xs_3->ca_x1=0.0;
    p_xs_3->ca_y0=0.0;
    p_xs_3->ca_y1=0.0;
    p_xs_3->offx=0.0;
    p_xs_3->offy=0.0;
  }
  else if (strncmp(xs_config_tag,XSH_MOD_CFG_VIS,16)==0 ||
	   strncmp(xs_config_tag,XSH_MOD_CFG_TAB_VIS,20)==0) {
    p_xs_3->d2_x1=0.00287311525385538;
    p_xs_3->d2_x2=1.55125984130817E-05;
    p_xs_3->d2_x3=-4.00980399239143E-05;
  }
  else if (strncmp(xs_config_tag,XSH_MOD_CFG_UVB,16)==0 ||
	   strncmp(xs_config_tag,XSH_MOD_CFG_TAB_UVB,20)==0) {
    p_xs_3->d2_x1=0.0;
    p_xs_3->d2_x2=0.0;
    p_xs_3->d2_x3=0.0;
  }



  xsh_msg("tag=%s",xs_config_tag);

    /* Load the table */
  if ((tab = xsh_load_table_check(xs_config_file,xs_config_tag)) == NULL) {
    cpl_msg_error(__func__, "Cannot load the config table") ;
    return -1 ;
  }
  nlines = cpl_table_get_nrow(tab) ;

  for (ii=0 ; ii<nlines ; ii++) {
    (p_all_par+ii)->best=cpl_table_get_double(tab, XSH_COL_MODEL_CONF_BEST,
					      ii, NULL) ;
    (p_all_par+ii)->min = cpl_table_get_double(tab, XSH_COL_MODEL_CONF_LOW,
					       ii, NULL) ;
    (p_all_par+ii)->max = cpl_table_get_double(tab,XSH_COL_MODEL_CONF_HIGH,
					       ii, NULL) ;
    (p_all_par+ii)->flag = cpl_table_get_int(tab, XSH_COL_MODEL_CONF_FLAG,
					     ii, NULL) ;
    sprintf((p_all_par+ii)->name, (char*)cpl_table_get_string(tab,
                  XSH_COL_MODEL_CONF_NAME, ii));
    if ((p_all_par+ii)->flag==1) {
      if ((p_all_par+ii)->best<(p_all_par+ii)->min ||
          (p_all_par+ii)->best>(p_all_par+ii)->max) {
        printf("limits wrong in config file for parameter: %s \n",
               (p_all_par+ii)->name);
        abort();
      }
    }
  }
  xsh_free_table(&tab);
  jj=0;
  for (ii=0 ; ii<nlines ; ii++) {
    d2r=1.0;
    par_found=0;
/*below the number of char in the variable name MUST be given to strncmp*/
    compa=strncmp((p_all_par+ii)->name,"arm",3);
    if (compa==0) {
      p_xs_3->arm=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"temper",6);
    if (compa==0) {
      p_xs_3->temper=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"arm",3);
    if (compa==0) {
      p_xs_3->arm=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"config_mjd",10);
    if (compa==0) {
      p_xs_3->config_mjd=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"t_ir_p2",7);
    if (compa==0) {
      p_xs_3->t_ir_p2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"t_ir_p3",7);
    if (compa==0) {
      p_xs_3->t_ir_p3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"es_x",4);
    if (compa==0) {
      p_xs_3->es_x=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"es_y",4);
    if (compa==0) {
      p_xs_3->es_y=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mues",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mues=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nues",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nues=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taues",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taues=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"slit_scale",10);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->slit_scale=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"es_s",4);
    if (compa==0) {
      p_xs_3->es_s=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"es_w",4);
    if (compa==0) {
      p_xs_3->es_w=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"fcol",4);
    if (compa==0) {
      p_xs_3->fcol=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"cmup1",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->cmup1=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup1",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup1=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup1",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup1=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup1",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup1=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup2",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup2=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup2",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup2=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup2",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup2=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup3",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup3=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup3",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup3=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup3",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup3=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup4",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup4=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup4",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup4=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup4",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup4=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup5",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup5=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup5",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup5=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup5",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup5=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mup6",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mup6=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nup6",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nup6=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taup6",5);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taup6=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mug",3);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mug=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nug",3);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nug=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taug",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taug=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"sg",2);
    if (compa==0) {
      p_xs_3->sg=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"fdet",4);
    if (compa==0) {
      p_xs_3->fdet=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"mud",3);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->mud=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"nud",3);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->nud=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"taud",4);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->taud=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pix",3);
    if (compa==0) {
      p_xs_3->pix=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"chipx",5);
    if (compa==0) {
      p_xs_3->chipx=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"chipy",5);
    if (compa==0) {
      p_xs_3->chipy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"chiprot",7);
    if (compa==0) {
      d2r=DEG2RAD;
      p_xs_3->chiprot=(p_all_par+ii)->best*d2r;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_xx",7);
    if (compa==0) {
      p_xs_3->pc_x_xx=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_x1",7);
    if (compa==0) {
      p_xs_3->pc_x_x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_yy",7);
    if (compa==0) {
      p_xs_3->pc_x_yy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_y1",7);
    if (compa==0) {
      p_xs_3->pc_x_y1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_xy",7);
    if (compa==0) {
      p_xs_3->pc_x_xy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_x3",7);
    if (compa==0) {
      p_xs_3->pc_x_x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_x2y",8);
    if (compa==0) {
      p_xs_3->pc_x_x2y=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_y2x",8);
    if (compa==0) {
      p_xs_3->pc_x_y2x=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_x_y3",7);
    if (compa==0) {
      p_xs_3->pc_x_y3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_xx",7);
    if (compa==0) {
      p_xs_3->pc_y_xx=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_x1",7);
    if (compa==0) {
      p_xs_3->pc_y_x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_yy",7);
    if (compa==0) {
      p_xs_3->pc_y_yy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_y1",7);
    if (compa==0) {
      p_xs_3->pc_y_y1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_xy",7);
    if (compa==0) {
      p_xs_3->pc_y_xy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_x3",7);
    if (compa==0) {
      p_xs_3->pc_y_x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_x2y",8);
    if (compa==0) {
      p_xs_3->pc_y_x2y=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_y2x",8);
    if (compa==0) {
      p_xs_3->pc_y_y2x=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc_y_y3",7);
    if (compa==0) {
      p_xs_3->pc_y_y3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_x_xy3",9);
    if (compa==0) {
      p_xs_3->pc4_x_xy3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_x_x3y",9);
    if (compa==0) {
      p_xs_3->pc4_x_x3y=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_x_x2y2",10);
    if (compa==0) {
      p_xs_3->pc4_x_x2y2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_x_x4",8);
    if (compa==0) {
      p_xs_3->pc4_x_x4=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_x_y4",8);
    if (compa==0) {
      p_xs_3->pc4_x_y4=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_y_xy3",9);
    if (compa==0) {
      p_xs_3->pc4_y_xy3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_y_x3y",9);
    if (compa==0) {
      p_xs_3->pc4_y_x3y=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_y_x2y2",10);
    if (compa==0) {
      p_xs_3->pc4_y_x2y2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_y_x4",8);
    if (compa==0) {
      p_xs_3->pc4_y_x4=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"pc4_y_y4",8);
    if (compa==0) {
      p_xs_3->pc4_y_y4=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"ca_x0",5);
    if (compa==0) {
      p_xs_3->ca_x0=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"ca_x1",5);
    if (compa==0) {
      p_xs_3->ca_x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"ca_y0",5);
    if (compa==0) {
      p_xs_3->ca_y0=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"ca_y1",5);
    if (compa==0) {
      p_xs_3->ca_y1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_x1",5);
    if (compa==0) {
      p_xs_3->d2_x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_x2",5);
    if (compa==0) {
      p_xs_3->d2_x2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_x3",5);
    if (compa==0) {
      p_xs_3->d2_x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y1x0",7);
    if (compa==0) {
      p_xs_3->d2_y1x0=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y1x1",7);
    if (compa==0) {
      p_xs_3->d2_y1x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y1x2",7);
    if (compa==0) {
      p_xs_3->d2_y1x2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y1x3",7);
    if (compa==0) {
      p_xs_3->d2_y1x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y2x0",7);
    if (compa==0) {
      p_xs_3->d2_y2x0=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y2x1",7);
    if (compa==0) {
      p_xs_3->d2_y2x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y2x2",7);
    if (compa==0) {
      p_xs_3->d2_y2x2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y2x3",7);
    if (compa==0) {
      p_xs_3->d2_y2x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y3x0",7);
    if (compa==0) {
      p_xs_3->d2_y3x0=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y3x1",7);
    if (compa==0) {
      p_xs_3->d2_y3x1=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y3x2",7);
    if (compa==0) {
      p_xs_3->d2_y3x2=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"d2_y3x3",7);
    if (compa==0) {
      p_xs_3->d2_y3x3=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"offx",4);
    if (compa==0) {
      p_xs_3->offx=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"offy",4);
    if (compa==0) {
      p_xs_3->offy=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"flipx",5);
    if (compa==0) {
      p_xs_3->flipx=(p_all_par+ii)->best;
      par_found=1;
    }
    compa=strncmp((p_all_par+ii)->name,"flipy",5);
    if (compa==0) {
      p_xs_3->flipy=(p_all_par+ii)->best;
      par_found=1;
    }
    for (kk=0;kk<9;kk++) {
      sprintf(tempstr,"slit[%d]",kk);
      indlen=strlen(tempstr);
      compa=strncmp((p_all_par+ii)->name,tempstr,indlen);
      if (compa==0) {
        p_xs_3->slit[kk]=(p_all_par+ii)->best;
        par_found=1;
      }
    }
    if (par_found==1) {
      if ((p_all_par+ii)->flag!=0) {
        abest[jj]=(p_all_par+ii)->best;
        amin[jj]=(p_all_par+ii)->min;
        amax[jj]=(p_all_par+ii)->max;
        aname[jj]=ii;
        printf("%d %s %d \n", ii, (p_all_par+ii)->name, jj);
        jj++;
      }
    }
  }
  if (p_xs_3->arm==0) {
    p_xs_3->morder=UVB_morder;
    p_xs_3->blaze_pad=UVB_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_UVB);
    p_xs_3->xsize_corr=UVB_xsize_corr;
    p_xs_3->ysize_corr=UVB_ysize_corr;
  }
  else if (p_xs_3->arm==1) {
    p_xs_3->morder=VIS_morder;
    p_xs_3->blaze_pad=VIS_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_VIS);
    p_xs_3->xsize_corr=VIS_xsize_corr;
    p_xs_3->ysize_corr=VIS_ysize_corr;
  }
  else if (p_xs_3->arm==2) {
    p_xs_3->morder=NIR_morder;
    p_xs_3->blaze_pad=NIR_blaze_pad;
    xsh_instrument_set_arm(instr, XSH_ARM_NIR);
    p_xs_3->xsize_corr=NIR_xsize_corr;
    p_xs_3->ysize_corr=NIR_ysize_corr;
  }
  else {
    return -1;
  }
  instr_config=xsh_instrument_get_config(instr);
  p_xs_3->BSIZE=instr_config->ny;
  p_xs_3->ASIZE=instr_config->nx;
  p_xs_3->SIZE=p_xs_3->BSIZE;
  p_xs_3->chipypix=(float)(p_xs_3->BSIZE);
  p_xs_3->chipxpix=(float)(p_xs_3->ASIZE);
  p_xs_3->morder_min=instr_config->order_min;
  p_xs_3->morder_max=instr_config->order_max;

  p_xs_3->pix_X=p_xs_3->pix_Y=p_xs_3->pix;

  xsh_instrument_free(&instr);

  return jj;
}

#define xsh_model_config_enter(name, val, index, units)			\
    cpl_table_set_string(tab, XSH_COL_MODEL_CONF_NAME, index, name) ;       \
    cpl_table_set_double(tab, XSH_COL_MODEL_CONF_BEST, index, val) ;        \
    cpl_table_set_double(tab, XSH_COL_MODEL_CONF_LOW,  index, val) ;        \
    cpl_table_set_double(tab, XSH_COL_MODEL_CONF_HIGH, index, val) ;        \
    cpl_table_set_int(tab, XSH_COL_MODEL_CONF_FLAG, index, 0) ;        \
    cpl_table_set_string(tab, XSH_COL_MODEL_CONF_UNITS, index, units) ;                 

cpl_table * xsh_model_io_output_cfg(struct xs_3 *p_xs_3)
{
    cpl_table   *   tab ;
    char            tempstr[10];
    int             nb_parameters ;
    int             i, j ;
  
    /* Initialise */
    nb_parameters = 91 ;
    
    /* Create the table */
    tab = cpl_table_new(nb_parameters) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_BEST, CPL_TYPE_DOUBLE) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_LOW, CPL_TYPE_DOUBLE) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_HIGH, CPL_TYPE_DOUBLE) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_FLAG, CPL_TYPE_INT) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_NAME, CPL_TYPE_STRING) ;
    cpl_table_new_column(tab, XSH_COL_MODEL_CONF_UNITS, CPL_TYPE_STRING) ;

    i = 0 ;
    xsh_model_config_enter("arm",     p_xs_3->arm,       i, NULL) ; i++ ;
    xsh_model_config_enter("temper",     p_xs_3->temper,       i,"K") ; i++ ;
    xsh_model_config_enter("t_ir_p2",     p_xs_3->t_ir_p2,       i,"K") ; i++ ;
    xsh_model_config_enter("t_ir_p3",     p_xs_3->t_ir_p3,       i,"K") ; i++ ;
    xsh_model_config_enter("es_x",      p_xs_3->es_x,        i,"mm") ; i++ ;
    xsh_model_config_enter("es_y",       p_xs_3->es_y,         i,"mm") ; i++ ;
    xsh_model_config_enter("mues",       p_xs_3->mues/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nues",       p_xs_3->nues/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taues",      p_xs_3->taues/DEG2RAD,i,"deg") ; i++ ;
    xsh_model_config_enter("slit_scale",      p_xs_3->slit_scale,i,"mm/\"") ; i++ ;
    xsh_model_config_enter("es_s",       p_xs_3->es_s,         i,"mm") ; i++ ;
    xsh_model_config_enter("es_w",       p_xs_3->es_w,         i,"mm") ; i++ ;
    xsh_model_config_enter("fcol",        p_xs_3->fcol,          i,"mm") ; i++ ;
    xsh_model_config_enter("cmup1",       p_xs_3->cmup1/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup1",       p_xs_3->mup1/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup1",       p_xs_3->nup1/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup1",      p_xs_3->taup1/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup2",       p_xs_3->mup2/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup2",       p_xs_3->nup2/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup2",      p_xs_3->taup2/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup3",       p_xs_3->mup3/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup3",       p_xs_3->nup3/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup3",      p_xs_3->taup3/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup4",       p_xs_3->mup4/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup4",       p_xs_3->nup4/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup4",      p_xs_3->taup4/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup5",       p_xs_3->mup5/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup5",       p_xs_3->nup5/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup5",      p_xs_3->taup5/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mup6",       p_xs_3->mup6/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("nup6",       p_xs_3->nup6/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("taup6",      p_xs_3->taup6/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("mug",        p_xs_3->mug/DEG2RAD,  i,"deg") ; i++ ;
    xsh_model_config_enter("nug",        p_xs_3->nug/DEG2RAD,  i,"deg") ; i++ ;
    xsh_model_config_enter("taug",       p_xs_3->taug/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("sg",         p_xs_3->sg,           i,"mm-1") ; i++ ;
    xsh_model_config_enter("fdet",       p_xs_3->fdet,           i,"mm") ; i++ ;
    xsh_model_config_enter("mud",        p_xs_3->mud/DEG2RAD,  i,"deg") ; i++ ;
    xsh_model_config_enter("nud",        p_xs_3->nud/DEG2RAD,  i,"deg") ; i++ ;
    xsh_model_config_enter("taud",       p_xs_3->taud/DEG2RAD, i,"deg") ; i++ ;
    xsh_model_config_enter("pix",        p_xs_3->pix,          i,"mm") ; i++ ;
    xsh_model_config_enter("chipx",   p_xs_3->chipx,     i,"mm") ; i++ ;
    xsh_model_config_enter("chipy",   p_xs_3->chipy,     i,"mm") ; i++ ;
    xsh_model_config_enter("chiprot", p_xs_3->chiprot/DEG2RAD,i,"deg");i++;
    xsh_model_config_enter("pc_x_xx",   p_xs_3->pc_x_xx,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_x1",   p_xs_3->pc_x_x1,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_yy",   p_xs_3->pc_x_yy,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_y1",   p_xs_3->pc_x_y1,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_xy",   p_xs_3->pc_x_xy,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_x3",   p_xs_3->pc_x_x3,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_x2y",   p_xs_3->pc_x_x2y,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_y2x",   p_xs_3->pc_x_y2x,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_x_y3",   p_xs_3->pc_x_y3,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_xx",   p_xs_3->pc_y_xx,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_x1",   p_xs_3->pc_y_x1,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_yy",   p_xs_3->pc_y_yy,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_y1",   p_xs_3->pc_y_y1,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_xy",   p_xs_3->pc_y_xy,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_x3",   p_xs_3->pc_y_x3,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_x2y",   p_xs_3->pc_y_x2y,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_y2x",   p_xs_3->pc_y_y2x,     i,NULL) ; i++ ;
    xsh_model_config_enter("pc_y_y3",   p_xs_3->pc_y_y3,     i,NULL) ; i++ ;
    if (p_xs_3->arm==2 && p_xs_3->d2_y1x0==0.0) {
      xsh_model_config_enter("pc4_x_xy3",   p_xs_3->pc4_x_xy3,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_x_x3y",   p_xs_3->pc4_x_x3y,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_x_x2y2",   p_xs_3->pc4_x_x2y2,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_x_x4",   p_xs_3->pc4_x_x4,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_x_y4",   p_xs_3->pc4_x_y4,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_y_xy3",   p_xs_3->pc4_y_xy3,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_y_x3y",   p_xs_3->pc4_y_x3y,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_y_x2y2",   p_xs_3->pc4_y_x2y2,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_y_x4",   p_xs_3->pc4_y_x4,     i,NULL) ; i++ ;
      xsh_model_config_enter("pc4_y_y4",   p_xs_3->pc4_y_y4,     i,NULL) ; i++ ;
      xsh_model_config_enter("ca_x0",   p_xs_3->ca_x0,     i,NULL) ; i++ ;
      xsh_model_config_enter("ca_x1",   p_xs_3->ca_x1,     i,NULL) ; i++ ;
      xsh_model_config_enter("ca_y0",   p_xs_3->ca_y0,     i,NULL) ; i++ ;
      xsh_model_config_enter("ca_y1",   p_xs_3->ca_y1,     i,NULL) ; i++ ;
    }
    else {
      xsh_model_config_enter("d2_y1x0",   p_xs_3->d2_y1x0,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y1x1",   p_xs_3->d2_y1x1,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y1x2",   p_xs_3->d2_y1x2,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y1x3",   p_xs_3->d2_y1x3,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y2x0",   p_xs_3->d2_y2x0,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y2x1",   p_xs_3->d2_y2x1,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y2x2",   p_xs_3->d2_y2x2,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y2x3",   p_xs_3->d2_y2x3,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y3x0",   p_xs_3->d2_y3x0,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y3x1",   p_xs_3->d2_y3x1,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y3x2",   p_xs_3->d2_y3x2,     i,NULL) ; i++ ;
      xsh_model_config_enter("d2_y3x3",   p_xs_3->d2_y3x3,     i,NULL) ; i++ ;
      xsh_model_config_enter("offx",       p_xs_3->offx,         i,"pix") ; i++ ;
      xsh_model_config_enter("offy",       p_xs_3->offy,         i,"pix") ; i++ ;
    }
    xsh_model_config_enter("d2_x1",   p_xs_3->d2_x1,     i,NULL) ; i++ ;
    xsh_model_config_enter("d2_x2",   p_xs_3->d2_x2,     i,NULL) ; i++ ;
    xsh_model_config_enter("d2_x3",   p_xs_3->d2_x3,     i,NULL) ; i++ ;

    xsh_model_config_enter("flipx",      p_xs_3->flipx,        i,NULL) ; i++ ;
    xsh_model_config_enter("flipy",      p_xs_3->flipy,        i,NULL) ; i++ ;
    for (j=0 ; j<9 ; j++) {
        sprintf(tempstr, "slit[%d]", j) ;
        xsh_model_config_enter(tempstr,  p_xs_3->slit[j],      i,"\"") ; i++ ;
    }
    xsh_model_config_enter("config_mjd",     p_xs_3->config_mjd,       i,"days") ; i++ ;

    return tab ;
}




/**@}*/

cpl_table * xsh_load_table_check(
        const char  *   in,
        const char  *   procatg)
{
    cpl_table           *   out ;
    int                     nok ;

    /* Check entries */
    if (in == NULL) return NULL ;
    if (procatg == NULL) return NULL ;

    
    /* Load the table */
    if ((out = cpl_table_load(in, 1, 0)) == NULL) {
        cpl_msg_error(__func__, "Cannot load %s as a table", in) ;
        return NULL ;
    }

    /* Check that the columns are there */
    nok = 0 ;
    // XSH_MOD_CFG_UVB
    /* REG : add new PRO CATG there */
    if (
         !strcmp(procatg, XSH_MOD_CFG_TAB_UVB)  ||
         !strcmp(procatg, XSH_MOD_CFG_TAB_VIS)  ||
         !strcmp(procatg, XSH_MOD_CFG_TAB_NIR)  ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_AFC_UVB)  ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_AFC_VIS)  ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_AFC_NIR)  ||

         !strcmp(procatg, XSH_MOD_CFG_OPT_REC_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_REC_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_REC_NIR) ||


         !strcmp(procatg, XSH_MOD_CFG_OPT_FMT_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_FMT_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_FMT_NIR) ||


         !strcmp(procatg, XSH_MOD_CFG_OPT_2D_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_2D_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_2D_NIR) ||

         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_SLIT_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_SLIT_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_SLIT_NIR) ||

         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_IFU_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_IFU_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPT_WAV_IFU_NIR) ||
/* For now the following 3 checks are duplicates of the first 3
 * since XSH_MOD_CFG_*** == XSH_MOD_CFG_TAB_***                 */
      /* !strcmp(procatg, XSH_MOD_CFG_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_NIR) || */
         !strcmp(procatg, XSH_MOD_CFG_FAN_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_FAN_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_FAN_NIR) ||
         !strcmp(procatg, XSH_MOD_CFG_OPEN_UVB) ||
         !strcmp(procatg, XSH_MOD_CFG_OPEN_VIS) ||
         !strcmp(procatg, XSH_MOD_CFG_OPEN_NIR)) {

        nok += xsh_table_check_column(out, XSH_COL_MODEL_CONF_NAME) ;
        nok += xsh_table_check_column(out, XSH_COL_MODEL_CONF_BEST) ;
        nok += xsh_table_check_column(out, XSH_COL_MODEL_CONF_LOW) ;
        nok += xsh_table_check_column(out, XSH_COL_MODEL_CONF_HIGH) ;
        nok += xsh_table_check_column(out, XSH_COL_MODEL_CONF_FLAG) ;
    } else {
        cpl_msg_error(__func__, "Unsupported PRO.CATG: %s", procatg) ;
        cpl_table_delete(out) ;
        return NULL ;
    }

    /* Check if ok */
    if (nok > 0) {
      xsh_msg_error("%d",cpl_error_get_code());
      xsh_msg_error("%s",cpl_error_get_where());
        cpl_table_delete(out) ;
        return NULL ;
    }
    
    return out ;
}


static int xsh_table_check_column(
        const cpl_table     *   tab,
        const char          *   col) 
{
    int         ret ;
    if (!cpl_table_has_column(tab, col)) {
        cpl_msg_error(__func__, "Column %s is missing", col) ;
        ret = 1 ;
    } else ret = 0 ;
    return ret ;
}

void xsh_model_io_output_cfg_txt(struct xs_3 *p_xs_3)
{
  FILE * tempcfg;
  char tempstr[10];
  int jj;

  /* Initialise */
  tempcfg=fopen("xsh_temp.cfg","w");

  fprintf(tempcfg,"%lf %lf %lf %d temper\n",     p_xs_3->temper,p_xs_3->temper,p_xs_3->temper,0);
  fprintf(tempcfg,"%lf %lf %lf %d t_ir_p2\n",     p_xs_3->t_ir_p2,p_xs_3->t_ir_p2,p_xs_3->t_ir_p2,0);
  fprintf(tempcfg,"%lf %lf %lf %d t_ir_p3\n",     p_xs_3->t_ir_p3,p_xs_3->t_ir_p3,p_xs_3->t_ir_p3,0);
  fprintf(tempcfg,"%lf %lf %lf %d es_x\n",      p_xs_3->es_x,p_xs_3->es_x,p_xs_3->es_x,0);
  fprintf(tempcfg,"%lf %lf %lf %d es_y\n",       p_xs_3->es_y,p_xs_3->es_y,p_xs_3->es_y,0);
  fprintf(tempcfg,"%lf es_y_tot\n",       p_xs_3->es_y_tot);
  fprintf(tempcfg,"%lf %lf %lf %d mues\n",       p_xs_3->mues/DEG2RAD,p_xs_3->mues/DEG2RAD,p_xs_3->mues/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nues\n",       p_xs_3->nues/DEG2RAD,p_xs_3->nues/DEG2RAD,p_xs_3->nues/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taues\n",      p_xs_3->taues/DEG2RAD,p_xs_3->taues/DEG2RAD,p_xs_3->taues/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d slit_scale\n",       p_xs_3->slit_scale,p_xs_3->slit_scale,p_xs_3->slit_scale,0);
  fprintf(tempcfg,"%lf %lf %lf %d es_s\n",       p_xs_3->es_s,p_xs_3->es_s,p_xs_3->es_s,0);
  fprintf(tempcfg,"%lf %lf %lf %d es_w\n",       p_xs_3->es_w,p_xs_3->es_w,p_xs_3->es_w,0);
  fprintf(tempcfg,"%lf %lf %lf %d fcol\n",        p_xs_3->fcol,p_xs_3->fcol,p_xs_3->fcol,0);
  fprintf(tempcfg,"%lf %lf %lf %d cmup1\n",       p_xs_3->cmup1/DEG2RAD,p_xs_3->cmup1/DEG2RAD,p_xs_3->cmup1/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup1\n",       p_xs_3->mup1/DEG2RAD,p_xs_3->mup1/DEG2RAD,p_xs_3->mup1/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup1\n",       p_xs_3->nup1/DEG2RAD,p_xs_3->nup1/DEG2RAD,p_xs_3->nup1/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup1\n",      p_xs_3->taup1/DEG2RAD,p_xs_3->taup1/DEG2RAD,p_xs_3->taup1/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup2\n",       p_xs_3->mup2/DEG2RAD,p_xs_3->mup2/DEG2RAD,p_xs_3->mup2/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup2\n",       p_xs_3->nup2/DEG2RAD,p_xs_3->nup2/DEG2RAD,p_xs_3->nup2/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup2\n",      p_xs_3->taup2/DEG2RAD,p_xs_3->taup2/DEG2RAD,p_xs_3->taup2/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup3\n",       p_xs_3->mup3/DEG2RAD,p_xs_3->mup3/DEG2RAD,p_xs_3->mup3/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup3\n",       p_xs_3->nup3/DEG2RAD,p_xs_3->nup3/DEG2RAD,p_xs_3->nup3/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup3\n",      p_xs_3->taup3/DEG2RAD,p_xs_3->taup3/DEG2RAD,p_xs_3->taup3/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup4\n",       p_xs_3->mup4/DEG2RAD,p_xs_3->mup4/DEG2RAD,p_xs_3->mup4/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup4\n",       p_xs_3->nup4/DEG2RAD,p_xs_3->nup4/DEG2RAD,p_xs_3->nup4/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup4\n",      p_xs_3->taup4/DEG2RAD,p_xs_3->taup4/DEG2RAD,p_xs_3->taup4/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup5\n",       p_xs_3->mup5/DEG2RAD,p_xs_3->mup5/DEG2RAD,p_xs_3->mup5/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup5\n",       p_xs_3->nup5/DEG2RAD,p_xs_3->nup5/DEG2RAD,p_xs_3->nup5/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup5\n",      p_xs_3->taup5/DEG2RAD,p_xs_3->taup5/DEG2RAD,p_xs_3->taup5/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mup6\n",       p_xs_3->mup6/DEG2RAD,p_xs_3->mup6/DEG2RAD,p_xs_3->mup6/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nup6\n",       p_xs_3->nup6/DEG2RAD,p_xs_3->nup6/DEG2RAD,p_xs_3->nup6/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taup6\n",      p_xs_3->taup6/DEG2RAD,p_xs_3->taup6/DEG2RAD,p_xs_3->taup6/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d mug\n",        p_xs_3->mug/DEG2RAD,p_xs_3->mug/DEG2RAD,p_xs_3->mug/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nug\n",        p_xs_3->nug/DEG2RAD,p_xs_3->nug/DEG2RAD,p_xs_3->nug/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taug\n",       p_xs_3->taug/DEG2RAD,p_xs_3->taug/DEG2RAD,p_xs_3->taug/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d sg\n",         p_xs_3->sg,p_xs_3->sg,p_xs_3->sg,0);
  fprintf(tempcfg,"%lf %lf %lf %d fdet\n",         p_xs_3->fdet,p_xs_3->fdet,p_xs_3->fdet,0);
  fprintf(tempcfg,"%lf %lf %lf %d mud\n",        p_xs_3->mud/DEG2RAD,p_xs_3->mud/DEG2RAD,p_xs_3->mud/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d nud\n",        p_xs_3->nud/DEG2RAD,p_xs_3->nud/DEG2RAD,p_xs_3->nud/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d taud\n",       p_xs_3->taud/DEG2RAD,p_xs_3->taud/DEG2RAD,p_xs_3->taud/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d pix\n",        p_xs_3->pix,p_xs_3->pix,p_xs_3->pix,0);
  fprintf(tempcfg,"%lf %lf %lf %d chipx\n",   p_xs_3->chipx,p_xs_3->chipx,p_xs_3->chipx,0);
  fprintf(tempcfg,"%lf %lf %lf %d chipy\n",   p_xs_3->chipy,p_xs_3->chipy,p_xs_3->chipy,0);
  fprintf(tempcfg,"%lf %lf %lf %d chiprot\n", p_xs_3->chiprot/DEG2RAD,p_xs_3->chiprot/DEG2RAD,p_xs_3->chiprot/DEG2RAD,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_xx\n",   p_xs_3->pc_x_xx,p_xs_3->pc_x_xx,p_xs_3->pc_x_xx,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_x1\n",   p_xs_3->pc_x_x1,p_xs_3->pc_x_x1,p_xs_3->pc_x_x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_yy\n",   p_xs_3->pc_x_yy,p_xs_3->pc_x_yy,p_xs_3->pc_x_yy,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_y1\n",   p_xs_3->pc_x_y1,p_xs_3->pc_x_y1,p_xs_3->pc_x_y1,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_xy\n",   p_xs_3->pc_x_xy,p_xs_3->pc_x_xy,p_xs_3->pc_x_xy,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_x3\n",   p_xs_3->pc_x_x3,p_xs_3->pc_x_x3,p_xs_3->pc_x_x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_x2y\n",   p_xs_3->pc_x_x2y,p_xs_3->pc_x_x2y,p_xs_3->pc_x_x2y,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_y2x\n",   p_xs_3->pc_x_y2x,p_xs_3->pc_x_y2x,p_xs_3->pc_x_y2x,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_x_y3\n",   p_xs_3->pc_x_y3,p_xs_3->pc_x_y3,p_xs_3->pc_x_y3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_xx\n",   p_xs_3->pc_y_xx,p_xs_3->pc_y_xx,p_xs_3->pc_y_xx,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_x1\n",   p_xs_3->pc_y_x1,p_xs_3->pc_y_x1,p_xs_3->pc_y_x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_yy\n",   p_xs_3->pc_y_yy,p_xs_3->pc_y_yy,p_xs_3->pc_y_yy,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_y1\n",   p_xs_3->pc_y_y1,p_xs_3->pc_y_y1,p_xs_3->pc_y_y1,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_xy\n",   p_xs_3->pc_y_xy,p_xs_3->pc_y_xy,p_xs_3->pc_y_xy,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_x3\n",   p_xs_3->pc_y_x3,p_xs_3->pc_y_x3,p_xs_3->pc_y_x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_x2y\n",   p_xs_3->pc_y_x2y,p_xs_3->pc_y_x2y,p_xs_3->pc_y_x2y,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_y2x\n",   p_xs_3->pc_y_y2x,p_xs_3->pc_y_y2x,p_xs_3->pc_y_y2x,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc_y_y3\n",   p_xs_3->pc_y_y3,p_xs_3->pc_y_y3,p_xs_3->pc_y_y3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_x_xy3\n",   p_xs_3->pc4_x_xy3,p_xs_3->pc4_x_xy3,p_xs_3->pc4_x_xy3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_x_x3y\n",   p_xs_3->pc4_x_x3y,p_xs_3->pc4_x_x3y,p_xs_3->pc4_x_x3y,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_x_x2y2\n",   p_xs_3->pc4_x_x2y2,p_xs_3->pc4_x_x2y2,p_xs_3->pc4_x_x2y2,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_x_x4\n",   p_xs_3->pc4_x_x4,p_xs_3->pc4_x_x4,p_xs_3->pc4_x_x4,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_x_y4\n",   p_xs_3->pc4_x_y4,p_xs_3->pc4_x_y4,p_xs_3->pc4_x_x4,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_y_xy3\n",   p_xs_3->pc4_y_xy3,p_xs_3->pc4_y_xy3,p_xs_3->pc4_y_xy3,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_y_x3y\n",   p_xs_3->pc4_y_x3y,p_xs_3->pc4_y_x3y,p_xs_3->pc4_y_x3y,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_y_x2y2\n",   p_xs_3->pc4_y_x2y2,p_xs_3->pc4_y_x2y2,p_xs_3->pc4_y_x2y2,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_y_x4\n",   p_xs_3->pc4_y_x4,p_xs_3->pc4_y_x4,p_xs_3->pc4_y_x4,0);
  fprintf(tempcfg,"%lf %lf %lf %d pc4_y_y4\n",   p_xs_3->pc4_y_y4,p_xs_3->pc4_y_y4,p_xs_3->pc4_y_x4,0);
  fprintf(tempcfg,"%lf %lf %lf %d ca_x0\n",   p_xs_3->ca_x0,p_xs_3->ca_x0,p_xs_3->ca_x0,0);
  fprintf(tempcfg,"%lf %lf %lf %d ca_x1\n",   p_xs_3->ca_x1,p_xs_3->ca_x1,p_xs_3->ca_x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d ca_y0\n",   p_xs_3->ca_y0,p_xs_3->ca_y0,p_xs_3->ca_y0,0);
  fprintf(tempcfg,"%lf %lf %lf %d ca_y1\n",   p_xs_3->ca_y1,p_xs_3->ca_y1,p_xs_3->ca_y1,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_x1\n",   p_xs_3->d2_x1,p_xs_3->d2_x1,p_xs_3->d2_x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_x2\n",   p_xs_3->d2_x2,p_xs_3->d2_x2,p_xs_3->d2_x2,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_x3\n",   p_xs_3->d2_x3,p_xs_3->d2_x3,p_xs_3->d2_x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y1x0\n",   p_xs_3->d2_y1x0,p_xs_3->d2_y1x0,p_xs_3->d2_y1x0,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y1x1\n",   p_xs_3->d2_y1x1,p_xs_3->d2_y1x1,p_xs_3->d2_y1x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y1x2\n",   p_xs_3->d2_y1x2,p_xs_3->d2_y1x2,p_xs_3->d2_y1x2,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y1x3\n",   p_xs_3->d2_y1x3,p_xs_3->d2_y1x3,p_xs_3->d2_y1x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y2x0\n",   p_xs_3->d2_y2x0,p_xs_3->d2_y2x0,p_xs_3->d2_y2x0,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y2x1\n",   p_xs_3->d2_y2x1,p_xs_3->d2_y2x1,p_xs_3->d2_y2x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y2x2\n",   p_xs_3->d2_y2x2,p_xs_3->d2_y2x2,p_xs_3->d2_y2x2,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y2x3\n",   p_xs_3->d2_y2x3,p_xs_3->d2_y2x3,p_xs_3->d2_y2x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y3x0\n",   p_xs_3->d2_y3x0,p_xs_3->d2_y3x0,p_xs_3->d2_y3x0,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y3x1\n",   p_xs_3->d2_y3x1,p_xs_3->d2_y3x1,p_xs_3->d2_y3x1,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y3x2\n",   p_xs_3->d2_y3x2,p_xs_3->d2_y3x2,p_xs_3->d2_y3x2,0);
  fprintf(tempcfg,"%lf %lf %lf %d d2_y3x3\n",   p_xs_3->d2_y3x3,p_xs_3->d2_y3x3,p_xs_3->d2_y3x3,0);
  fprintf(tempcfg,"%lf %lf %lf %d offx\n",       p_xs_3->offx,p_xs_3->offx,p_xs_3->offx,0);
  fprintf(tempcfg,"%lf %lf %lf %d offy\n",       p_xs_3->offy,p_xs_3->offy,p_xs_3->offy,0);
  fprintf(tempcfg,"%lf %lf %lf %d flipx\n",      p_xs_3->flipx,p_xs_3->flipx,p_xs_3->flipx,0);
  fprintf(tempcfg,"%lf %lf %lf %d flipy\n",      p_xs_3->flipy,p_xs_3->flipy,p_xs_3->flipy,0);
  for (jj=0 ; jj<9 ; jj++) {
    sprintf(tempstr, "slit[%d]", jj) ;
    fprintf(tempcfg,"%lf %lf %lf %d %s \n",p_xs_3->slit[jj],p_xs_3->slit[jj],p_xs_3->slit[jj],0,tempstr);
  }
  fclose(tempcfg);
  return;
}
