/* $Id: detmon_utils.h,v 1.5 2013-07-05 12:51:21 jtaylor Exp $
 *
 * This file is part of the IIINSTRUMENT Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-07-05 12:51:21 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_DETMON_UTILS_H
#define XSH_DETMON_UTILS_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

const char * xsh_detmon_get_license(void) ;

cpl_imagelist *
xsh_detmon_load_frameset_window(const cpl_frameset * fset, cpl_type type,
                            cpl_size pnum, cpl_size xtnum,
                            cpl_size llx, cpl_size lly,
                            cpl_size urx, cpl_size ury,
                            cpl_size nx, cpl_size ny);

cpl_image * xsh_detmon_subtract_create_window(const cpl_image * a,
                                          const cpl_image * b,
                                          cpl_size llx,
                                          cpl_size lly,
                                          cpl_size urx,
                                          cpl_size ury);

cpl_image * xsh_detmon_subtracted_avg(const cpl_image * on1,
                                  const cpl_image * off1,
                                  const cpl_image * on2,
                                  const cpl_image * off2,
                                  cpl_size llx, cpl_size lly,
                                  cpl_size urx, cpl_size ury);

#endif
