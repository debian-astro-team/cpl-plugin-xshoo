/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2001-2018 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef XSH_MOLECFIT_MODEL_H
#define XSH_MOLECFIT_MODEL_H
/*----------------------------------------------------------------------------*/
/**
 *                              Includes
 */
/*----------------------------------------------------------------------------*/

/* Include both telluriccorr *and* our extra wrapper codes, since
   we deliberately don't want to have them (pre-included) in telluriccorr.h 
   to ensure telluriccorr is still comptabile with molecfit_model
*/

#include <string.h>
#include <math.h>

#include <cpl.h>

//#include <telluriccorr.h>
#include "mf_wrap.h"
#include "mf_wrap_config.h"
#include "mf_spectrum.h"
#include "xsh_molecfit_utils.h"

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Enumeration types
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Defines
 */
/*----------------------------------------------------------------------------*/


#define XSHOOTER_INPUT_DATA_FLUX_COLUMN "Flux"

//descriptions for the input parameters...
//by default grab the DESC already defined in molecfit_config or telluriccorr
//in case we want to define our own xshooter specific help
#define XSH_MOLECFIT_PARAMETER_LIST_DESC                "List of molecules to include in the fit (comma separated). If set to 'NULL', the values provided in MOLECULES_XXX will be used, where XXX is UVB, VIS or NIR. If not, the provided list overrides what is specified in the MOLECULES_XXX input. Note: in order to override MOLECULES_XXX, the recipe parameters REL_COL, FIT_MOLEC, and LIST_MOLEC must be all specified and different from 'NULL'."
//#define XSH_MOLECFIT_PARAMETER_LIST_DESC                MOLECFIT_PARAMETER_LIST_DESC

#define XSH_MOLECFIT_PARAMETER_FIT_DESC                 "List of flags (1 and 0), comma separated, that specifies if a molecule has to be fitted (flag=1) or computed (flag=0). If set to 'NULL', the values provided in MOLECULES_XXX will be used, where XXX is UVB, VIS or NIR. If not, the provided list overrides what is specified in the MOLECULES_XXX input. Note: in order to override MOLECULES_XXX, the following recipe parameters rel_col, fit_molec, and list_molec must be all specified and different from 'NULL'."
//#define XSH_MOLECFIT_PARAMETER_FIT_DESC                 MOLECFIT_PARAMETER_FIT_DESC

#define XSH_MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC      "List of relative column densities of the molecules used (comma separated). If set to 'NULL', the values provided in MOLECULES_XXX will be used, where XXX is UVB, VIS or NIR. If not, the provided list overrides what is specified in the MOLECULES_XXX input. Note: in order to override MOLECULES_XXX, the following recipe parameters rel_col, fit_molec, and list_molec must be all specified and different from 'NULL'." 
//#define XSH_MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC      MOLECFIT_PARAMETER_RELATIVE_VALUE_DESC

#define XSH_MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC  "Comma separated list of values that define the boundaries of the wavelength regions to fit (low_1, upper_1, low_2, upper_2,...,low_n, upper_n), in microns. If set to 'NULL' the values specified in the fits table given by the tag WAVE_INCLUDE_XXX will be used, where XXX is UVB, VIS or NIR. If not, these values override those specified in WAVE_INCLUDE_XXX."
//#define XSH_MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC  MOLECFIT_PARAMETER_WAVE_RANGE_INCLUDE_DESC

#define XSH_MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC  "Comma separated list of values that define the boundaries of the wavelength regions to exclude in the fit (low_1, upper_1, low_2, upper_2,...,low_n, upper_n), in microns. If set to 'NULL' the values specified in the fits table given by the tag WAVE_EXCLUDE_XXX will be used, where XXX is UVB, VIS or NIR. If not, these values override those specified in WAVE_EXCLUDE_XXX."
//#define XSH_MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC  MOLECFIT_PARAMETER_WAVE_RANGE_EXCLUDE_DESC

#define XSH_MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC "Comma separated list of values that define the boundaries of the pixel regions to exclude in the fit (low_1, upper_1, low_2, upper_2,...,low_n, upper_n), in pixel units. If set to 'NULL' the values specified in the fits table given by the tag PIXEL_EXCLUDE_XXX will be used, where XXX is UVB, VIS or NIR. If not, these values override those specified in PIXEL_EXCLUDE_XXX."
//#define XSH_MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC MOLECFIT_PARAMETER_PIXEL_RANGE_EXCLUDE_DESC

#define XSH_MF_PARAMETERS_COLUMN_LAMBDA_DESC            "Name of the column in the input that identifies the wavelength. Note: this parameter is relevant only for inputs in binary table format." 
//#define XSH_MF_PARAMETERS_COLUMN_LAMBDA_DESC            MF_PARAMETERS_COLUMN_LAMBDA_DESC

#define XSH_MF_PARAMETERS_COLUMN_FLUX_DESC              "Name of the column in the input that identifies the flux. Note: this parameter is relevant only for inputs in binary table format." 
//#define XSH_MF_PARAMETERS_COLUMN_FLUX_DESC              MF_PARAMETERS_COLUMN_FLUX_DESC

#define XSH_MF_PARAMETERS_COLUMN_DFLUX_DESC             "Name of the column in the input that identifies the flux error. Note: this parameter is relevant only for inputs in binary table format." 
//#define XSH_MF_PARAMETERS_COLUMN_DFLUX_DESC             MF_PARAMETERS_COLUMN_DFLUX_DESC

#define XSH_MF_PARAMETERS_DEFAULT_ERROR_DESC            "Default error relative to mean for the case that the error column is not provided." 
//#define XSH_MF_PARAMETERS_DEFAULT_ERROR_DESC            MF_PARAMETERS_DEFAULT_ERROR_DESC

#define XSH_MF_PARAMETERS_FTOL_DESC                     "Relative chi2 convergence criterion."
//#define XSH_MF_PARAMETERS_FTOL_DESC                     MF_PARAMETERS_FTOL_DESC

#define XSH_MF_PARAMETERS_XTOL_DESC                     "Relative parameter convergence criterion." 
//#define XSH_MF_PARAMETERS_XTOL_DESC                     MF_PARAMETERS_XTOL_DESC

#define XSH_MF_PARAMETERS_FIT_CONTINUUM_DESC            "Comma delimited string of flags (1=true, 0=false) for fitting the continuum in specific regions. The number of values must match the number of wavelength ranges to fit. If one single value is given, then it is assumed to be valid for all the wavelength ranges. If set to 'NULL', then the values are taken from the column CONT_POLY_ORDER of the input table WAVE_INCLUDE. If this file is not given, then the default is used." 
//#define XSH_MF_PARAMETERS_FIT_CONTINUUM_DESC            MF_PARAMETERS_FIT_CONTINUUM_DESC

#define XSH_MF_PARAMETERS_CONTINUUM_N_DESC              "Polynomial order for the continuum model fit to each wavelength region, presented as a comma delimited string. If a single value is given, then it is assumed to be valid for all the wavelength ranges. If set to 'NULL', then the values are taken from the column CONT_POLY_ORDER of the input table WAVE_INCLUDE. If this file is not given, then the default is used." 
//#define XSH_MF_PARAMETERS_CONTINUUM_N_DESC              MF_PARAMETERS_CONTINUUM_N_DESC

#define XSH_MF_PARAMETERS_FIT_WLC_DESC                  "A list of flags (1 or 0) that specifies which ranges are to be included as a part of the wavelength correction fitting if wavelength fitting has been selected. If a single value is given, then it is assumed to be valid for all the wavelength ranges." 
//#define XSH_MF_PARAMETERS_FIT_WLC_DESC                  MF_PARAMETERS_FIT_WLC_DESC

#define XSH_MF_PARAMETERS_WLC_N_DESC                    "Degree of the polynomial used to refine the wavelength solution." 
//#define XSH_MF_PARAMETERS_WLC_N_DESC                    MF_PARAMETERS_WLC_N_DESC

#define XSH_MF_PARAMETERS_WLC_CONST_DESC                "Initial term for refinement of the wavelength solution."
//#define XSH_MF_PARAMETERS_WLC_CONST_DESC                MF_PARAMETERS_WLC_CONST_DESC

#define XSH_MF_PARAMETERS_FIT_RES_BOX_DESC              "Flag that specifies if the instrumental line spread function is fitted by a Boxcar function." 
//#define XSH_MF_PARAMETERS_FIT_RES_BOX_DESC              MF_PARAMETERS_FIT_RES_BOX_DESC

#define XSH_MF_PARAMETERS_RES_BOX_DESC                  "Initial value in pixels of the Boxcar function width that fits the instrumental line spread function (only used if --FIT_RES_BOX=TRUE)." 
//#define XSH_MF_PARAMETERS_RES_BOX_DESC                  MF_PARAMETERS_RES_BOX_DESC

#define XSH_MF_PARAMETERS_FIT_GAUSS_DESC                "Flag that specifies if the instrumental line spread function is fitted by a Gaussian function." 
//#define XSH_MF_PARAMETERS_FIT_GAUSS_DESC                MF_PARAMETERS_FIT_GAUSS_DESC

#define XSH_MF_PARAMETERS_RES_GAUSS_DESC                "Inital value in pixels of the FWHM of the Gaussian function that fits the instrumental line spread function (only used if --FIT_RES_GAUSS=TRUE)." 
//#define XSH_MF_PARAMETERS_RES_GAUSS_DESC                MF_PARAMETERS_RES_GAUSS_DESC

#define XSH_MF_PARAMETERS_FIT_LORENTZ_DESC              "Flag that specifies if the instrumental line spread function is fitted by a Lorentzian function." 
//#define XSH_MF_PARAMETERS_FIT_LORENTZ_DESC              MF_PARAMETERS_FIT_LORENTZ_DESC

#define XSH_MF_PARAMETERS_RES_LORENTZ_DESC              "Initial value in pixels of the FWHM of the Lorentz function that fits the instrumental line spread function (only used if --FIT_RES_LORENTZ=TRUE)." 
//#define XSH_MF_PARAMETERS_RES_LORENTZ_DESC              MF_PARAMETERS_RES_LORENTZ_DESC

#define XSH_MF_PARAMETERS_KERN_MODE_DESC                "Flag that indicates whether to use a Voigt approximation instead of the Lorentz or Gauss functions to fit the instrumental line spread function." 
//#define XSH_MF_PARAMETERS_KERN_MODE_DESC                MF_PARAMETERS_KERN_MODE_DESC

#define XSH_MF_PARAMETERS_KERN_FAC_DESC                 "Size of the Gaussian/Lorentzian/Voigtian kernel, expressed in units of FWHM." 
//#define XSH_MF_PARAMETERS_KERN_FAC_DESC                 MF_PARAMETERS_KERN_FAC_DESC

#define XSH_MF_PARAMETERS_VAR_KERN_DESC                 "Flag indicating if the kernel is constant or varies linearly with wavelength (i.e. resolving power is constant)." 
//#define XSH_MF_PARAMETERS_VAR_KERN_DESC                 MF_PARAMETERS_VAR_KERN_DESC

#define XSH_MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC    "If TRUE, use the kernel library if it is provided." 
//#define XSH_MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC    MOLECFIT_PARAMETER_USE_INPUT_KERNEL_DESC

#define XSH_MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC       "Flag that indicates whether the telescope background should be fitted." 
//#define XSH_MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC       MF_PARAMETERS_FIT_TELESCOPE_BACK_DESC

#define XSH_MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC     "Initial value for the telescope background fit." 
//#define XSH_MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC     MF_PARAMETERS_TELESCOPE_BACK_CONST_DESC

#define XSH_MF_PARAMETERS_PWV_DESC                      "Value in mm of the precipitable water vapour for the input water vapor profile. If set to a positive value, then the merged profile composed of ref_atm, GDAS, and local meteorological data will scaled to this value. If negative, then no scaling is done." 
//#define XSH_MF_PARAMETERS_PWV_DESC                      MF_PARAMETERS_PWV_DESC

//other parameters
//#define XSH_MF_LNFL_LINE_DB_DESC                      MF_LNFL_LINE_DB_DESC
#define XSH_MF_LNFL_LINE_DB_DESC                        "AER version in format aer_v_X.X. For example aer_v_3.8 or aer_v_3.6."



/*----------------------------------------------------------------------------*/
/**
 *                 Global variables
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Macros
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 *                 Typedefs: Structured types
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 *                 Functions prototypes
 */
/*----------------------------------------------------------------------------*/

cpl_error_code xsh_molecfit_model_config(cpl_frameset *frameset,
		const cpl_parameterlist  *parlist,
		cpl_parameterlist* ilist,
		cpl_parameterlist* iframelist);

cpl_error_code xsh_molecfit_model_spec_header_calcs(const char* fname,const char* arm, cpl_parameterlist* ilist);

cpl_error_code xsh_molecfit_model_spec_data_calcs(mf_wrap_fits* data, const char* is_idp, cpl_parameterlist* ilist,mf_wrap_model_parameter* parameters);

cpl_error_code xsh_molecfit_setup_frameset(cpl_frameset* frameset,cpl_parameterlist* list,const char* arm,const char* input_name);


#endif /*XSH_MOLECFIT_MODEL_H*/
