/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or F1ITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#ifndef XSH_HDRL_FUNCTIONS_H
#define XSH_HDRL_FUNCTIONS_H

/*-----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/
#include <xsh_error.h>
#include <xsh_msg.h>
#include <hdrl.h>
#include <cpl.h>

cpl_frame*
xsh_hdrl_remove_crh_single( cpl_frame* frm,
                       xsh_instrument* instrument,
                       xsh_remove_crh_single_param *  crh_single_par,
                       const char* ftag ) ;


/*----------------------------------------------------------------------------*/


#endif
