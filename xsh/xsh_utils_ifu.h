/*                                                                           a
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */


/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_UTILS_IFU_H
#define XSH_UTILS_IFU_H
/*----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/
#include <cpl.h>
#include <xsh_parameters.h>


void
xsh_edge_check(const int px,const int nx,const int rad_x,
               int* llx,int* urx);
void
xsh_convert_xy_to_ws(double x_centroid, 
		     double* p_obj_cen,
		     double* p_slit,
		     double* p_wave,
		     const int lly,
		     const int nx,
		     const int row,
		     double* p_obj_cen_s,
		     double* p_obj_cen_w);

cpl_error_code
xsh_table_edges_swap_low_upp(cpl_table** tab);

cpl_table*
xsh_table_edge_prepare(const char* name);
cpl_error_code
xsh_ifu_trace_object_calibrate(const char* ifu_object_ff_name,
                               const char* order_tab_edges_ifu_name,
                               const char* slit_map_name,
                               const char* wave_map_name);

cpl_frame*
xsh_build_ifu_map(cpl_frame* div_frame,
                  cpl_frame* wavemap_frame,
                  cpl_frame* slitmap_frame,
                  xsh_instrument* instrument);

cpl_error_code
xsh_build_ifu_cube(cpl_frame* div_frame, 
                   cpl_frame* ifu_cfg_tab_frame,
                   cpl_frame* ifu_cfg_cor_frame,
                   cpl_frame* spectral_format_frame,
                   cpl_frame* model_config_frame,
                   cpl_frame* wavesol_frame,
                   xsh_instrument* instrument,
                   cpl_frameset* frameset,
                   cpl_parameterlist* parameters,
                   xsh_rectify_param * rectify_par, 
                   const char* recipe_id, 
                   const char* rec_prefix,
                   const int frame_is_object);
cpl_error_code 
xsh_frame_check_model_cfg_is_proper_for_sci(cpl_frame* model_config_frame,
                                            cpl_frame* sci_frame,
                                            xsh_instrument* instrument);

cpl_error_code 
xsh_frame_check_model_cfg_is_afc_corrected(cpl_frame* model_config_frame);

cpl_error_code
xsh_cube_set_wcs(cpl_propertylist * plist,
                 float cenLambda,
                 float disp_x,
                 float disp_y,
                 float disp_z,
                 float center_x,
                 float center_y,
                 int   center_z);

cpl_matrix * xsh_atrous( cpl_vector *spec, int nscales); 

cpl_frameset * xsh_shift_offsettab( cpl_frameset *shift_ifu_frameset,
  double offset_low, double offset_up);

#endif
