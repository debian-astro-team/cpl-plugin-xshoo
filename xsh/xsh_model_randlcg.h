/* $Id: xsh_model_randlcg.h,v 1.6 2008-05-23 09:07:49 amodigli Exp $
 *
 * This file is part of the ESO X-shooter Pipeline                          
 * Copyright (C) 2006 European Southern Observatory 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* xsh_randlcg.h	prototypes for the minimal standard random number generator,

   Linear Congruential Method, the "minimal standard generator"
   Park & Miller, 1988, Comm of the ACM, 31(10), pp. 1192-1201


  rcsid: @(#)xsh_randlcg.h	1.1 15:48:09 11/21/94   EFC

*/

#ifndef XSH_MODEL_RANDLCG_H
#define XSH_MODEL_RANDLCG_H 1.1

long get_seed(void);
unsigned long int xsh_randlcg(void);
long xsh_set_seed(long int);

#endif

