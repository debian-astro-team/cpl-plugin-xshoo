/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: rhaigron $
 * $Date: 2010-09-09 13:04:39 $
 * $Revision: 1.18 $
 */


#ifndef XSH_DATA_LINETILT_H
#define XSH_DATA_LINETILT_H 1.2

#include <cpl.h>


#define XSH_LINETILT_TABLE_NB_COL 15

#define XSH_LINETILT_TABLE_COLNAME_WAVELENGTH "WAVELENGTH"
#define XSH_LINETILT_TABLE_UNIT_WAVELENGTH "none"

#define XSH_LINETILT_TABLE_COLNAME_NAME "NAME"
#define XSH_LINETILT_TABLE_UNIT_NAME "none"

#define XSH_LINETILT_TABLE_COLNAME_ORDER "ORDER"
#define XSH_LINETILT_TABLE_UNIT_ORDER "none"

#define XSH_LINETILT_TABLE_COLNAME_FLUX "Flux"
#define XSH_LINETILT_TABLE_UNIT_FLUX "ADU"

#define XSH_LINETILT_TABLE_COLNAME_INTENSITY "Intensity"
#define XSH_LINETILT_TABLE_UNIT_INTENSITY "ADU"

#define XSH_LINETILT_TABLE_COLNAME_CENPOSX "Xcen"
#define XSH_LINETILT_TABLE_UNIT_CENPOSX "pixel"

#define XSH_LINETILT_TABLE_COLNAME_CENPOSY "Ycen"
#define XSH_LINETILT_TABLE_UNIT_CENPOSY "pixel"

#define XSH_LINETILT_TABLE_COLNAME_GAUSSY "YGauss"
#define XSH_LINETILT_TABLE_UNIT_GAUSSY "pixel"

#define XSH_LINETILT_TABLE_COLNAME_TILTY "Ytilt"
#define XSH_LINETILT_TABLE_UNIT_TILTY "pixel"

#define XSH_LINETILT_TABLE_COLNAME_FWHM "FWHM"
#define XSH_LINETILT_TABLE_UNIT_FWHM "pixel"

#define XSH_LINETILT_TABLE_COLNAME_SHIFTY "SHIFT_Y"
#define XSH_LINETILT_TABLE_UNIT_SHIFTY "pixel"

#define XSH_LINETILT_TABLE_COLNAME_TILT "TILT"
#define XSH_LINETILT_TABLE_UNIT_TILT "none"

#define XSH_LINETILT_TABLE_COLNAME_CHISQ "chisq"
#define XSH_LINETILT_TABLE_UNIT_CHISQ "none"

#define XSH_LINETILT_TABLE_COLNAME_SPECRES "SPECRES"
#define XSH_LINETILT_TABLE_UNIT_SPECRES "none"

#define XSH_LINETILT_TABLE_COLNAME_FLAG "FLAG"
#define XSH_LINETILT_TABLE_UNIT_FLAG "none"

typedef struct{
  /* wavelength of arc line */
  float wavelength;
  /* name of arc line*/
  char* name;
  int order ;			/**< Order number */
  double cenposx ;		/**< Central position in X */
  double cenposy ;		/**< Central position in Y */
  double pre_pos_y ;		/**< Previous Y position */
  double tilt_y;
  double shift_y;
  double deltay ;		/**< Difference with input wavesol */
  double slit ;			/**< Slit value of this line */
  int slit_index ;		/**< Slit index */
  double tilt ;			/**< Slope of the line */
  double chisq ;		/**< Xi2 of the fit */
  double xmin,
    xmax ;
  double specres ;		/**< Spectral resolution */
  double area ;             /**< Integrated Flux over fit line width */
  double intensity;
  int ntot ;			/**< Nb of points total */
  int ngood ;			/**< Nb of good points used in fit */
  int flag ;			/**< Flag (bad/good) (0/1) line */
} xsh_linetilt ;


typedef struct{
  int size;
  int full_size ;
  xsh_linetilt ** list;
  cpl_propertylist* header;
}xsh_linetilt_list;

xsh_linetilt_list * xsh_linetilt_list_new( int size,
					   cpl_propertylist * header ) ;
void xsh_linetilt_list_free(xsh_linetilt_list** list);
xsh_linetilt * xsh_linetilt_new(void);
void xsh_linetilt_free( xsh_linetilt ** tilt) ;
void xsh_linetilt_list_add( xsh_linetilt_list * list,
			    xsh_linetilt * line, int idx ) ;
cpl_frame * xsh_linetilt_list_save( xsh_linetilt_list * list,
				    xsh_instrument * instrument,
				    const char * filename,
                                    const char* tag,
                                    const double kappa,
                                    const int niter ) ;

double * xsh_linetilt_list_get_posx( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_posy( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_pre_posy( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_deltay( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_sigma_y( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_orders( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_wavelengths( xsh_linetilt_list * list) ;
double * xsh_linetilt_list_get_slits( xsh_linetilt_list * list) ;
int * xsh_linetilt_list_get_slit_index(  xsh_linetilt_list * list) ;
cpl_propertylist * xsh_linetilt_list_get_header( xsh_linetilt_list * list) ;
int xsh_linetilt_is_duplicate( xsh_linetilt_list * list, float lambda,
			       int order ) ;

#endif  /* XSH_DATA_LINETILT_H */
