/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-04-01 06:24:05 $
 * $Revision: 1.118 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_PFITS_H
#define XSH_PFITS_H

/*----------------------------------------------------------------------------
   								Includes
 ----------------------------------------------------------------------------*/
#include <stdbool.h>
#include <cpl.h>
#include <xsh_pfits_qc.h>
/*----------------------------------------------------------------------------
                               Defines
 ----------------------------------------------------------------------------*/
#define XSH_MJDOBS "MJD-OBS"
#define XSH_MJDEND "MJD-END"
#define XSH_NAXIS "NAXIS"
#define XSH_INSTRUME "INSTRUME"

#define XSH_RA "RA"
#define XSH_DEC "DEC"

#define XSH_OBS_PROG_ID  "ESO OBS PROG ID"
#define XSH_PRO_TECH     "ESO PRO TECH"
#define XSH_SNR          "SNR"
#define XSH_PRO_REC1_PIPE_ID "ESO PRO REC1 PIPE ID"

#define XSH_TPL_EXPNO "ESO TPL EXPNO"
#define XSH_TPL_START "ESO TPL START"
#define XSH_TPL_END "ESO TPL END"

#define XSH_GEOLAT "ESO TEL GEOLAT"
#define XSH_GEOLON "ESO TEL GEOLON"
#define XSH_UTC "UTC"

#define XSH_TELESCOP "TELESCOP"

#define XSH_TEL_TARG_ALPHA "ESO TEL TARG ALPHA"
#define XSH_TEL_TARG_DELTA "ESO TEL TARG DELTA"


#define XSH_CD1 "CD1"

#define XSH_CD11 "CD1_1"
#define XSH_CD12 "CD1_2"
#define XSH_CD21 "CD2_1"
#define XSH_CD22 "CD2_2"


#define XSH_DET_WIN1_UIT1 "ESO DET WIN1 UIT1"


#define XSH_CD13 "CD1_3"
#define XSH_CD31 "CD3_1"
#define XSH_CD23 "CD2_3"
#define XSH_CD32 "CD3_2"
#define XSH_CD33 "CD3_3"

#define XSH_LST "LST"



#define XSH_NAXIS1 "NAXIS1"
#define XSH_NAXIS2 "NAXIS2"
#define XSH_NAXIS3 "NAXIS3"

#define XSH_CRPIX1 "CRPIX1"
#define XSH_CRPIX2 "CRPIX2"
#define XSH_CRPIX3 "CRPIX3"


#define XSH_CDELT1 "CDELT1"
#define XSH_CDELT2 "CDELT2"
#define XSH_CDELT3 "CDELT3"

#define XSH_CRVAL1 "CRVAL1"
#define XSH_CRVAL2 "CRVAL2"
#define XSH_CRVAL3 "CRVAL3"


#define XSH_CTYPE1 "CTYPE1"
#define XSH_CTYPE2 "CTYPE2"
#define XSH_CTYPE3 "CTYPE3"

#define XSH_CUNIT1 "CUNIT1"
#define XSH_CUNIT2 "CUNIT2"
#define XSH_CUNIT3 "CUNIT3"

#define XSH_BUNIT "BUNIT"
#define XSH_BUNIT_FLUX_ABS_C "erg/s/cm2/Angstrom"
#define XSH_BUNIT_FLUX_REL_C "ADU"
#define XSH_BUNIT_NONE_C ""

#define XSH_RAW1_NAME "ESO PRO REC1 RAW1 NAME"
#define XSH_RAW1_CATG "ESO PRO REC1 RAW1 CATG"
#define XSH_ARCFILE "ARCFILE"
#define XSH_ORIGFILE "ORIGFILE"
#define XSH_DATE    "DATE"
#define XSH_DATE_OBS    "DATE-OBS"
#define XSH_EXTNAME "EXTNAME"
#define XSH_EXPTIME "EXPTIME"

#define XSH_OBS_ID    "ESO OBS ID"
#define XSH_OBS_TARG_NAME    "ESO OBS TARG NAME"

#define XSH_AIRM_START  "ESO TEL AIRM START"
#define XSH_AIRM_END    "ESO TEL AIRM END"

#define XSH_SEEING_START "ESO TEL AMBI FWHM START"
#define XSH_SEEING_END "ESO TEL AMBI FWHM END"

#define XSH_SLIT_UVB "ESO INS OPTI3 NAME"
#define XSH_SLIT_VIS "ESO INS OPTI4 NAME"
#define XSH_SLIT_NIR "ESO INS OPTI5 NAME"

#define XSH_FOCU1ENC_VAL  "ESO INS FOCU1 ENC"

#define XSH_TEMP82_VAL  "ESO INS TEMP82 VAL"
#define XSH_TEMP2_VAL  "ESO INS TEMP2 VAL"
#define XSH_TEMP5_VAL  "ESO INS TEMP5 VAL"

#define XSH_DPR_TECH "ESO DPR TECH" 
#define XSH_DPR_TYPE "ESO DPR TYPE" 
#define XSH_DPR_CATG "ESO DPR CATG" 
#define XSH_OUT_NX  "ESO DET OUT1 NX"
#define XSH_OUT_NY  "ESO DET OUT1 NY"
#define XSH_CHIP_NX  "ESO DET CHIP NX"
#define XSH_CHIP_NY  "ESO DET CHIP NY"
#define XSH_WIN_NX  "ESO DET WIN NX"
#define XSH_WIN_NY  "ESO DET WIN NY"
#define XSH_WIN_BINX "ESO DET WIN1 BINX"
#define XSH_WIN_BINY "ESO DET WIN1 BINY"
#define XSH_OVSCX  "ESO DET OUT1 OVSCX"
#define XSH_OVSCY "ESO DET OUT1 OVSCY"
#define XSH_PRSCX "ESO DET OUT1 PRSCX"
#define XSH_PRSCY "ESO DET OUT1 PRSCY"
#define XSH_RON  "ESO DET OUT1 RON"
#define XSH_CONAD "ESO DET OUT1 CONAD"

#define XSH_DET_PXSPACE "ESO DET CHIP PXSPACE"
#define XSH_DET_GAIN "ESO DET OUT1 GAIN"
#define XSH_DET_WIN1_DIT1 "ESO DET WIN1 DIT1"
#define XSH_DET_DIT "ESO DET DIT"
#define XSH_DET_NDIT "ESO DET NDIT"
#define XSH_DET_NDITSKIP "ESO DET NDITSKIP"
#define XSH_PSZX "ESO DET CHIP1 PSZX"
#define XSH_PSZY "ESO DET CHIP1 PSZY"

#define XSH_SEQ_ARM "ESO SEQ ARM"

#define XSH_LAMP_ON_OFF "ESO INS1 LAMP5 ST" // for SINFONI, XSH 'INS LAMP5 ST'

#define XSH_BIAS_LEFT_MEDIAN "ESO PRO BIAS LEFT MEDIAN"
#define XSH_BIAS_RIGHT_MEDIAN "ESO PRO BIAS RIGHT MEDIAN"
#define XSH_BIAS_UP_MEDIAN "ESO PRO BIAS UP MEDIAN"
#define XSH_BIAS_DOWN_MEDIAN "ESO PRO BIAS DOWN MEDIAN"
#define XSH_BIAS_LEFT_STDEV "ESO PRO BIAS LEFT STDEV"
#define XSH_BIAS_RIGHT_STDEV "ESO PRO BIAS RIGHT STDEV"
#define XSH_BIAS_UP_STDEV "ESO PRO BIAS UP STDEV"
#define XSH_BIAS_DOWN_STDEV "ESO PRO BIAS DOWN STDEV"
#define XSH_PCATG "ESO PRO CATG"
#define XSH_WAVESOL_LAMBDA_MIN "ESO PRO WAVESOL LAMBDA MIN"
#define XSH_WAVESOL_LAMBDA_MAX "ESO PRO WAVESOL LAMBDA MAX"
#define XSH_WAVESOL_ORDER_MIN "ESO PRO WAVESOL ORDER MIN"
#define XSH_WAVESOL_ORDER_MAX "ESO PRO WAVESOL ORDER MAX"
#define XSH_WAVESOL_SLIT_MIN "ESO PRO WAVESOL SLIT MIN"
#define XSH_WAVESOL_SLIT_MAX "ESO PRO WAVESOL SLIT MAX"

#define XSH_WAVESOL_X_MIN "ESO PRO WAVESOL X MIN"
#define XSH_WAVESOL_X_MAX "ESO PRO WAVESOL X MAX"
#define XSH_WAVESOL_Y_MIN "ESO PRO WAVESOL Y MIN"
#define XSH_WAVESOL_Y_MAX "ESO PRO WAVESOL Y MAX"

#define XSH_EXTRACT_SLIT_MIN "ESO PRO EXTRACT SLIT MIN"
#define XSH_EXTRACT_SLIT_MAX "ESO PRO EXTRACT SLIT MAX"


#define XSH_RECTIFY_BIN_LAMBDA "ESO PRO RECT BIN LAMBDA"
#define XSH_RECTIFY_BIN_SPACE  "ESO PRO RECT BIN SPACE"
#define XSH_RECTIFY_LAMBDA_MIN "ESO PRO RECT LAMBDA MIN"
#define XSH_RECTIFY_LAMBDA_MAX "ESO PRO RECT LAMBDA MAX"
#define XSH_RECTIFY_SPACE_MIN  "ESO PRO RECT SPACE MIN"
#define XSH_RECTIFY_SPACE_MAX  "ESO PRO RECT SPACE MAX"
#define XSH_DATANCOM "ESO PRO DATANCOM"

#define XSH_SPECTRALFORMAT_DIST_ORDER "ESO PRO DIST ORDER"

#define XSH_WAVESOLTYPE "ESO PRO WAVESOL TYPE"
#define XSH_WAVESOLTYPE_POLYNOMIAL "POLYNOMIAL"
#define XSH_WAVESOLTYPE_MODEL "PHYSMOD"


#define XSH_MODEL_CFG_START "ESO MOD CFG START"

#define XSH_DPR_TECH_SINGLE_PINHOLE "ECHELLE,PINHOLE"
#define XSH_DPR_TECH_MULTI_PINHOLE "ECHELLE,MULTI-PINHOLE" 

#define XSH_NOD_CUMULATIVE_OFFSET_DEC "ESO SEQ CUMOFF DEC"
#define XSH_NOD_CUMULATIVE_OFFSET_RA "ESO SEQ CUMOFF RA"

#define XSH_NOD_CUMULATIVE_OFFSETX "ESO SEQ CUMOFF X"
#define XSH_NOD_CUMULATIVE_OFFSETY "ESO SEQ CUMOFF Y"

#define XSH_NOD_THROW "ESO SEQ NOD THROW"
#define XSH_NOD_JITTER_BOX "ESO SEQ JITTER WIDTH"

#define XSH_NOD_RELATIVE_OFFSET_DEC "ESO SEQ RELOFF DEC"
#define XSH_NOD_RELATIVE_OFFSET_RA "ESO SEQ RELOFF RA"
#define XSH_POSANG "ESO ADA POSANG"


#define XSH_NOD_RELATIVE_B_OFFSET_RA "ESO PRO B RELOFF RA"
#define XSH_NOD_RELATIVE_B_OFFSET_DEC "ESO PRO B RELOFF DEC"
#define XSH_NOD_CUMULATIVE_B_OFFSET_RA "ESO PRO B CUMOFF RA"
#define XSH_NOD_CUMULATIVE_B_OFFSET_DEC "ESO PRO B CUMOFF DEC"
#define XSH_QC_NPIXSAT "ESO QC NPIXSAT"
#define XSH_QC_NPIXSAT_C "Number of saturated pixels"
#define XSH_QC_FPIXSAT "ESO QC FPIXSAT"
#define XSH_QC_FPIXSAT_C "Fraction of saturated pixels"


#define XSH_QC_NPIXSAT_TOT "ESO QC NPIXSAT TOT"
#define XSH_QC_NPIXSAT_TOT_C "Total number of saturated pixels"
#define XSH_QC_FPIXSAT_TOT "ESO QC FPIXSAT TOT"
#define XSH_QC_FPIXSAT_TOT_C "Fraction of saturated pixels"


#define XSH_QC_NPIXRANGE "ESO QC NPIXRANG"
#define XSH_QC_NPIXRANGE_C "Number of pixels in range 4800-5200 ADU"
#define XSH_QC_FPIXRANGE "ESO QC FPIXRANG"
#define XSH_QC_FPIXRANGE_C "Frac of pix in range 4800-5200 ADU"


#define XSH_SLITMAP_ORDER_EDGUP "ESO PRO ORD%d EDGUP SLIT"
#define XSH_SLITMAP_ORDER_EDGLO "ESO PRO ORD%d EDGLO SLIT"
#define XSH_SLITMAP_ORDER_CEN "ESO PRO ORD%d CEN SLIT"
#define XSH_SLITMAP_ORDER_SLICUP "ESO PRO ORD%d SLICUP SLIT"
#define XSH_SLITMAP_ORDER_SLICLO "ESO PRO ORD%d SLICLO SLIT"

#define XSH_SLITMAP_MEDIAN_EDGUP "ESO PRO MEDIAN EDGUP SLIT"
#define XSH_SLITMAP_MEDIAN_EDGLO "ESO PRO MEDIAN EDGLO SLIT"
#define XSH_SLITMAP_MEDIAN_CEN "ESO PRO MEDIAN CEN SLIT"
#define XSH_SLITMAP_MEDIAN_SLICUP "ESO PRO MEDIAN SLICUP SLIT"
#define XSH_SLITMAP_MEDIAN_SLICLO "ESO PRO MEDIAN SLICLO SLIT"

#define XSH_WAVEMAP_ORDER_LAMBDA_MIN "ESO PRO ORD%d LAMBDA MIN"
#define XSH_WAVEMAP_ORDER_LAMBDA_MAX "ESO PRO ORD%d LAMBDA MAX"

#define XSH_SHIFTIFU_WAVEREF "ESO PRO LAMBDA REF"
#define XSH_SHIFTIFU_SLITREF "ESO PRO SLIT REF"
#define XSH_SHIFTIFU_SLITDOWNREF "ESO PRO SLIT DOWN REF"
#define XSH_SHIFTIFU_SLITCENREF "ESO PRO SLIT CEN REF"
#define XSH_SHIFTIFU_SLITUPREF "ESO PRO SLIT UP REF"

#define XSH_INSTRUME_VALUE                  "XSHOOTER"

/* Keywords related to the Science Data Product format. */
#define XSH_SDP_KEYWORD_PROCSOFT            "PROCSOFT"
#define XSH_SDP_KEYWORD_PRODLVL             "PRODLVL"
#define XSH_SDP_KEYWORD_PRODLVL_VALUE       2
#define XSH_SDP_KEYWORD_OBJECT              "OBJECT"
#define XSH_SDP_KEYWORD_PRODCATG            "PRODCATG"
#define XSH_SDP_KEYWORD_ORIGIN              "ORIGIN"
#define XSH_SDP_KEYWORD_ORIGIN_VALUE        "ESO"
#define XSH_SDP_KEYWORD_DISPELEM            "DISPELEM"
#define XSH_SDP_KEYWORD_SPECSYS             "SPECSYS"
#define XSH_SDP_KEYWORD_SPECSYS_VALUE       "TOPOCENT"
#define XSH_SDP_KEYWORD_VOCLASS             "VOCLASS"
#define XSH_SDP_KEYWORD_VOCLASS_VALUE       "SPECTRUM V2.0"
#define XSH_SDP_KEYWORD_VOPUB               "VOPUB"
#define XSH_SDP_KEYWORD_VOPUB_VALUE         "ESO/SAF"
#define XSH_SDP_KEYWORD_TITLE               "TITLE"
#define XSH_SDP_KEYWORD_APERTURE            "APERTURE"
#define XSH_SDP_KEYWORD_TELAPSE             "TELAPSE"
#define XSH_SDP_KEYWORD_TMID                "TMID"
#define XSH_SDP_KEYWORD_TUTYP(n)            "TUTYP"#n
#define XSH_SDP_KEYWORD_TUCD(n)             "TUCD"#n
#define XSH_SDP_KEYWORD_TDMIN(n)            "TDMIN"#n
#define XSH_SDP_KEYWORD_TDMAX(n)            "TDMAX"#n
#define XSH_SDP_KEYWORD_NELEM               "NELEM"
#define XSH_SDP_KEYWORD_EXTNAME             "EXTNAME"
#define XSH_SDP_KEYWORD_EXTNAME_VALUE       "SPECTRUM"
#define XSH_SDP_KEYWORD_INHERIT             "INHERIT"
#define XSH_SDP_KEYWORD_INHERIT_VALUE       CPL_TRUE
#define XSH_SDP_KEYWORD_SPEC_VAL            "SPEC_VAL"
#define XSH_SDP_KEYWORD_SPEC_BW             "SPEC_BW"
#define XSH_SDP_KEYWORD_PROG_ID             "PROG_ID"
#define XSH_SDP_KEYWORD_OBID1               "OBID1"
#define XSH_SDP_KEYWORD_M_EPOCH             "M_EPOCH"
#define XSH_SDP_KEYWORD_OBSTECH             "OBSTECH"
#define XSH_SDP_KEYWORD_FLUXCAL             "FLUXCAL"
#define XSH_SDP_KEYWORD_CONTNORM            "CONTNORM"
#define XSH_SDP_KEYWORD_WAVELMIN            "WAVELMIN"
#define XSH_SDP_KEYWORD_WAVELMAX            "WAVELMAX"
#define XSH_SDP_KEYWORD_SPEC_BIN            "SPEC_BIN"
#define XSH_SDP_KEYWORD_TOT_FLUX            "TOT_FLUX"
#define XSH_SDP_KEYWORD_FLUXERR             "FLUXERR"
#define XSH_SDP_KEYWORD_FLUXERR_VALUE       -2
#define XSH_SDP_KEYWORD_REFERENC            "REFERENC"
#define XSH_SDP_KEYWORD_REFERENC_VALUE      " "
#define XSH_SDP_KEYWORD_SPEC_RES            "SPEC_RES"
#define XSH_SDP_KEYWORD_SPEC_ERR            "SPEC_ERR"
#define XSH_SDP_KEYWORD_SPEC_SYE            "SPEC_SYE"
#define XSH_SDP_KEYWORD_LAMNLIN             "LAMNLIN"
#define XSH_SDP_KEYWORD_LAMRMS              "LAMRMS"
#define XSH_SDP_KEYWORD_GAIN                "GAIN"
#define XSH_SDP_KEYWORD_DETRON              "DETRON"
#define XSH_SDP_KEYWORD_EFFRON              "EFFRON"
#define XSH_SDP_KEYWORD_PROV(n)             "PROV"#n
#define XSH_SDP_KEYWORD_NCOMBINE            "NCOMBINE"
#define XSH_SDP_ADU_UNIT                    "adu"
#define XSH_SDP_FLUX_UNIT                   "erg.cm**(-2).s**(-1).angstrom**(-1)"
#define XSH_SDP_COLUMN_WAVE                 "WAVE"
#define XSH_SDP_COLUMN_WAVE_UNIT            "nm"
#define XSH_SDP_COLUMN_WAVE_FORMAT          "1E"
#define XSH_SDP_COLUMN_WAVE_TYPE            "spec:Data.SpectralAxis.Value"
#define XSH_SDP_COLUMN_WAVE_UCD             "em.wl;obs.atmos"
#define XSH_SDP_COLUMN_FLUX                 "FLUX"
#define XSH_SDP_COLUMN_FLUX_UNIT            XSH_SDP_ADU_UNIT
#define XSH_SDP_COLUMN_FLUX_UNIT_CALIB      XSH_SDP_FLUX_UNIT
#define XSH_SDP_COLUMN_FLUX_FORMAT          "1E"
#define XSH_SDP_COLUMN_FLUX_TYPE            "spec:Data.FluxAxis.Value"
#define XSH_SDP_COLUMN_FLUX_UCD             "phot.flux.density;em.wl;src.net;meta.main"
#define XSH_SDP_COLUMN_ERR                  "ERR"
#define XSH_SDP_COLUMN_ERR_UNIT             XSH_SDP_ADU_UNIT
#define XSH_SDP_COLUMN_ERR_UNIT_CALIB       XSH_SDP_FLUX_UNIT
#define XSH_SDP_COLUMN_ERR_FORMAT           "1E"
#define XSH_SDP_COLUMN_ERR_TYPE             "spec:Data.FluxAxis.Accuracy.StatError"
#define XSH_SDP_COLUMN_ERR_UCD              "stat.error;phot.flux.density;meta.main"
#define XSH_SDP_COLUMN_QUAL                 "QUAL"
#define XSH_SDP_COLUMN_QUAL_UNIT            " "
#define XSH_SDP_COLUMN_QUAL_FORMAT          "1J"
#define XSH_SDP_COLUMN_QUAL_TYPE            "spec:Data.FluxAxis.Accuracy.QualityStatus"
#define XSH_SDP_COLUMN_QUAL_UCD             "meta.code.qual;meta.main"
#define XSH_SDP_COLUMN_SNR                  "SNR"
#define XSH_SDP_COLUMN_SNR_UNIT             " "
#define XSH_SDP_COLUMN_SNR_FORMAT           "1E"
#define XSH_SDP_COLUMN_SNR_TYPE             "eso:Data.FluxAxis.Accuracy.SNR"
#define XSH_SDP_COLUMN_SNR_UCD              "stat.snr;meta.main"
#define XSH_SDP_COLUMN_FLUX_REDUCED         "FLUX_REDUCED"
#define XSH_SDP_COLUMN_FLUX_REDUCED_FORMAT  "1E"
#define XSH_SDP_COLUMN_FLUX_REDUCED_TYPE    "eso:Data.FluxAxis.Value"
#define XSH_SDP_COLUMN_FLUX_REDUCED_UCD     "phot.flux.density;em.wl;src.net;stat.uncalib"
#define XSH_SDP_COLUMN_ERR_REDUCED          "ERR_REDUCED"
#define XSH_SDP_COLUMN_ERR_REDUCED_FORMAT   "1E"
#define XSH_SDP_COLUMN_ERR_REDUCED_TYPE     "eso:Data.FluxAxis.Accuracy.StatError"
#define XSH_SDP_COLUMN_ERR_REDUCED_UCD      "stat.error;phot.flux.density;stat.uncalib"

/*----------------------------------------------------------------------------
 * Macros
 ----------------------------------------------------------------------------*/
/* Get the value of a keyword */
#define XSH_PFITS_GET(RET, LIST, KW, TYPE) \
  check_msg( xsh_get_property_value( LIST, KW, TYPE, &RET),\
             "Error reading keyword '%s'", KW)

/*----------------------------------------------------------------------------
 * Functions prototypes
 ----------------------------------------------------------------------------*/

double xsh_pfits_get_pixscale(const cpl_propertylist * plist);
double xsh_pfits_get_posangle(const cpl_propertylist * plist);
int xsh_pfits_get_FOCU1ENC (const cpl_propertylist * plist);
double xsh_pfits_get_temp82 (const cpl_propertylist * plist);
double xsh_pfits_get_temp2 (const cpl_propertylist * plist);
double xsh_pfits_get_temp5 (const cpl_propertylist * plist);
const char* xsh_pfits_get_dpr_tech(const cpl_propertylist * plist);
const char* xsh_pfits_get_dpr_type(const cpl_propertylist * plist);
const char* xsh_pfits_get_dpr_catg(const cpl_propertylist * plist);
const char* xsh_pfits_get_bunit(const cpl_propertylist * plist);
const char* xsh_pfits_get_cunit1(const cpl_propertylist * plist);
const char* xsh_pfits_get_cunit2(const cpl_propertylist * plist);
const char* xsh_pfits_get_cunit3(const cpl_propertylist * plist);
double xsh_pfits_get_mjdobs(const cpl_propertylist * plist);
int xsh_pfits_get_naxis(const cpl_propertylist * plist);
int xsh_pfits_get_naxis1(const cpl_propertylist * plist);
int xsh_pfits_get_naxis2(const cpl_propertylist * plist);
int xsh_pfits_get_naxis3(const cpl_propertylist * plist);
int xsh_pfits_get_binx(const cpl_propertylist * plist);
int xsh_pfits_get_biny(const cpl_propertylist * plist);
int xsh_pfits_get_out_nx(const cpl_propertylist * plist);
int xsh_pfits_get_chip_ny(const cpl_propertylist * plist);
int xsh_pfits_get_chip_nx(const cpl_propertylist * plist);
int xsh_pfits_get_out_ny(const cpl_propertylist * plist);
int xsh_pfits_get_ovscx(const cpl_propertylist * plist);
int xsh_pfits_get_ovscy(const cpl_propertylist * plist);
int xsh_pfits_get_prscx(const cpl_propertylist * plist);
int xsh_pfits_get_prscy(const cpl_propertylist * plist);
double xsh_pfits_get_gain(const cpl_propertylist * plist);
double xsh_pfits_get_ron(const cpl_propertylist * plist);
double xsh_pfits_get_lst (const cpl_propertylist * plist);
double xsh_pfits_get_conad(const cpl_propertylist * plist);
double xsh_pfits_get_det_gain(const cpl_propertylist * plist);
void xsh_pfits_set_dit(cpl_propertylist *, double dit) ;
void xsh_pfits_set_ndit(cpl_propertylist *, int ndit) ;
double xsh_pfits_get_dit(const cpl_propertylist *) ;
int xsh_pfits_get_ndit(const cpl_propertylist *) ;
double xsh_pfits_get_win1_dit1 (const cpl_propertylist * plist);
const char * xsh_pfits_get_date(const cpl_propertylist *) ;
const char * xsh_pfits_get_date_obs(const cpl_propertylist *) ;
const char * xsh_pfits_get_arcfile(const cpl_propertylist *) ;
const char * xsh_pfits_get_pcatg(const cpl_propertylist *) ;
double xsh_pfits_get_crval1( const cpl_propertylist *) ;
double xsh_pfits_get_crval2( const cpl_propertylist *) ;
double xsh_pfits_get_cdelt1( const cpl_propertylist *) ;
double xsh_pfits_get_cdelt2( const cpl_propertylist *) ;
double xsh_pfits_get_cdelt3( const cpl_propertylist *) ;
double xsh_pfits_get_crpix1(const cpl_propertylist * plist);
double xsh_pfits_get_crpix2(const cpl_propertylist * plist);
double xsh_pfits_get_crpix3(const cpl_propertylist * plist);
double xsh_pfits_get_crval3(const cpl_propertylist * plist);

double xsh_pfits_get_exptime( const cpl_propertylist *) ;
double xsh_pfits_get_det_win1_uit1( const cpl_propertylist *) ;
double xsh_pfits_get_pszx(const cpl_propertylist * plist) ;
double xsh_pfits_get_pszy(const cpl_propertylist * plist) ;
double xsh_pfits_get_det_pxspace(const cpl_propertylist * plist);
double xsh_pfits_get_bias_left_median(cpl_propertylist * plist);
double xsh_pfits_get_bias_right_median(cpl_propertylist * plist);
double xsh_pfits_get_bias_left_stdev(cpl_propertylist * plist);
double xsh_pfits_get_bias_right_stdev(cpl_propertylist * plist);
double xsh_pfits_get_bias_up_median(cpl_propertylist * plist);
double xsh_pfits_get_bias_down_median(cpl_propertylist * plist);
double xsh_pfits_get_bias_up_stdev(cpl_propertylist * plist);
double xsh_pfits_get_bias_down_stdev(cpl_propertylist * plist);

double xsh_pfits_get_ins_targ_alpha(const cpl_propertylist * plist);
double xsh_pfits_get_ins_targ_delta(const cpl_propertylist * plist);

double xsh_pfits_get_ra(const cpl_propertylist * plist);
double xsh_pfits_get_dec(const cpl_propertylist * plist);

const char * xsh_pfits_get_extname(const cpl_propertylist *) ;
bool xsh_pfits_get_lamp_on_off( const cpl_propertylist *) ;
int xsh_pfits_get_datancom( const cpl_propertylist *);
int xsh_pfits_get_nb_pinhole( const cpl_propertylist *);
double xsh_pfits_get_cd11 (const cpl_propertylist * plist);
double xsh_pfits_get_cd12 (const cpl_propertylist * plist);
double xsh_pfits_get_cd21 (const cpl_propertylist * plist);
double xsh_pfits_get_cd22 (const cpl_propertylist * plist);
double xsh_pfits_get_cd13 (const cpl_propertylist * plist);
double xsh_pfits_get_cd23 (const cpl_propertylist * plist);
double xsh_pfits_get_cd33 (const cpl_propertylist * plist);
double xsh_pfits_get_cd31 (const cpl_propertylist * plist);
double xsh_pfits_get_cd32 (const cpl_propertylist * plist);
double xsh_pfits_get_tel_targ_alpha( const cpl_propertylist * plist);
double xsh_pfits_get_tel_targ_delta( const cpl_propertylist * plist);


void xsh_pfits_set_cd1(cpl_propertylist * plist, double cd1);

void xsh_pfits_set_cd11(cpl_propertylist * plist, double cd11);
void xsh_pfits_set_cd12(cpl_propertylist * plist, double cd12);
void xsh_pfits_set_cd21(cpl_propertylist * plist, double cd21);
void xsh_pfits_set_cd22(cpl_propertylist * plist, double cd22);


void xsh_pfits_set_cd13(cpl_propertylist * plist, double cd23);
void xsh_pfits_set_cd23(cpl_propertylist * plist, double cd23);
void xsh_pfits_set_cd31(cpl_propertylist * plist, double cd31);
void xsh_pfits_set_cd32(cpl_propertylist * plist, double cd32);
void xsh_pfits_set_cd33(cpl_propertylist * plist, double cd33);


void xsh_pfits_set_crpix1(cpl_propertylist * plist, double crpix1);
void xsh_pfits_set_crpix2(cpl_propertylist * plist, double crpix2);
void xsh_pfits_set_crpix3(cpl_propertylist * plist, double crpix3);

void xsh_pfits_set_crval1(cpl_propertylist * plist, double crval1);
void xsh_pfits_set_crval2(cpl_propertylist * plist, double crval2);
void xsh_pfits_set_crval3(cpl_propertylist * plist, double crval3);

void xsh_pfits_set_cdelt1(cpl_propertylist * plist, double cdelt1);
void xsh_pfits_set_cdelt2(cpl_propertylist * plist, double cdelt2);
void xsh_pfits_set_cdelt3(cpl_propertylist * plist, double cdelt3);

void xsh_pfits_set_ctype1(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_ctype2(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_ctype3(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_bunit(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_cunit1(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_cunit2(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_cunit3(cpl_propertylist * plist, const char* value);

void xsh_pfits_set_exptime(cpl_propertylist * plist, double exptime);
void xsh_pfits_set_extname(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_pcatg(cpl_propertylist * plist, const char* value);
void xsh_pfits_set_dpr_type (cpl_propertylist * plist, const char *value);
void xsh_pfits_set_dpr_tech (cpl_propertylist * plist, const char *value);
void xsh_pfits_set_dpr_catg (cpl_propertylist * plist, const char *value);

void xsh_pfits_set_bias_left_median(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_right_median(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_up_median(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_down_median(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_left_stdev(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_right_stdev(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_up_stdev(cpl_propertylist * plist, double value);
void xsh_pfits_set_bias_down_stdev(cpl_propertylist * plist, double value);
void xsh_pfits_set_datancom( cpl_propertylist *plist, int value);

void xsh_pfits_set_wavesol_lambda_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_lambda_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_order_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_order_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_slit_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_slit_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_x_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_x_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_y_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_wavesol_y_max(cpl_propertylist * plist, double value);
double xsh_pfits_get_wavesol_lambda_min(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_lambda_max(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_order_min(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_order_max(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_slit_min(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_slit_max(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_x_min(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_x_max(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_y_min(cpl_propertylist * plist);
double xsh_pfits_get_wavesol_y_max(cpl_propertylist * plist);

double xsh_pfits_get_extract_slit_min(cpl_propertylist * plist);
double xsh_pfits_get_extract_slit_max(cpl_propertylist * plist);

int xsh_pfits_get_tpl_expno(const cpl_propertylist * plist);
double xsh_pfits_get_tpl_start(const cpl_propertylist * plist);
double xsh_pfits_get_tpl_end(const cpl_propertylist * plist);

double xsh_pfits_get_airm_end (const cpl_propertylist * plist);
double xsh_pfits_get_airm_start (const cpl_propertylist * plist);
double xsh_pfits_get_airm_mean (const cpl_propertylist * plist);

double xsh_pfits_get_seeing_start (const cpl_propertylist * plist) ;
double xsh_pfits_get_seeing_end (const cpl_propertylist * plist) ;

double xsh_pfits_get_slit_width (const cpl_propertylist * plist,
				 xsh_instrument * instrument ) ;

void xsh_pfits_set_rectify_bin_lambda(cpl_propertylist * plist, double value);
void xsh_pfits_set_rectify_bin_space(cpl_propertylist * plist, double value);
void xsh_pfits_set_rectify_lambda_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_rectify_lambda_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_rectify_space_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_rectify_space_max(cpl_propertylist * plist, double value);

double xsh_pfits_get_rectify_bin_lambda(cpl_propertylist * plist);
double xsh_pfits_get_rectify_bin_space(cpl_propertylist * plist);
double xsh_pfits_get_rectify_lambda_min(cpl_propertylist * plist);
double xsh_pfits_get_rectify_lambda_max(cpl_propertylist * plist);
double xsh_pfits_get_rectify_space_min(cpl_propertylist * plist);
double xsh_pfits_get_rectify_space_max(cpl_propertylist * plist);


void xsh_pfits_set_b_ra_reloffset(cpl_propertylist * plist, double value);
void xsh_pfits_set_b_dec_reloffset(cpl_propertylist * plist, double value);
void xsh_pfits_set_b_ra_cumoffset(cpl_propertylist * plist, double value);
void xsh_pfits_set_b_dec_cumoffset(cpl_propertylist * plist, double value);

double xsh_pfits_get_cumoffsety( const cpl_propertylist *plist);
double xsh_pfits_get_cumoffsetx( const cpl_propertylist *plist);
double xsh_pfits_get_posang(  const cpl_propertylist *plist);
double xsh_pfits_get_ra_cumoffset( const cpl_propertylist *plist);
double xsh_pfits_get_dec_cumoffset( const cpl_propertylist *plist);
double xsh_pfits_get_ra_reloffset( const cpl_propertylist *plist);
double xsh_pfits_get_dec_reloffset( const cpl_propertylist *plist);
double xsh_pfits_get_b_ra_reloffset( const cpl_propertylist *plist);
double xsh_pfits_get_b_dec_reloffset( const cpl_propertylist *plist);
double xsh_pfits_get_b_ra_cumoffset( const cpl_propertylist *plist);
double xsh_pfits_get_b_dec_cumoffset( const cpl_propertylist *plist);

double xsh_pfits_get_nod_reloffset( const cpl_propertylist * plist ) ;
double xsh_pfits_get_nod_cumoffset( const cpl_propertylist * plist ) ;
double xsh_pfits_get_nod_jitterwidth( const cpl_propertylist * plist) ;
double xsh_pfits_get_nodthrow( const cpl_propertylist * plist) ;

void xsh_pfits_set_wavesoltype( cpl_propertylist * plist, const char* value);
const char* xsh_pfits_get_wavesoltype(const cpl_propertylist *);

cpl_error_code
xsh_frame_force_pro_catg(const char* fname, 
                         const char* tag);


void xsh_pfits_set_arm( cpl_propertylist * plist, xsh_instrument* instr);

XSH_ARM xsh_pfits_get_arm( const cpl_propertylist * plist) ;
void xsh_pfits_set_extract_slit_min(cpl_propertylist * plist, double value);
void xsh_pfits_set_extract_slit_max(cpl_propertylist * plist, double value);
void xsh_pfits_set_nsat(cpl_propertylist * plist, int value);
void xsh_pfits_set_frac_sat(cpl_propertylist * plist, double value);
void xsh_pfits_set_total_nsat(cpl_propertylist * plist, int value);
void xsh_pfits_set_total_frac_sat(cpl_propertylist * plist, double value);
void xsh_pfits_set_n_range_pix(cpl_propertylist * plist, int value);
void xsh_pfits_set_frac_range_pix(cpl_propertylist * plist, double value);
char* xsh_pfits_get_slit_value (const cpl_propertylist * plist,xsh_instrument * instrument );

int xsh_pfits_get_obs_id(cpl_propertylist * plist);
const char* xsh_pfits_get_raw1name (const cpl_propertylist * plist);
const char* xsh_pfits_get_raw1catg (const cpl_propertylist * plist);
const char* xsh_pfits_get_obs_targ_name (const cpl_propertylist * plist);

void xsh_pfits_set_slitmap_order_edgup( cpl_propertylist * plist, int absorder, double value);
void xsh_pfits_set_slitmap_order_edglo( cpl_propertylist * plist, int absorder, double value);
void xsh_pfits_set_slitmap_order_cen( cpl_propertylist * plist, int absorder, double value);
void xsh_pfits_set_slitmap_order_slicup( cpl_propertylist * plist, int absorder, double value);
void xsh_pfits_set_slitmap_order_sliclo( cpl_propertylist * plist, int absorder, double value);

void xsh_pfits_set_slitmap_median_edgup( cpl_propertylist * plist, double value);
void xsh_pfits_set_slitmap_median_edglo( cpl_propertylist * plist, double value);
void xsh_pfits_set_slitmap_median_cen( cpl_propertylist * plist, double value);
void xsh_pfits_set_slitmap_median_slicup( cpl_propertylist * plist, double value);
void xsh_pfits_set_slitmap_median_sliclo( cpl_propertylist * plist, double value);

double xsh_pfits_get_slitmap_median_edgup(const cpl_propertylist * plist);
double xsh_pfits_get_slitmap_median_edglo(const cpl_propertylist * plist);
double xsh_pfits_get_slitmap_median_cen(const cpl_propertylist * plist);
double xsh_pfits_get_slitmap_median_slicup(const cpl_propertylist * plist);
double xsh_pfits_get_slitmap_median_sliclo(const cpl_propertylist * plist);

void xsh_pfits_set_wavemap_order_lambda_min( cpl_propertylist * plist, int absorder, double value);
void xsh_pfits_set_wavemap_order_lambda_max( cpl_propertylist * plist, int absorder, double value);
double xsh_pfits_get_geolon(const cpl_propertylist * plist);
double xsh_pfits_get_geolat(const cpl_propertylist * plist);
double xsh_pfits_get_utc(const cpl_propertylist * plist);

void xsh_pfits_set_shiftifu_lambdaref( cpl_propertylist * plist, double value);
void xsh_pfits_set_shiftifu_slitref( cpl_propertylist * plist, double value);
void xsh_pfits_set_shiftifu_slitdownref( cpl_propertylist * plist, double value);
void xsh_pfits_set_shiftifu_slitcenref( cpl_propertylist * plist, double value);
void xsh_pfits_set_shiftifu_slitupref( cpl_propertylist * plist, double value);
double xsh_pfits_get_shiftifu_lambdaref( cpl_propertylist * plist);
double xsh_pfits_get_shiftifu_slitref( cpl_propertylist * plist);
const char* xsh_pfits_get_telescop (const cpl_propertylist * plist);
cpl_error_code
xsh_plist_set_extra_keys(cpl_propertylist* plist,
			 const char* hduclas1,
			 const char* hduclas2,
			 const char* hduclas3,
			 const char* scidata,
			 const char* errdata,
			 const char* qualdata,
                         const int type);

cpl_error_code
xsh_pfits_set_wcs1(cpl_propertylist* header,
                   const double crpix1,
                   const double crval1,
                   const double cdelt1);
cpl_error_code
xsh_pfits_set_wcs2(cpl_propertylist* header,
                   const double crpix2,
                   const double crval2,
                   const double cdelt2);
cpl_error_code
xsh_pfits_set_wcs3(cpl_propertylist* header,
                   const double crpix3,
                   const double crval3,
                   const double cdelt3);

cpl_error_code
xsh_pfits_set_cd_matrix(cpl_propertylist* header,
                        const double cdelt1,
                        const double cdelt2);

cpl_error_code 
xsh_pfits_set_wcs(cpl_propertylist* header, 
	    const double crpix1, const double crval1, const double cdelt1, 
	    const double crpix2,const double crval2, const double cdelt2);
cpl_error_code
xsh_plist_div_by_fct(cpl_propertylist** plist,const int fctx,const int fcty);
cpl_error_code
xsh_pfits_combine_headers(cpl_propertylist* header,cpl_frameset* set);
cpl_error_code
xsh_pfits_combine_two_frames_headers(cpl_frame* first, cpl_frame* second);
int xsh_pfits_is_obs(cpl_propertylist* plist);
int xsh_pfits_is_flat(cpl_propertylist* plist);
#endif
