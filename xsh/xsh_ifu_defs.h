/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */
/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:37:36 $
 * $Revision: 1.5 $
 */

#if !defined(_XSH_IFU_DEFS_H)
#define _XSH_IFU_DEFS_H

#define SLITLET_LENGTH (LENGTH_SLIT_IFU/3.)
#define DOWN_SLITLET_MIN MIN_SLIT_IFU
#define DOWN_SLITLET_MAX (MIN_SLIT_IFU+SLITLET_LENGTH)
#define CEN_SLITLET_MIN DOWN_SLITLET_MAX
#define CEN_SLITLET_MAX (CEN_SLITLET_MIN+SLITLET_LENGTH)
#define UP_SLITLET_MIN CEN_SLITLET_MAX
#define UP_SLITLET_MAX MAX_SLIT_IFU

#define SLITLET_CEN_CENTER 0.0
#define SLITLET_DOWN_CENTER -4.0
#define SLITLET_UP_CENTER 4.0

enum {
  CENTER_SLIT,
  LOWER_IFU_SLITLET, CENTER_IFU_SLITLET, UPPER_IFU_SLITLET
} ;

/*
  DOWN is for lower slitlet (low Y), UP for upper (high Y)
*/
static const char * SlitletName[] = {"", "DOWN", "CEN", "UP"} ;

#endif
