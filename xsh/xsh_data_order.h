/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.38 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_ORDER_H
#define XSH_DATA_ORDER_H

#include <cpl.h>
#include <xsh_data_instrument.h>

#define XSH_ORDER_TABLE_COLNAME_ORDER "ORDER"
#define XSH_ORDER_TABLE_COLNAME_ABSORDER "ABSORDER"
#define XSH_ORDER_TABLE_COLNAME_CENTER "CENCOEF"
#define XSH_ORDER_TABLE_COLNAME_EDGUP "EDGUPCOEF"
#define XSH_ORDER_TABLE_COLNAME_EDGLO "EDGLOCOEF"
#define XSH_ORDER_TABLE_COLNAME_SLICUP "SLICUPCOEF"
#define XSH_ORDER_TABLE_COLNAME_SLICLO "SLICLOCOEF"
#define XSH_ORDER_TABLE_DEGY "DEGY"
#define XSH_ORDER_TABLE_COLNAME_STARTY "STARTY" 
#define XSH_ORDER_TABLE_COLNAME_ENDY "ENDY"

#define XSH_ORDER_TABLE_COLNAME_CENTERX "CENTER_X"
#define XSH_ORDER_TABLE_COLNAME_CENTERY "CENTER_Y"
#define XSH_ORDER_TABLE_COLNAME_EDGUPX "EDG_UP_X"
#define XSH_ORDER_TABLE_COLNAME_EDGUPY "EDG_UP_Y"
#define XSH_ORDER_TABLE_COLNAME_EDGLOX "EDG_LO_X"
#define XSH_ORDER_TABLE_COLNAME_EDGLOY "EDG_LO_Y"
#define XSH_ORDER_TABLE_COLNAME_SLICLOX "SLIC_LO_X"
#define XSH_ORDER_TABLE_COLNAME_SLICLOY "SLIC_LO_Y"
#define XSH_ORDER_TABLE_COLNAME_SLICUPX "SLIC_UP_X"
#define XSH_ORDER_TABLE_COLNAME_SLICUPY "SLIC_UP_Y"



typedef struct{
  /* relative index of order */
  int order;
  /* absolute index of order */
  int absorder;
  /* polynomial fitting the center points of the order */
  cpl_polynomial *cenpoly;
  /* polynomial fitting the upper edge of the order */
  cpl_polynomial *edguppoly;
  /* polynomial fitting the lower edge of the order */ 
  cpl_polynomial *edglopoly;
  /* polynomial fitting the blaze fonction */
  cpl_polynomial *blazepoly;
  /* For IFU */
  cpl_polynomial *sliclopoly;
  cpl_polynomial *slicuppoly;
  /* Polynomial degree (same for the 3 polynomes) */
  int pol_degree ;
  /* first pixel in Y in the order flux */
  int starty;
  /* last pixel in Y in the order flux */
  int endy;
  /*
  float edgupcoefsig0;
  float edgupcoefsig1;
  float edgupcoefsig2;
  float edglocoefsig0;
  float edglocoefsig1;
  float edglocoefsig2;
  */
}xsh_order;


typedef struct{
  int size;
  int bin_x;
  int bin_y;
  int absorder_min;
  int absorder_max;
  xsh_order* list;
  xsh_instrument* instrument; 
  cpl_propertylist* header;
}xsh_order_list;


typedef struct {
  double residmin ;
  double residmax ;
  double residavg ;
  double residrms ;


  int max_pred ;
  int min_pred ;
  int npred ;

  int max_det ;
  int min_det ;
  int ndet ;

  int nposall ;
  int npossel ;
} ORDERPOS_QC_PARAM ;

xsh_order_list* xsh_order_list_new( int size);
xsh_order_list* xsh_order_list_create(xsh_instrument* instr);
xsh_order_list* xsh_order_list_load(cpl_frame* frame, xsh_instrument* instr);
cpl_propertylist* xsh_order_list_get_header(xsh_order_list* list);
void xsh_order_list_set_starty( xsh_order_list* list, int i, int starty);
void xsh_order_list_set_endy( xsh_order_list* list, int i, int endy);
int xsh_order_list_get_starty(xsh_order_list* list, int i);
int xsh_order_list_get_endy(xsh_order_list* list, int i);
int xsh_order_list_get_index_by_absorder( xsh_order_list* list, 
  double absorder);
void xsh_order_list_free(xsh_order_list** list);
xsh_order_list* xsh_order_list_merge( xsh_order_list* lista,
  xsh_order_list* listb);

cpl_frame* 
xsh_order_list_save(xsh_order_list* list,
                    xsh_instrument* instrument,
                    const char* filename,
                    const char* tag,const int ny);
void xsh_order_list_set_bin_x( xsh_order_list* list, int bin);
void xsh_order_list_set_bin_y( xsh_order_list* list, int bin);
double xsh_order_list_eval( xsh_order_list* list, cpl_polynomial *poly, 
  double y);
int xsh_order_list_eval_int( xsh_order_list* list, cpl_polynomial *poly,
  double y);

void xsh_order_list_dump( xsh_order_list* list, const char * fname ) ;
void xsh_order_list_fit(xsh_order_list *list, int size, double* vorderdata,
  double* posx, double* posy, int deg_poly);
int xsh_order_list_get_order( xsh_order_list * list, int absorder ) ;
void xsh_order_list_verify( xsh_order_list * list, int ny ) ;
void xsh_order_list_apply_shift( xsh_order_list *list, double xshift, double yshift);

void xsh_order_split_qth_d2( cpl_frame* order_tab_frame, 
  cpl_frame* spectrum_format_frame, cpl_frame** qth_order_tab_frame, 
  cpl_frame** d2_order_tab_frame, xsh_instrument* instr);

#endif  /* XSH_ORDER_H */
