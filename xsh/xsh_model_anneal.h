/* $Id: xsh_model_anneal.h,v 1.4 2011-12-02 14:15:28 amodigli Exp $
 *
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_MODEL_ANNEAL_H
#define XSH_MODEL_ANNEAL_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#ifndef XSH_MODEL_KERNEL_H
#include <xsh_model_kernel.h>
#endif
/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

cpl_table* xsh_model_anneal_reduce(const char*, const char*, const char*);
void get_meas_coordinates(int, coord*, char*);
int countlines(const char*) ;

#endif
