/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-01-19 12:41:42 $
 * $Revision: 1.7 $
 */

#if !defined(XSH_DATA_ATMOS_EXT_H)
#define XSH_DATA_ATMOS_EXT_H


#include <cpl.h>

#define XSH_ATMOS_EXT_LIST_COLNAME_WAVELENGTH "LAMBDA"
#define XSH_ATMOS_EXT_LIST_COLNAME_K "EXTINCTION"
#define XSH_ATMOS_EXT_LIST_COLNAME_OLD "LA_SILLA"

typedef struct {
  int size ;
  cpl_propertylist * header ;
  double * lambda ;
  double * K ;
} xsh_atmos_ext_list ;

xsh_atmos_ext_list * xsh_atmos_ext_list_load( cpl_frame * ext_frame ) ;
void xsh_atmos_ext_list_free( xsh_atmos_ext_list ** list ) ;
xsh_atmos_ext_list * xsh_atmos_ext_list_create( int size ) ;
double * xsh_atmos_ext_list_get_lambda( xsh_atmos_ext_list * list ) ;
double * xsh_atmos_ext_list_get_K( xsh_atmos_ext_list * list ) ;
cpl_error_code 
xsh_atmos_ext_dump_ascii( xsh_atmos_ext_list * list, const char* filename );

#endif
