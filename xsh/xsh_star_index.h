/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2010-01-12 09:26:45 $
 * $Revision: 1.1 $
 *
*/
#ifndef _SINFONI_SINFO_STAR_INDEX_H_
#define _SINFONI_SINFO_STAR_INDEX_H_


typedef struct _star_index_ star_index;

/* Loading the index from the fits file
 * */
star_index* star_index_load(const char* fits_file);

/*Save the index to the fits file
 * */
star_index* star_index_create(void);
/* Add a new start to the index. To save the changed index to the file star_index_save() should be called
 * */
int star_index_add(star_index* pindex, double RA, double DEC, const char* star_name, cpl_table* ptable);
int star_index_remove_by_name(star_index* pindex, const char* starname);
int start_index_get_size(star_index* pindex);
int star_index_save(star_index* pindex, const char* fits_file_name);
cpl_table* star_index_get(star_index* pindex, double RA, double DEC, double RA_EPS, double DEC_EPS, const char** pstar_name);
void star_index_delete(star_index* pindex);
void star_index_dump(star_index* pindex, FILE* pfile);



#endif
