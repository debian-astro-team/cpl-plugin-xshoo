#ifndef XSH_COMPUTE_LINEARITY_H
#define XSH_COMPUTE_LINEARITY_H

/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.6 $
 */


/*-----------------------------------------------------------------------------
  Typedefs
  -----------------------------------------------------------------------------*/
typedef struct
{
  double exptime;		/**< exposure time */
  cpl_frame *frame;		/**< pointer to frame */
  int on_off ;			/**< Special for NIR frames */
} TIME_FRAME;

#define LINEAR_POLYNOMIAL_DEGREE 2

cpl_frameset * xsh_subtract_on_off( cpl_frameset *set,
				    xsh_instrument *instrument ) ;

#endif

